<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <section|Drafts>

  <subsection|The Relation between Creation Operator and Free Field>

  <\theorem>
    The relation between creation operator and free field is given by

    <\eqnarray*>
      <tformat|<table|<row|<cell|a<rsup|\<dagger\>><around*|(|\<b-p\>|)>>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-\<mathi\> <sqrt|<frac|1|2
      E<around*|(|\<b-p\>|)>>> \<pi\><around*|(|\<b-x\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]> .>>>>
    </eqnarray*>

    where used, in field-representation, <math|\<pi\><around*|(|\<b-x\>|)>=-\<mathi\>
    \<delta\>/\<delta\>\<phi\><around*|(|\<b-x\>|)>>. In addition,

    <\eqnarray*>
      <tformat|<table|<row|<cell|a<around*|(|\<b-p\>|)>>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|-\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>+\<mathi\> <sqrt|<frac|1|2
      E<around*|(|\<b-p\>|)>>> \<pi\><around*|(|\<b-x\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|-\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>+<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]> .>>>>
    </eqnarray*>
  </theorem>

  <\proof>
    C.f. QTF, section 9.2. First notice (<math|E<around*|(|\<b-p\>|)>=<sqrt|\<b-p\><rsup|2>+m<rsup|2>>>)

    <\equation*>
      \<phi\><around*|(|\<b-x\>|)>=<big|int>\<mathd\><rsup|3>p
      <sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <around*|[|\<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      a<around*|(|\<b-p\>|)>+\<mathe\><rsup|-\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>
    </equation*>

    and

    <\equation*>
      \<pi\><around*|(|\<b-x\>|)>=<wide|\<phi\>|\<dot\>><around*|(|\<b-x\>|)>=<around*|(|-\<mathi\>|)>
      <big|int>\<mathd\><rsup|3>p <sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      <around*|[|\<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      a<around*|(|\<b-p\>|)>-\<mathe\><rsup|-\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]> .
    </equation*>

    Then,

    <\eqnarray*>
      <tformat|<table|<row|<cell|>|<cell|>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|-\<mathi\> \<b-q\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-q\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>+\<mathi\> <sqrt|<frac|1|2
      E<around*|(|\<b-q\>|)>>> \<pi\><around*|(|\<b-x\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<sqrt|<frac|E<around*|(|\<b-q\>|)>|2>>
      <big|int>\<mathd\><rsup|3>p <sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <big|int>\<mathd\><rsup|3>x \<mathe\><rsup|-\<mathi\>
      \<b-q\>\<cdot\>\<b-x\>> <around*|[|\<mathe\><rsup|\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<around*|(|\<b-p\>|)>+\<mathe\><rsup|-\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>+>>|<row|<cell|>|<cell|+>|<cell|\<mathi\>
      <around*|(|-\<mathi\>|)> <sqrt|<frac|1|2 E<around*|(|\<b-q\>|)>>>
      <big|int>\<mathd\><rsup|3>p <sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      <big|int>\<mathd\><rsup|3>x \<mathe\><rsup|-\<mathi\>
      \<b-q\>\<cdot\>\<b-x\>> <around*|[|\<mathe\><rsup|\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<around*|(|\<b-p\>|)>-\<mathe\><rsup|-\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<sqrt|<frac|E<around*|(|\<b-q\>|)>|2>>
      <big|int>\<mathd\><rsup|3>p <sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      \ <around*|[|\<delta\><rsup|3><around*|(|\<b-p\>-\<b-q\>|)>
      a<around*|(|\<b-p\>|)>+\<delta\><rsup|3><around*|(|\<b-p\>+\<b-q\>|)>
      a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>+>>|<row|<cell|>|<cell|+>|<cell|<sqrt|<frac|1|2
      E<around*|(|\<b-q\>|)>>> <big|int>\<mathd\><rsup|3>p
      <sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      <around*|[|\<delta\><rsup|3><around*|(|\<b-p\>-\<b-q\>|)>
      a<around*|(|\<b-p\>|)>-\<delta\><rsup|3><around*|(|\<b-p\>+\<b-q\>|)>
      a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>
      <around*|[|a<around*|(|\<b-q\>|)>+a<rsup|\<dagger\>><around*|(|-\<b-q\>|)>|]>+<frac|1|2>
      <around*|[|a<around*|(|\<b-q\>|)>-a<rsup|\<dagger\>><around*|(|-\<b-q\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|a<around*|(|\<b-q\>|)>
      .>>>>
    </eqnarray*>

    So, we get

    <\eqnarray*>
      <tformat|<table|<row|<cell|a<rsup|\<dagger\>><around*|(|\<b-p\>|)>>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-\<mathi\> <sqrt|<frac|1|2
      E<around*|(|\<b-p\>|)>>> \<pi\><around*|(|\<b-x\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]> .>>>>
    </eqnarray*>

    where used, in field-representation, <math|\<pi\><around*|(|\<b-x\>|)>=-\<mathi\>
    \<delta\>/\<delta\>\<phi\><around*|(|\<b-x\>|)>>. In addition,

    <\eqnarray*>
      <tformat|<table|<row|<cell|a<around*|(|\<b-p\>|)>>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|-\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>+\<mathi\> <sqrt|<frac|1|2
      E<around*|(|\<b-p\>|)>>> \<pi\><around*|(|\<b-x\>|)>|]>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|-\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>+<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]> .>>>>
    </eqnarray*>
  </proof>

  <subsection|Instance of State as Boundary Action>

  <\theorem>
    For all state <math|\| \<alpha\>\<rangle\>> where exists functional
    <math|F<rsub|\<alpha\>>> s.t.

    <\equation*>
      \| \<alpha\>\<rangle\>=exp<around*|(|F<rsub|\<alpha\>><around*|[|a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|]>|)>
      \|0\<rangle\> ,
    </equation*>

    we have

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<langle\>
      \<phi\><around*|(|\<b-x\>|)>\|\<alpha\>
      \<rangle\>>|<cell|=>|<cell|exp<around*|(|F<rsub|\<alpha\>><around*|[|<big|int>\<mathd\><rsup|3>x
      \<mathe\><rsup|\<mathi\> \<b-p\>\<cdot\>\<b-x\>>
      <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]>|]>|)>\<times\>>>|<row|<cell|>|<cell|\<times\>>|<cell|exp<around*|(|-<frac|1|2>
      <big|int>\<mathd\><rsup|3>x \<mathd\><rsup|3>y
      \<phi\><around*|(|\<b-x\>|)> <with|math-font|cal|E><around*|(|\<b-x\>-\<b-y\>|)>
      \<phi\><around*|(|\<b-y\>|)>|)> ,>>>>
    </eqnarray*>

    where used <math|\<langle\> \<phi\><around*|(|\<b-x\>|)>\|0
    \<rangle\>=exp<around*|(|-1/2 <big|int>\<mathd\><rsup|3>x
    \<mathd\><rsup|3>y \<phi\><around*|(|\<b-x\>|)>
    <with|math-font|cal|E><around*|(|\<b-x\>-\<b-y\>|)>
    \<phi\><around*|(|\<b-y\>|)>|)>> with <math|<with|math-font|cal|E>> given
    by eq. (9.2.13) in QTF.
  </theorem>

  Then, the perturbation to Fock-vacuum state is an instance of this. Indeed:

  <\example>
    For state (un-normalized)

    <\equation*>
      \| \<alpha\>\<rangle\>\<assign\>\| 0\<rangle\>+\<epsilon\>
      <big|int>\<mathd\><rsup|3>p f<around*|(|\<b-p\>|)> \|
      \<b-p\>\<rangle\>=<around*|(|1+\<epsilon\> <big|int>\<mathd\><rsup|3>p
      f<around*|(|\<b-p\>|)> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|)> \|
      0\<rangle\>=exp<around*|(|\<epsilon\> <big|int>\<mathd\><rsup|3>p
      f<around*|(|\<b-p\>|)> a<rsup|\<dagger\>><around*|(|\<b-p\>|)>|)> \|
      0\<rangle\> ,
    </equation*>

    we have

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<langle\>
      \<phi\><around*|(|\<b-x\>|)>\|\<alpha\>
      \<rangle\>>|<cell|=>|<cell|exp<around*|(|\<epsilon\>
      <big|int>\<mathd\><rsup|3>p f<around*|(|\<b-p\>|)>
      <big|int>\<mathd\><rsup|3>x \<mathe\><rsup|\<mathi\>
      \<b-p\>\<cdot\>\<b-x\>> <around*|[|<sqrt|<frac|E<around*|(|\<b-p\>|)>|2>>
      \<phi\><around*|(|\<b-x\>|)>-<sqrt|<frac|1|2 E<around*|(|\<b-p\>|)>>>
      <frac|\<delta\>|\<delta\>\<phi\><around*|(|\<b-x\>|)>>|]>|)>\<times\>>>|<row|<cell|>|<cell|\<times\>>|<cell|exp<around*|(|-<frac|1|2>
      <big|int>\<mathd\><rsup|3>x \<mathd\><rsup|3>y
      \<phi\><around*|(|\<b-x\>|)> <with|math-font|cal|E><around*|(|\<b-x\>-\<b-y\>|)>
      \<phi\><around*|(|\<b-y\>|)>|)> .>>>>
    </eqnarray*>
  </example>
</body>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|1.1|?>>
    <associate|auto-3|<tuple|1.2|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Drafts>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>