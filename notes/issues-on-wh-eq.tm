<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <\question>
    How is W-H equation derived?
  </question>

  First seperate <math|\<phi\>=\<phi\><rsub|\<less\>>+\<phi\><rsub|\<gtr\>>>.
  Then

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int>D<around*|[|\<phi\>|]>
    exp<around*|(|-S<around*|[|\<phi\>|]>|)>>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\><rsub|\<less\>>|]>
    D<around*|[|\<phi\><rsub|\<gtr\>>|]> exp<around*|(|-S<rsub|0><around*|[|\<phi\><rsub|\<less\>>|]>-S<rsub|0><around*|[|\<phi\><rsub|\<gtr\>>|]>-S<rsub|I><around*|[|\<phi\><rsub|\<less\>>,\<phi\><rsub|\<gtr\>>|]>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\><rsub|\<less\>>|]>
    exp<around*|(|-S<rsub|0><around*|[|\<phi\><rsub|\<less\>>|]>|)>
    <big|sum><rsub|n=0><rsup|+\<infty\>><frac|<around*|(|-1|)><rsup|n>|n!>\<langle\>S<rsub|I><rsup|n><around*|[|\<phi\><rsub|\<less\>>,\<phi\><rsub|\<gtr\>>|]>\<rangle\><rsub|\<phi\><rsub|\<gtr\>>>
    ,>>>>
  </eqnarray*>

  where used <math|\<phi\><rsub|\<less\>> \<phi\><rsub|\<gtr\>>\<equiv\>0>
  and <math|\<langle\>#\<rangle\><rsub|\<phi\><rsub|\<gtr\>>>\<assign\><big|int>D<around*|[|\<phi\><rsub|\<gtr\>>|]>
  exp<around*|(|-S<rsub|0><around*|[|\<phi\><rsub|\<gtr\>>|]>|)> #>. From
  this expansion formalism, we conclude that the Feynman diagram all shall
  have the property that all extra-line carry momentum of
  <math|\<phi\><rsub|\<less\>>> and all inner-lines of
  <math|\<phi\><rsub|\<gtr\>>>. This implies that there're only tree diagram
  and loop diagram with all inner-line constructing the tree or loop being of
  <math|\<phi\><rsub|\<gtr\>>>. E.g., there's no such diagram with one
  inner-line of a loop is of <math|\<phi\><rsub|\<gtr\>>> and the other is of
  <math|\<phi\><rsub|\<less\>>>. asdf

  <\question>
    [The Gaussian Integral]

    Consider the Gaussian integral

    <\equation*>
      <big|int>D<around*|[|\<phi\>|]> exp<around*|{|<big|int>\<mathd\>q
      <frac|\<delta\><rsup|2>S|\<delta\>\<phi\><around*|(|q|)>
      \<delta\>\<phi\><around*|(|-q|)>> \<phi\><around*|(|q|)>
      \<phi\><around*|(|-q|)>|}> .
    </equation*>

    First,

    <\equation*>
      <big|int>\<mathd\>q <frac|\<delta\><rsup|2>S|\<delta\>\<phi\><around*|(|q|)>
      \<delta\>\<phi\><around*|(|-q|)>> \<phi\><around*|(|q|)>
      \<phi\><around*|(|-q|)><above|\<longrightarrow\>|dis.><big|sum><rsub|j><frac|\<Delta\>q
      \<delta\><rsup|2>S|\<delta\>\<phi\><around*|(|q<rsub|j>|)>
      \<delta\>\<phi\><around*|(|q<rsub|j>|)>>\<phi\><around*|(|q<rsub|j>|)>
      \<phi\><around*|(|q<rsub|j>|)> .
    </equation*>

    By

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big|prod><rsub|j><big|int>\<mathd\>x<rsub|j>
      \<mathe\><rsup|-<big|sum><rsub|j> a<rsub|j>
      x<rsub|j><rsup|2>>>|<cell|\<sim\>>|<cell|<big|prod><rsub|j><frac|1|<sqrt|a<rsub|j>>>=<big|prod><rsub|j>exp<around*|{|-<frac|1|2>
      ln<around*|(|a<rsub|j>|)>|}> ,>>>>
    </eqnarray*>

    and letting <math|a<rsub|j>=\<Delta\>q
    <around*|(|\<delta\><rsup|2>S/\<delta\>\<phi\><around*|(|q<rsub|j>|)>
    \<delta\>\<phi\><around*|(|q<rsub|j>|)>|)>> and <math|x<rsub|j>=
    \<phi\><around*|(|q<rsub|j>|)>>, gives

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big|prod><rsub|j>exp<around*|{|-<frac|1|2>
      ln<around*|(|a<rsub|j>|)>|}>>|<cell|=>|<cell|exp<around*|{|-<frac|1|2>
      <big|sum><rsub|j>ln<around*|(|<frac|\<Delta\>q
      \<delta\><rsup|2>S|\<delta\>\<phi\><around*|(|q<rsub|j>|)>
      \<delta\>\<phi\><around*|(|q<rsub|j<rsub|>>|)>>|)>|}>>>|<row|<cell|>|<cell|=>|<cell|<text|non-sense>
      .>>>>
    </eqnarray*>

    And it's hard to be as expected as

    <\equation*>
      exp<around*|{|-<frac|1|2> <big|sum><rsub|j>\<Delta\>q
      ln<around*|(|<frac|\<delta\><rsup|2>S|\<delta\>\<phi\><around*|(|q<rsub|j>|)>
      \<delta\>\<phi\><around*|(|q<rsub|j<rsub|>>|)>>|)>|}> .
    </equation*>

    <\remark>
      This problem is absent in QTF, since the <math|det> term is left to be
      eliminated by denominator of <math|Z<around*|[|J|]>/Z<around*|[|0|]>>.
      C.f. section 9.4 of QTF.

      This problem is also absent in Polchinski equation, since nowhere such
      Gaussian integral is made.
    </remark>

    <\proposition>
      <\with|color|red>
        With this realization, we find that this Gaussian integral is
        non-sense except for expanding <math|\<phi\><around*|(|x|)>> by
        Fourier series, with basis <math|L<rsup|-d/2> exp<around*|(|\<mathi\>
        2 \<pi\> j\<cdot\>x/L|)>=\<langle\>x\|j\<rangle\>> where <math|L> the
        length of box and <math|j\<in\>\<bbb-Z\><rsup|d>>, as

        <\eqnarray*>
          <tformat|<table|<row|<cell|\<phi\><around*|(|x|)>>|<cell|=>|<cell|\<langle\>
          x\|\<phi\> \<rangle\>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|j>\<langle\>
          j\|\<phi\> \<rangle\> \<langle\> x\|
          j\<rangle\>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|L<rsup|d/2>>
          <big|sum><rsub|j>\<phi\><rsub|j> exp<around*|(|\<mathi\> <frac|2
          \<pi\> j|L>\<cdot\>x|)> ,>>>>
        </eqnarray*>

        instead of by Fourier integral, as
        <math|\<phi\><around*|(|x|)>=<big|int>\<mathd\>p
        \<phi\><around*|(|x|)> exp<around*|(|\<mathi\> p\<cdot\>x|)>>.
      </with>
    </proposition>
  </question>

  <\question>
    Is the property of <math|<big|int>D<around*|[|\<phi\>|]>> employed during
    the deriving of W-H equation?
  </question>

  <\remark>
    So, things must be re-derived under Fourier series. Remind that

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big|int>\<mathd\>x
      \<phi\><rsup|n><around*|(|x|)>>|<cell|=>|<cell|<around*|(|<frac|1|L<rsup|d/2>>|)><rsup|n>
      <big|sum><rsub|j<rsub|1>,\<ldots\>,j<rsub|n>>
      \<phi\><rsub|j<rsub|1>>\<cdots\>\<phi\><rsub|j<rsub|n>>
      <big|int>\<mathd\>x exp<around*|(|<frac|2 \<pi\> \<mathi\>
      x\<cdot\><around*|(|j<rsub|1>+\<cdots\>+j<rsub|n>|)>|L>|)>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<frac|1|L<rsup|d/2>>|)><rsup|n-2>
      <big|sum><rsub|j<rsub|1>,\<ldots\>,j<rsub|n>>
      \<delta\><rsub|j<rsub|1>+\<cdots\>+j<rsub|n>>
      \<phi\><rsub|j<rsub|1>>\<cdots\>\<phi\><rsub|j<rsub|n>> .>>>>
    </eqnarray*>

    Then, consider the instance <math|S=<around*|(|1/4!|)>
    <big|int>\<mathd\>x \<phi\><rsup|4><around*|(|x|)>>. And calculated
    2-order ``shell''-expansion. Denote <math|j> ``off-shell'' and <math|l>
    ``on-shell''. Then,

    <\eqnarray*>
      <tformat|<table|<row|<cell|S<around*|[|\<phi\>|]>>|<cell|<above|\<longrightarrow\>|<text|2-order>>>|<cell|<frac|1|2>
      <around*|(|<frac|1|L<rsup|d/2>>|)><rsup|n-2>
      <big|sum><rsub|j<rsub|1>,j<rsub|2>,l<rsub|1>,l<rsub|2>>
      \<delta\><rsub|j<rsub|1>+j<rsub|2>+l<rsub|1>+l<rsub|2>>
      \<phi\><rsub|j<rsub|1>> \<phi\><rsub|j<rsub|2>> \<phi\><rsub|l<rsub|1>>
      \<phi\><rsub|l<rsub|2>>>>|<row|<cell|>|<cell|=>|<cell|<frac|1|2>
      <around*|(|<frac|1|L<rsup|d/2>>|)><rsup|n-2>
      <big|sum><rsub|j<rsub|1>,j<rsub|2>,l>
      \<theta\><around*|(|<around*|\||j<rsub|1>+j<rsub|2>+l|\|>\<in\><frac|L|2
      \<pi\>> <around*|(|\<mu\>-\<delta\>\<mu\>,\<mu\>|]>|)>
      \<phi\><rsub|j<rsub|1>> \<phi\><rsub|j<rsub|2>> \<phi\><rsub|l>
      \<phi\><rsub|-j<rsub|1>-j<rsub|2>-l>>>>>
    </eqnarray*>

    Next, if the very property of <math|<big|int>D<around*|[|\<phi\>|]>> is
    employed, then, for any <math|l> given, <math|j<rsub|1>+j<rsub|2>> lies
    on the target space of <math|<around*|(|d-1|)>>-sphere with radius
    <math|\<mu\> L/<around*|(|2 \<pi\>|)>>. This provides <math|d-1>
    restrictions to <math|2 d> components of
    <math|<around*|(|j<rsub|1>,j<rsub|2>|)>>. So, the <math|\<theta\>>
    function provides <math|<around*|(|j<rsub|1>,j<rsub|2>|)>> <math|2
    d-<around*|(|d-1|)>=d+1> dofs. Hence, this is definitely <strong|not> the
    case <math|\<theta\>\<approx\>\<delta\><rsub|j<rsub|1>+j<rsub|2>>> where
    only <math|d> dofs are left. <with|color|red|So, this is a left problem.>

    <\remark>
      <with|color|dark blue|As Wilson (1973) put, this problem is a hard one.
      And then he avoided it by employing soft cut-off, as Polchinski. This
      <strong|motives> the soft cut-off, and in turn, the exact RGE.>
    </remark>
  </remark>

  <\question>
    From Polchinski to Wenger-Honghton?
  </question>

  For instance, set the <em|prior>

  <\equation*>
    S<rprime|'>=-<big|int>\<mathd\>x <around*|{|<frac|g<rsub|2><around*|(|\<Lambda\>|)>|2!>
    \<phi\><rsup|2><around*|(|x|)>+<frac|g<rsub|4><around*|(|\<Lambda\>|)>|4!>
    \<phi\><rsup|4><around*|(|x|)>+<frac|g<rsub|6><around*|(|\<Lambda\>|)>|6!>
    \<phi\><rsup|6><around*|(|x|)>|}> ,
  </equation*>

  where we follow the notation of Polchinski (see also K. Huang). And

  <\equation*>
    S<rsub|0>=<big|int>\<mathd\>x \<mathd\>y
    K<rsup|-1><around*|(|x-y,\<Lambda\>|)> \<phi\><around*|(|x|)>
    \<phi\><around*|(|y|)>=<big|int>\<mathd\>p
    G<rsup|-1><around*|(|p,\<Lambda\>|)> \<phi\><around*|(|p|)>
    \<phi\><around*|(|-p|)> .
  </equation*>

  <\theorem>
    Polchinski equation does not enclose the prior. However, if approximation
    <math|K<around*|(|-\<mathi\> \<partial\>,\<Lambda\>|)>\<approx\>K<around*|(|0,\<Lambda\>|)>>,
    i.e. <math|G<around*|(|p,\<Lambda\>|)>\<approx\>G<around*|(|0,\<Lambda\>|)>>,
    is valid, then the Polchinski equation does enclose the prior, but its
    result is not that given by Wegner-Honghton equation.
  </theorem>

  <\proof>
    Directly,

    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|\<delta\>S<rprime|'>|\<delta\>\<phi\><around*|(|x|)>>>|<cell|=>|<cell|-<around*|{|g<rsub|2><around*|(|\<Lambda\>|)>
      \<phi\><around*|(|x|)>+<frac|g<rsub|4><around*|(|\<Lambda\>|)>|3!>
      \<phi\><rsup|3><around*|(|x|)>+<frac|g<rsub|6><around*|(|\<Lambda\>|)>|5!>
      \<phi\><rsup|5><around*|(|x|)>|}> ;>>|<row|<cell|<frac|\<delta\><rsup|2>S<rprime|'>|\<delta\>\<phi\><around*|(|x|)>
      \<delta\>\<phi\><around*|(|y|)>>>|<cell|=>|<cell|-\<delta\><around*|(|x-y|)>
      <around*|{|g<rsub|2><around*|(|\<Lambda\>|)>+<frac|g<rsub|4><around*|(|\<Lambda\>|)>|2!>
      \<phi\><rsup|2><around*|(|x|)>+<frac|g<rsub|6><around*|(|\<Lambda\>|)>|4!>
      \<phi\><rsup|4><around*|(|x|)>|}> .>>>>
    </eqnarray*>

    Polchinski equation is

    <\equation*>
      <frac|\<partial\>S<rprime|'>|\<partial\>\<Lambda\>>=-<frac|1|2>
      <big|int>\<mathd\>x \<mathd\>y <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|x-y|)>
      <around*|[|<frac|\<delta\><rsup|2>S<rprime|'>|\<delta\>\<phi\><around*|(|x|)>
      \<delta\>\<phi\><around*|(|y|)>>-<frac|\<delta\>S<rprime|'>|\<delta\>\<phi\><around*|(|x|)>>
      <frac|\<delta\>S<rprime|'>|\<delta\>\<phi\><around*|(|y|)>>|]> .
    </equation*>

    In particular in our instance,

    <\equation*>
      lhs=-<big|int>\<mathd\>x <around*|{|<frac|1|2!>
      <frac|\<mathd\>g<rsub|2>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|2><around*|(|x|)>+<frac|1|4!>
      <frac|\<mathd\>g<rsub|4>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|4><around*|(|x|)>+<frac|1|6!>
      <frac|\<mathd\>g<rsub|6>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|6><around*|(|x|)>|}> ;
    </equation*>

    and

    <\eqnarray*>
      <tformat|<table|<row|<cell|rhs>|<cell|=>|<cell|<frac|1|2>
      <big|int>\<mathd\>x <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      <around*|[|<frac|g<rsub|4>|2!> \<phi\><rsup|2><around*|(|x|)>+<frac|g<rsub|6>|4!>
      \<phi\><rsup|4><around*|(|x|)>|]>+>>|<row|<cell|>|<cell|+>|<cell|<frac|1|2>
      <big|int>\<mathd\>x \<mathd\>y <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|x-y,\<Lambda\>|)>
      <around*|[|g<rsub|2><rsup|2> \<phi\><around*|(|x|)>
      \<phi\><around*|(|y|)>+<frac|g<rsub|2> g<rsub|4>|3!>
      \<phi\><rsup|3><around*|(|x|)> \<phi\><around*|(|x|)>+<around*|(|<frac|g<rsub|4><rsup|2>|<around*|(|3!|)><rsup|2>>
      \<phi\><rsup|3><around*|(|x|)> \<phi\><rsup|3><around*|(|y|)>+<frac|g<rsub|2>
      g<rsub|6>|5!> \<phi\><rsup|5><around*|(|x|)>
      \<phi\><around*|(|y|)>|)>|]> ,>>>>
    </eqnarray*>

    where we have omitted the constant term and higher power (of
    <math|\<phi\>>) terms, and used the fact
    <math|K<around*|(|x-y,\<Lambda\>|)>=K<around*|(|y-x,\<Lambda\>|)>>.

    The second line of <math|rhs> having no associated part in the prior,
    thus Polchinski equation does not <em|enclose> the prior. Indeed, they
    are out of prior. E.g. (<math|G> is the Fourier transform of <math|K>)

    <\eqnarray*>
      <tformat|<table|<row|<cell|<big|int>\<mathd\>x \<mathd\>y
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|x-y,\<Lambda\>|)>
      \<phi\><rsup|3><around*|(|x|)> \<phi\><around*|(|x|)>>|<cell|=>|<cell|<big|int>\<mathd\>p<rsub|1>\<cdots\>\<mathd\>p<rsub|5>
      <frac|\<partial\>G|\<partial\>\<Lambda\>><around*|(|p<rsub|1>,\<Lambda\>|)>
      \<phi\><around*|(|p<rsub|2>|)>\<cdots\>\<phi\><around*|(|p<rsub|5>|)>\<times\>>>|<row|<cell|>|<cell|\<times\>>|<cell|<big|int>\<mathd\>x
      \<mathd\>y \<mathe\><rsup|\<mathi\> p<rsub|1>\<cdot\><around*|(|x-y|)>>
      \<mathe\><rsup|\<mathi\> <around*|(|p<rsub|2>+p<rsub|3>+p<rsub|4>|)>\<cdot\>x>
      \<mathe\><rsup|\<mathi\> p<rsub|5>\<cdot\>y>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\>p<rsub|1>\<cdots\>\<mathd\>p<rsub|5>
      <frac|\<partial\>G|\<partial\>\<Lambda\>><around*|(|p<rsub|1>,\<Lambda\>|)>
      \<phi\><around*|(|p<rsub|2>|)>\<cdots\>\<phi\><around*|(|p<rsub|5>|)>\<times\>>>|<row|<cell|>|<cell|\<times\>>|<cell|\<delta\><around*|(|p<rsub|1>+p<rsub|2>+p<rsub|3>+p<rsub|4>|)>
      \<delta\><around*|(|p<rsub|1>-p<rsub|5>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\>p<rsub|1>\<cdots\>\<mathd\>p<rsub|4>
      \<delta\><around*|(|p<rsub|1>+p<rsub|2>+p<rsub|3>+p<rsub|4>|)>
      <frac|\<partial\>G|\<partial\>\<Lambda\>><around*|(|p<rsub|1>,\<Lambda\>|)>
      \<phi\><around*|(|p<rsub|1>|)>\<cdots\>\<phi\><around*|(|p<rsub|4>|)>
      .>>>>
    </eqnarray*>

    On the other hand,

    <\equation*>
      <big|int>\<mathd\>x <around*|(|<frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|-\<mathi\>
      \<partial\>,\<Lambda\>|)> \<phi\><around*|(|x|)>|)>
      \<phi\><around*|(|x|)><rsup|3>=<big|int>\<mathd\>p<rsub|1>\<cdots\>\<mathd\>p<rsub|4>
      \<delta\><around*|(|p<rsub|1>+p<rsub|2>+p<rsub|3>+p<rsub|4>|)>
      <frac|\<partial\>G|\<partial\>\<Lambda\>><around*|(|p<rsub|1>,\<Lambda\>|)>
      \<phi\><around*|(|p<rsub|1>|)>\<cdots\>\<phi\><around*|(|p<rsub|4>|)> .
    </equation*>

    So we conclude

    <\equation*>
      <big|int>\<mathd\>x \<mathd\>y <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|x-y,\<Lambda\>|)>
      \<phi\><rsup|3><around*|(|x|)> \<phi\><around*|(|x|)>=<big|int>\<mathd\>x
      <around*|(|<frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|-\<mathi\>
      \<partial\>,\<Lambda\>|)> \<phi\><around*|(|x|)>|)>
      \<phi\><around*|(|x|)><rsup|3> ,
    </equation*>

    which is a term that is out of the prior. The first statement is proven.

    If we expand <math|<around*|(|\<partial\>K/\<partial\>\<Lambda\>|)><around*|(|-\<mathi\>
    \<partial\>,\<Lambda\>|)>> by <math|\<partial\>>, i.e.

    <\equation*>
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|-\<mathi\>
      \<partial\>,\<Lambda\>|)><above|=|T.E.><frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>+<with|math-font|cal|O><around*|(|\<partial\><rsup|1>|)>
      ,
    </equation*>

    and propose that approximation of omitting
    <math|<with|math-font|cal|O><around*|(|\<partial\><rsup|1>|)>> is valid,
    then

    <\equation*>
      <big|int>\<mathd\>x \<mathd\>y <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|x-y,\<Lambda\>|)>
      \<phi\><rsup|3><around*|(|x|)> \<phi\><around*|(|x|)>\<approx\><big|int>\<mathd\>x
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      \<phi\><around*|(|x|)><rsup|4> .
    </equation*>

    With this approximation,

    <\eqnarray*>
      <tformat|<table|<row|<cell|rhs>|<cell|=>|<cell|<frac|1|2>
      <big|int>\<mathd\>x <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      <around*|[|<around*|(|<frac|g<rsub|4>|2!>+g<rsub|2><rsup|2>|)>
      \<phi\><rsup|2><around*|(|x|)>+<around*|(|<frac|g<rsub|6>|4!>+<frac|g<rsub|2>
      g<rsub|4>|3!>|)> \<phi\><rsup|4><around*|(|x|)>+<around*|(|<frac|g<rsub|4><rsup|2>|<around*|(|3!|)><rsup|2>>
      +<frac|g<rsub|2> g<rsub|6>|5!>|)> \<phi\><rsup|6><around*|(|x|)>|]>
      .>>>>
    </eqnarray*>

    Remind that

    <\equation*>
      lhs=-<big|int>\<mathd\>x <around*|{|<frac|1|2!>
      <frac|\<mathd\>g<rsub|2>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|2><around*|(|x|)>+<frac|1|4!>
      <frac|\<mathd\>g<rsub|4>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|4><around*|(|x|)>+<frac|1|6!>
      <frac|\<mathd\>g<rsub|6>|\<mathd\>\<Lambda\>><around*|(|\<Lambda\>|)>
      \<phi\><rsup|6><around*|(|x|)>|}> .
    </equation*>

    Then, <math|lhs=rhs> gives

    <\eqnarray*>
      <tformat|<table|<row|<cell|<frac|\<mathd\>g<rsub|2>|\<mathd\>\<Lambda\>>>|<cell|=>|<cell|-<frac|1|2>
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      <around*|(|g<rsub|4>+2 g<rsub|2><rsup|2>|)>
      ;>>|<row|<cell|<frac|\<mathd\>g<rsub|4>|\<mathd\>\<Lambda\>>>|<cell|=>|<cell|-<frac|1|2>
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      <around*|(|g<rsub|6>+4 g<rsub|2> g<rsub|4>|)>
      ;>>|<row|<cell|<frac|\<mathd\>g<rsub|6>|\<mathd\>\<Lambda\>>>|<cell|=>|<cell|-<frac|1|2>
      <frac|\<partial\>K|\<partial\>\<Lambda\>><around*|(|0,\<Lambda\>|)>
      <around*|(|20g<rsub|4><rsup|2> +6 g<rsub|2> g<rsub|6>|)> .>>>>
    </eqnarray*>

    So, we conclude that, with this approximation, Polchinski equation does
    enclose the prior. However, this is not that given by Wegner-Honghton
    equation!
  </proof>
</body>

<\references>
  <\collection>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
  </collection>
</references>