<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <section|Introduction>

  RGE is called for when we focus only on large spatial scale, while omitting
  any phenomena in smaller spatial scale. This is intuitive valid, since we
  all know we can study the friction without knowing the fluctuation of
  atoms. But, if coupling between different momentum-modes exists, we must
  keep in mind that the actions of large spatial scale, and that containing
  also smaller spatial scale modes, are different (L.Landau's mistake).
  Indeed, the law of friction is different from that of QED. The RGE is,
  then, employed for giving a quantitative description of this modification
  on action.

  It was born for studying the Ising model, but inspired from the fixed
  source theory (solvable renormalizable model). Verily, because of the
  generality of this modification on action, RGE is useful in many areas. For
  instance, in cosmology, c.f. <hlink|this
  paper|http://arxiv.org/abs/hep-ph/0410095v3>.

  RGE, however, has developed many versions. In this note, we first give the
  original version that Wilson employed. Then, show up the second version by
  Callan-S?, which is based on Wilson's.

  <\notation>
    Throughout this note, all bared quantities are labeled by a <math|B>
    index, while all renormalizaed are by none.
  </notation>

  <section|RGE in Wilson's Approach>

  Consider a truncated theory, with a ultraviolet cut-off <math|\<Lambda\>>.
  Thus

  <\equation*>
    \<phi\><around*|(|x;\<Lambda\>|)>=<big|int><rsub|<around*|\||p|\|>\<in\><around*|[|0,\<Lambda\>|]>>\<mathd\>p
    \<mathe\><rsup|\<mathi\> p\<cdot\>x> \<phi\><around*|(|p|)> .
  </equation*>

  Its Euclidean<\footnote>
    For the equivalence and conversion between Minkovskian and Euclidean
    theory, c.f. the note asdf.
  </footnote> un-normalized partition functional is

  <\equation*>
    <wide|Z|~><around*|[|J;\<Lambda\>|]>=<big|int>D<around*|[|\<phi\>|]><rsub|\<Lambda\>>
    exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>;\<Lambda\>|]>-<big|int><rsup|\<Lambda\>>\<mathd\>p
    \<phi\><around*|(|p|)> J<around*|(|-p|)>|)> ,
  </equation*>

  where denoted <math|<big|int>D<around*|[|\<phi\>|]><rsub|\<Lambda\>>\<assign\><big|prod><rsub|p\<in\><around*|[|0,\<Lambda\>|]>><big|int>\<mathd\><around*|(|\<phi\><around*|(|p|)>|)>>,
  and where the parameter <math|\<Lambda\>> in
  <math|S<around*|[|#1;\<Lambda\>|]>> is represented by the dependence of
  <math|\<Lambda\>> in the couplings in <math|S>, as to be revealed in the
  following. This kind of truncated action, whose couplings depend on the
  truncated scale <math|\<Lambda\>>, is called <em|Wilson (Wilsonian)
  effective action>.

  In any case, this action can always be gained by reducing a full theory,
  for instance, by matching results of S-matrices between that calculated by
  the full theory and that by the Wilson effective action. It must be
  stressed that all couplings in Wilson effective action
  <math|S<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>;\<Lambda\>|]>> are
  bared, including the field <math|\<phi\><around*|(|x;\<Lambda\>|)>>. The
  bared coefficients, even though all finite, can be much great, so that
  renormalization is inevitable.

  Normalized partition function is <math|Z<around*|[|J;\<Lambda\>|]>=<wide|Z|~><around*|[|J;\<Lambda\>|]>/<wide|Z|~><around*|[|0;\<Lambda\>|]>>.
  But, by virtue of this simple relation, we focus on <math|<wide|Z|~>> only.

  <subsection|Integrate out Momentum Slice>

  Conveniently, denote <math|\<psi\><around*|(|x|)>\<assign\><big|int><rsub|<around*|\||p|\|>\<in\><around*|(|s
  \<Lambda\>,\<Lambda\>|]>>\<mathd\>p \<mathe\><rsup|\<mathi\> p\<cdot\>x>
  \<phi\><around*|(|p|)>>; thus <math|\<phi\><around*|(|x;\<Lambda\>|)>=\<phi\><around*|(|x;s
  \<Lambda\>|)>+\<psi\><around*|(|x|)>>.

  <\theorem>
    For <math|s \<Lambda\>> with any <math|0\<less\>s\<less\>1>,
    <math|\<exists\> S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>;s\<Lambda\>|]>>, s.t.

    <\equation*>
      <wide|Z|~><around*|[|J;\<Lambda\>|]>=<big|int>D<around*|[|\<phi\>|]><rsub|s
      \<Lambda\>> exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;s \<Lambda\>|]>-<big|int><rsup|s \<Lambda\>>\<mathd\>p
      \<phi\><around*|(|p|)> J<around*|(|-p|)>|)> .
    </equation*>
  </theorem>

  \;

  <\proof>
    For any configuration <math|\<phi\><around*|(|x;s \<Lambda\>|)>> given,
    denote

    <\equation*>
      exp<around*|(|-\<delta\>S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>|]>|)>\<assign\><big|int>D<around*|[|\<psi\>|]>
      exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>+\<psi\><around*|(|x|)>;\<Lambda\>|]>+S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>|)> .
    </equation*>

    Then, <math|S<around*|[|\<phi\><around*|(|x;s \<Lambda\>|)>;s
    \<Lambda\>|]>=S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>;\<Lambda\>|]>+\<delta\>S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>|]>>. Indeed, let <math|J<around*|(|p|)>=0> for any
    <math|p\<in\><around*|(|s \<Lambda\>,\<Lambda\>|]>> then the source term
    <math|<big|int><rsup|s \<Lambda\>>\<mathd\>p \<phi\><around*|(|p|)>
    J<around*|(|-p|)>> is directly gained. This is because
    <math|J<around*|(|p|)>> with <math|p\<in\><around*|(|s
    \<Lambda\>,\<Lambda\>|]>> is useless when <math|\<phi\><around*|(|p|)>>
    with <math|p\<in\><around*|(|s \<Lambda\>,\<Lambda\>|]>> has been
    integrated out. Then, by omitting this source term,

    <\eqnarray*>
      <tformat|<table|<row|<cell|<wide|Z|~><around*|[|J;\<Lambda\>|]>>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\>|]><rsub|
      \<Lambda\>> exp<around*|(|-S<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>;
      \<Lambda\>|]>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\>|]><rsub|s
      \<Lambda\>> <big|int>D<around*|[|\<psi\>|]>
      exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>+\<psi\><around*|(|x|)>;\<Lambda\>|]>+S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\>|]><rsub|s
      \<Lambda\>> exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>|)> <big|int>D<around*|[|\<psi\>|]>
      exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>+\<psi\><around*|(|x|)>;\<Lambda\>|]>+S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>|)>>>|<row|<cell|>|<cell|=>|<cell|<big|int>D<around*|[|\<phi\>|]><rsub|s
      \<Lambda\>> exp<around*|(|-S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>;\<Lambda\>|]>-\<delta\>S<around*|[|\<phi\><around*|(|x;s
      \<Lambda\>|)>|]>|)> ,>>>>
    </eqnarray*>

    which is what we want.
  </proof>

  <subsection|A Toy Instance>

  With this proof, we find the key is to calculate
  <math|exp<around*|(|-\<delta\>S<around*|[|\<phi\><around*|(|x;s
  \<Lambda\>|)>|]>|)>>. For clarity the key ideas while avoiding complicated
  calculation, consider the toy mode with the bared couplings
  <math|\<lambda\><rsub|B>> small enough so that the perturbative calculation
  is straight-forward without any renormalization. In addition, let
  <math|<around*|\||1-s|\|>\<ll\>1>.

  <\example>
    <label|example: lambda phi 4 model>Consider a truncated Euclidean
    <math|\<lambda\> \<phi\><rsup|4>> model with mass
    <math|m\<ll\>\<Lambda\>> and with <math|\<lambda\>\<ll\>1>, whose action
    is

    <\equation*>
      S<around*|[|\<phi\>;\<Lambda\>|]>=-<big|int><frac|1|2>
      <around*|(|\<partial\><rsup|2>+m<rsub|B><rsup|2>|)>
      \<phi\><rsup|2><around*|(|x;\<Lambda\>|)>+<frac|\<lambda\><rsub|B>|4!>
      \<phi\><rsup|4><around*|(|x;\<Lambda\>|)> \<mathd\>x .
    </equation*>
  </example>

  To calculate <math|\<delta\>S>, we first notice

  <\eqnarray*>
    <tformat|<table|<row|<cell|>|<cell|>|<cell|S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>+\<psi\><around*|(|x|)>;\<Lambda\>|]>-S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>;\<Lambda\>|]>>>|<row|<cell|>|<cell|=>|<cell|-<big|int><frac|1|2>
    <around*|(|\<partial\><rsup|2>+m<rsub|B><rsup|2>|)>
    \<psi\><rsup|2><around*|(|x|)>+>>|<row|<cell|>|<cell|+>|<cell|\<lambda\><rsub|B>
    <around*|(|<frac|1|3!>\<phi\><around*|(|x;s \<Lambda\>|)>
    \<psi\><rsup|3><around*|(|x|)>+<frac|1|4> \<phi\><rsup|2><around*|(|x;s
    \<Lambda\>|)> \<psi\><rsup|2><around*|(|x|)>+<frac|1|3!>
    \<phi\><rsup|3><around*|(|x;s \<Lambda\>|)>
    \<psi\><around*|(|x|)>+<frac|1|4!> \<psi\><rsup|4><around*|(|x;\<Lambda\>|)>|)>
    \<mathd\>x ,>>>>
  </eqnarray*>

  where used <math|<big|int>\<mathd\>x <around*|(|\<partial\><rsup|2>+m<rsup|2>|)>
  \<phi\><around*|(|x;s \<Lambda\>|)> \<psi\><around*|(|x|)>=0> since Fourier
  modes are orthogonal to each other. Next is to perturbation to
  <math|\<lambda\>> in path-integral of <math|\<psi\>>. Keep in mind that
  <math|<around*|\<langle\>|\<phi\><rsup|any>
  \<psi\><rsup|odd>|\<rangle\>><rsub|\<psi\>>=0>. At 1st order of expansion,
  up to un-important constant, only <math|\<phi\><rsup|2><around*|(|x;s
  \<Lambda\>|)> \<psi\><rsup|2><around*|(|x|)>> term contributes, which is

  <\eqnarray*>
    <tformat|<table|<row|<cell|>|<cell|>|<cell|<frac|\<lambda\><rsub|B>|4>
    <big|int>D<around*|[|\<psi\>|]> exp<around*|(|-<frac|1|2><big|int><rsub|<around*|\||p|\|>\<in\><around*|(|s
    \<Lambda\>,\<Lambda\>|)>>\<mathd\>p \<psi\><around*|(|p|)>
    <around*|(|p<rsup|2>+m<rsub|B><rsup|2>|)>
    \<psi\><around*|(|-p|)>|)>\<times\>>>|<row|<cell|>|<cell|\<times\>>|<cell|<big|int><rsub|<around*|\||p|\|>\<in\><around*|[|0,s
    \<Lambda\>|]>>\<mathd\>p<rsub|1>\<mathd\>p<rsub|2><big|int><rsub|<around*|\||p|\|>\<in\><around*|[|s
    \<Lambda\>,\<Lambda\>|]>>\<mathd\>p<rsub|3>\<mathd\>p<rsub|4>
    \<delta\><rsup|3><around*|(|p<rsub|1>+\<cdots\>+p<rsub|4>|)>
    \<phi\><around*|(|p<rsub|1>|)> \<phi\><around*|(|p<rsub|2>|)>
    \<phi\><around*|(|p<rsub|3>|)> \<phi\><around*|(|p<rsub|4>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|\<lambda\><rsub|B>|4>
    <big|int><rsub|<around*|\||p|\|>\<in\><around*|[|0,s
    \<Lambda\>|]>>\<mathd\>p<rsub|1>\<mathd\>p<rsub|2><big|int><rsub|<around*|\||p|\|>\<in\><around*|[|s
    \<Lambda\>,\<Lambda\>|]>>\<mathd\>p<rsub|3>\<mathd\>p<rsub|4>
    \<delta\><rsup|3><around*|(|p<rsub|1>+\<cdots\>+p<rsub|4>|)>
    \<phi\><around*|(|p<rsub|1>|)> \<phi\><around*|(|p<rsub|2>|)>
    <frac|\<delta\><around*|(|p<rsub|3>+p<rsub|4>|)>|p<rsub|3><rsup|2>+m<rsub|B><rsup|2>>>>|<row|<cell|>|<cell|=>|<cell|<frac|\<lambda\><rsub|B>|4>
    <big|int>\<mathd\>x \<phi\><rsup|2><around*|(|x;s \<Lambda\>|)>
    <big|int><rsub|<around*|\||p|\|>\<in\><around*|[|s
    \<Lambda\>,\<Lambda\>|]>> \<mathd\>p <frac|1|p<rsup|2>+m<rsub|B><rsup|2>>
    <around*|(|\<approx\><frac|1|p<rsup|2>>|)>>>|<row|<cell|>|<cell|=>|<cell|<frac|\<lambda\><rsub|B>
    \<Lambda\><rsup|2>|4> <around*|(|1-s<rsup|2>|)> <big|int>\<mathd\>x
    \<phi\><rsup|2><around*|(|x;s \<Lambda\>|)>>>>>
  </eqnarray*>

  where in the last step used <math|m\<ll\>\<Lambda\>>, and dropped some
  cookies. Thus

  <\equation*>
    \<delta\>S<around*|[|\<phi\><around*|(|x;s
    \<Lambda\>|)>|]>=-<frac|\<lambda\><rsub|B> \<Lambda\><rsup|2>|4>
    <around*|(|1-s<rsup|2>|)> <big|int>\<mathd\>x
    \<phi\><rsup|2><around*|(|x;s \<Lambda\>|)>+<with|math-font|cal|O><around*|(|\<lambda\><rsup|2>|)>
  </equation*>

  Recognizing that this contributes a <math|\<delta\>m<rsub|B><rsup|2>\<sim\>\<lambda\><rsub|B>
  \<Lambda\><rsup|2> <around*|(|1-s<rsup|2>|)>>.

  In the 2nd order of expansion, we can recognize that, for instance,
  <math|<around*|(|\<lambda\><rsub|B> \<phi\><rsup|2><around*|(|x;s
  \<Lambda\>|)> \<psi\><rsup|2><around*|(|x|)>|)><rsup|2>> contributes to
  <math|\<delta\>\<lambda\>>. Interestingly, there's contribution to
  <math|\<phi\><rsup|6><around*|(|x;s \<Lambda\>|)>>!

  <subsection|Matching Vertices Method>

  Still consider the previous example <reference|example: lambda phi 4
  model>, but in vertex function approach. This is what Polchinski (1984)
  did. At one-loop level, it is manifest that, when momenta of in-going
  particles, <math|q<rsub|j>>s, are much smaller than <math|\<Lambda\>>,

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<delta\>m<rsub|B><rsup|2>>|<cell|=>|<cell|\<lambda\><rsub|B>
    <big|int><rsub|<around*|\||p|\|>\<in\><around*|(|s
    \<Lambda\>,\<Lambda\>|]>>\<mathd\>p <frac|1|p<rsup|2>+m<rsub|B><rsup|2>>
    \<sim\>\<lambda\><rsub|B> \<Lambda\><rsup|2>
    <around*|(|1-s<rsup|2>|)>>>|<row|<cell|\<delta\>\<lambda\><rsub|B>>|<cell|=>|<cell|\<lambda\><rsub|B><rsup|2>
    <big|int><rsub|<around*|\||p|\|>\<in\><around*|(|s
    \<Lambda\>,\<Lambda\>|]>>\<mathd\>p <frac|1|p<rsup|2>
    <around*|(|q<rsub|1>+q<rsub|2>-p|)><rsup|2>>
    \<sim\>\<lambda\><rsub|B><rsup|2> <around*|(|1-ln<around*|(|s|)>|)>
    ;>>|<row|<cell|\<delta\>g<rsub|B 6>>|<cell|=>|<cell|0
    ;>>|<row|<cell|\<delta\>g<rsub|B 8>>|<cell|=>|<cell|\<lambda\><rsub|B><rsup|4>
    <big|int><rsub|<around*|\||p|\|>\<in\><around*|(|s
    \<Lambda\>,\<Lambda\>|]>> \<mathd\>p <around*|(|<frac|1|p<rsup|2>>|)><rsup|4>\<sim\>\<lambda\><rsub|B><rsup|4>
    \<Lambda\><rsup|-4> <around*|(|1-s<rsup|-4>|)>
    ;>>|<row|<cell|>|<cell|\<ldots\>>|<cell|,>>>>
  </eqnarray*>

  where <math|g<rsub|B n>> denotes the bared coefficients of
  <math|\<phi\><rsup|n><around*|(|x|)>> term in Lagrangian density. (Sorry
  that I cannot draw the associated Feynman diagrams.) Notice that this
  approach gains the same as Wilson's integrating out process at
  <math|\<lambda\><rsub|B><rsup|1>>-order. But, it's much easier and manifest
  than the previous method.

  Manifestly, after integrating out a momentum slice, many couplings,
  including infinitely many irrelevant ones, emerge. This hints that, when
  writing down an effective action, it would be essential to including all
  terms of interaction that are valid by additional restrictions such as
  symmetries.

  <subsection|General Case>

  Generally, <math|\<Lambda\>> is large. This makes \Pdivergence\Q such that
  contribution from each diagram cannot be small (even though still finite),
  so that renormalization is essential. Thus, bared couplings cannot be small
  enough, while the observed (renormalized) couplings, however, can.
  Explicitly, we perform the general renormalization procedure with
  renomalized couplings, say <math|g>, fixed (remind that renormalized mass
  is the pole that is observed), but with ultraviolet cut-off. This calculate
  can be taken by setting ultraviolet cut-off at <math|\<Lambda\>> and at
  <math|s \<Lambda\>>. Each of them can furnish bared couplings,
  <math|g<rsub|B><around*|(|\<Lambda\>|)>=g+\<delta\>g<around*|(|\<Lambda\>|)>>
  or <math|g<rsub|B><around*|(|s \<Lambda\>|)>=g+\<delta\>g<around*|(|s
  \<Lambda\>|)>>, which are finite. In Wilson's approach, the beta function,
  then, is the phase vector field of <math|\<Lambda\>
  \<partial\>g<rsub|B>/\<partial\>\<Lambda\>>, with additional re-scaling, as
  we will see.

  <subsection|Beta Function>

  The (bared) coefficients in Wilson effective action
  <math|S<around*|[|\<phi\><around*|(|x;s \<Lambda\>|)>;s \<Lambda\>|]>>
  depends on <math|s \<Lambda\>>, i.e. <math|m<rsub|B>=m<rsub|B><around*|(|s
  \<Lambda\>|)>> and <math|g<rsub|B n>=g<rsub|B n><around*|(|s
  \<Lambda\>|)>>. In previous instance, <math|g<rsub|B
  j\<gtr\>4><around*|(|\<Lambda\>|)>=0>, but not keep vanishing when
  <math|s\<neq\>1>. By deriving to <math|s>, we can get differential
  equations for these coefficients, like <math|\<mathd\>g<rsub|B
  n>/\<mathd\>s=\<beta\><rsub|n><around*|(|g<rsub|B>;s \<Lambda\>|)>>, for
  some <math|\<beta\>> to be determined. But, it benefits a lot if
  <math|\<beta\>> is independent of <math|s \<Lambda\>>, so that the phase
  vector field is stable. This can be fulfilled by a re-scaling. In
  <math|S<around*|[|\<phi\><around*|(|x;\<mu\>|)>;\<mu\>|]>> for a general
  <math|\<mu\>>, we re-scale

  <\eqnarray*>
    <tformat|<table|<row|<cell|p>|<cell|\<rightarrow\>>|<cell|<wide|p|~>
    \<mu\> ;>>|<row|<cell|x>|<cell|\<rightarrow\>>|<cell|
    <wide|x|~>\<mu\><rsup|-1>>>|<row|<cell|\<phi\><around*|(|p|)>>|<cell|\<rightarrow\>>|<cell|<wide|\<phi\>|~><around*|(|<wide|p|~>|)>\<mu\><rsup|-3>
    ;>>|<row|<cell|m<rsub|B><rsup|2><around*|(|\<mu\>|)>>|<cell|\<rightarrow\>>|<cell|<wide|m|~><rsub|B><rsup|2><around*|(|\<mu\>|)>
    \<mu\><rsup|2> ;>>|<row|<cell|g<rsub|B
    n><around*|(|\<mu\>|)>>|<cell|\<rightarrow\>>|<cell|<wide|g|~><rsub|B
    n><around*|(|\<mu\>|)> \<mu\><rsup|4-n> ,>>>>
  </eqnarray*>

  so that <math|<wide|p|~>\<in\><around*|[|0,1|]>>, and that
  <math|S<around*|[|\<phi\><around*|(|x;\<mu\>|)>;\<mu\>|]>=S<around*|[|<wide|\<phi\>|~><around*|(|<wide|x|~>;1|)>;\<mu\>|]>>
  (Reminding that <math|\<mu\>> in <math|S> represents the <math|\<mu\>> in
  <math|m<rsub|B><around*|(|\<mu\>|)>> and
  <math|g<rsub|B><around*|(|\<mu\>|)>>), within which the integral always
  ranges over <math|<around*|[|0,1|]>>.

  Back to previous instance (therein the index <math|B> is dropped). Before
  integrating out, <math|<wide|m|~><rsup|2><around*|(|\<Lambda\>|)>=m<rsub|B><rsup|2><around*|(|\<Lambda\>|)>
  \<Lambda\><rsup|-2>>. After integrating out,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<wide|m|~><rsub|B><rsup|2><around*|(|s
    \<Lambda\>|)>>|<cell|=>|<cell|m<rsub|B><rsup|2><around*|(|s \<Lambda\>|)>
    <around*|(|s \<Lambda\>|)><rsup|-2>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<wide|m|~><rsub|B><rsup|2><around*|(|\<Lambda\>|)>
    \<Lambda\><rsup|2>-<wide|\<lambda\>|~><rsub|B><around*|(|\<Lambda\>|)>
    \<Lambda\><rsup|2> <around*|(|1-s<rsup|2>|)>|)> <around*|(|s
    \<Lambda\>|)><rsup|2>>>|<row|<cell|>|<cell|=>|<cell|<around*|(|<wide|m|~><rsub|B><rsup|2><around*|(|\<Lambda\>|)>-<wide|\<lambda\>|~><rsub|B><around*|(|\<Lambda\>|)>
    <around*|(|1-s<rsup|2>|)>|)> s<rsup|2> .>>>>
  </eqnarray*>

  Deriving to <math|s> on both sides, and then letting <math|s=1>, get

  <\equation*>
    \<Lambda\><frac|\<mathd\><wide|m|~><rsub|B><rsup|2><around*|(|\<Lambda\>|)>|\<mathd\>\<Lambda\>>=2
    <wide|m|~><rsub|B><rsup|2><around*|(|\<Lambda\>|)>-2<wide|\<lambda\>|~><rsub|B><around*|(|\<Lambda\>|)>=:\<beta\><rsub|2><around*|(|<wide|m|~><rsub|B><rsup|2>,<wide|\<lambda\>|~><rsub|B>|)>
    ,
  </equation*>

  where <math|\<mathd\><wide|m|~><rsup|2><rsub|B><around*|(|\<Lambda\>|)>/\<mathd\>\<Lambda\>>
  just represents the first order derivative of function
  <math|<wide|m|~><rsup|2><rsub|B><around*|(|#|)>>. In this differential
  equation, the <math|\<Lambda\>> is implicitly contained. The component of
  phase vector field, <math|\<beta\><rsub|2><around*|(|<wide|m|~><rsub|B><rsup|2>,<wide|\<lambda\>|~><rsub|B>|)>=2
  <wide|m|~><rsub|B><rsup|2>-2<wide|\<lambda\>|~><rsub|B>> is stable
  forsooth. Contrarily, if not perform the re-scaling, then
  <math|m<rsub|B><around*|(|s \<Lambda\>|)>=m<rsub|B><around*|(|\<Lambda\>|)>-\<lambda\><rsub|B><around*|(|\<Lambda\>|)>
  \<Lambda\><rsup|2> <around*|(|1-s<rsup|2>|)>>, which does make
  <math|\<beta\>> depend on <math|\<Lambda\>>, for which the phase vector is
  unstable.

  This is the RGE version 1. For further qualitative study, especially on
  stable point of RGE, c.f. QTF, section 12.4.

  <subsection|Stable Point>

  Consider the root of <math|\<beta\><rsub|n><around*|(|<wide|g|~><rsub|B>|)>>
  of all <math|n> (<math|n> dofs v.s. <math|n> equations), flowing through
  which any phase flow must be fixed onto it forever. This is the stable
  point, denoted by <math|<wide|g|~><rsub|B><rsup|\<ast\>>>. Consider the
  flow with <math|\<Lambda\>> decreasing,
  <math|<wide|g|~><rsub|B><rsup|\<ast\>>> is an attractor on <math|n>-axis of
  phase space as long as <math|\<partial\>\<beta\><rsub|n>/\<partial\><wide|g|~><rsub|B
  n><around*|(|g<rsup|\<ast\>><rsub|B>|)>\<gtr\>0>; and it repulses phase
  flow as long as <math|\<partial\>\<beta\><rsub|n>/\<partial\><wide|g|~><rsub|B
  n><around*|(|g<rsup|\<ast\>><rsub|B>|)>\<less\>0>. So, for the axis of
  phase on which it's an attractor, the information of the full theory is
  smeared out! Realizing this was Wilson's breakthrough (1970), as Wilson
  himself said so.

  <subsection|Digression: Effective Potential and Symmetry Breaking>

  <\definition>
    Effective potential <math|V<rsub|eff>> is defined as

    <\equation*>
      V<rsub|eff><around*|[|\<Phi\><around*|(|t|)>|]>\<assign\>lim<rsub|\<Lambda\>\<rightarrow\>0<rsup|+>>V<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>,g<rsub|B><around*|(|\<Lambda\>|)>|]>
      ,
    </equation*>

    where <math|\<Phi\><around*|(|t|)>\<assign\>\<phi\><around*|(|\<b-p\>=0,t|)>>
    and <math|V<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>,g<rsub|B><around*|(|\<Lambda\>|)>|]>>
    the potential in <math|S<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>,g<rsub|B><around*|(|\<Lambda\>|)>|]>>.
  </definition>

  By <math|><math|lim<rsub|\<Lambda\>\<rightarrow\>0<rsup|+>>S<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>,g<rsub|B><around*|(|\<Lambda\>|)>|]>>,
  a system with only momentum mode is obtained. Such a system is quantum
  mechanical rather than of QFT!

  The couplings <math|lim<rsub|\<Lambda\>\<rightarrow\>0<rsup|+>>
  g<rsub|B><around*|(|\<Lambda\>|)>> determine the shape of the effective
  potential. If the effective potential has the form like
  <math|<around*|(|\<Phi\><rsup|2>-a<rsup|2>|)><rsup|2>>, then it has three
  stable points: <math|\<Phi\>=0,\<pm\>a>. Instead of being <math|\<Phi\>=0>
  as usual, the minimal points, however, are <math|\<Phi\>=\<pm\>a>, around
  which there are two potential wells. The ground state of this quantum
  mechanical system is in one of the two potential wells, which are in
  symmetric positions.

  <subsection|Discussion: What's Renormalization for Wilson Effective
  Action?>

  For a Wilson effective action <math|S<around*|[|\<phi\><around*|(|x;\<Lambda\>|)>;\<Lambda\>|]>>,
  the S-matrix is directly calculated from the bared field instead of the
  renormalized, between which there's a <math|Z> factor. So, why is Wilson's
  approach, with bared field, to S-matrix valid? What's the role of
  renormalization in Wilson's approach?

  The motivation of renormalization of field is making all things that
  construct S-matrix, the observable, finite and well-defined. But, as a
  truncated action, everything in and given by Wilson effective action is
  finite and well-defined, even without renormalization. And, starting at the
  Wilson effective action with either a renormalized field (with the
  renormalization scheme independent of ad hoc renomralization scale, such as
  that in QTF) or a bared field will return an expression of S-matrix, with
  parameters the couplings of the Wilson effective action. Then, for both
  cases, the explicit values of these couplings can be fitted out by
  comparing with experiment, even though the fitted values in different cases
  are different. And then, for both cases, the prediction of further
  experiment follows. So we find that, <with|font-series|bold|for Wilson
  effective action, renormalization is nothing but a re-paramenterization,
  thus far from essential.> This is unlike the case of non-truncated action,
  where, without renormalization, everything is infinite and ill-defined, so
  that renormalization plays an essential role.

  <with|color|red|But, this prescription involves obstacle: from it S-matrix
  cannot be calculated out, since the bared couplings are so large that
  perturbation breaks down.>

  <with|color|dark blue|The fact is that the coupling observed at infrared
  limit is tiny. That is, the renormalized coupling with renormalization
  scale at <math|\<b-p\>\<approx\>\<b-0\>> is tiny. And it is on this that
  perturbation calculation of S-matrix is taken!>

  <subsection|Problems with Wilson's Approach>

  Perturbation calculation can only be carried out when
  <math|\<lambda\><rsub|B><around*|(|\<Lambda\>|)>> is small. In general
  case, however, <math|\<lambda\><rsub|B><around*|(|\<Lambda\>|)>>, the
  bared, is extremely large. This breaks perturbation down; but it is the
  only way we know to perform some calculation.

  <subsection|Summary>

  <\enumerate>
    <item>If coupling between different momentum-modes exists, we must keep
    in mind that the actions of large spatial scale, and that containing also
    smaller spatial scale modes, are different (L.Landau's mistake).

    <item>Wilson's approach is manipulating the bared couplings.

    <item>Beta function is independent of ultraviolet cut-off.

    <item>The existence of stable point on phase space smears out the
    information of the full theory on those bared couplings for which the
    stable point is an attractor.

    <item>Large bared couplings break perturbation down. But, practically,
    bared couplings are usually extremely large.
  </enumerate>

  <section|RGE in Callan-S? Approach>

  This section bases on the book asdf. But the deriving is different; and
  many crucial conceptions are clarified. Don't be nerving, nowhere will we
  do renormalization explicitly.

  Keep in mind that, in this section, only one full theory is considered.
  This is crucial when analyze the relation of dependence.

  <\notation>
    Denote the couplings in Wilson effective actions
    <math|S<around*|[|\<phi\><rsub|><around*|(|x;\<Lambda\>|)>;\<Lambda\><rsub|>|]>>
    by <math|<around*|{|g<rsub|B 1><around*|(|\<Lambda\>|)>,g<rsub|B
    2><around*|(|\<Lambda\>|)>,\<ldots\>|}>> including the bared mass square.
  </notation>

  <subsection|Renormalization Scheme>

  <subsubsection|Renormalized Vertex Function><label|section: Renormalized
  Vertex Function>

  <\definition>
    Renormalized vertex function is DEFINED by

    <\equation*>
      \<Gamma\><rsup|<around*|(|N|)>>\<assign\>Z<rsup|-N/2>
      <around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>
      \<Gamma\><rsub|B><rsup|<around*|(|N|)>><around*|(|\<b-p\><rsub|1>,\<ldots\>,\<b-p\><rsub|N>;g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>
      ,
    </equation*>

    such that, for some <math|\<mu\>>, called renormalization scale,

    <\enumerate>
      <item><math|\<Gamma\><rsup|<around*|(|2|)>>\|<rsub|<around*|\||\<b-p\>|\|>=\<mu\>>=0>;

      <item><math|<frac|\<partial\>\<Gamma\><rsup|<around*|(|2|)>>|\<partial\><around*|(|p<rsup|2>|)>>\|<rsub|<around*|\||\<b-p\>|\|>=\<mu\>>=1>
      ;

      <item><math|\<Gamma\><rsup|<around*|(|4|)>>\|<rsub|<around*|\||\<b-p\><rsub|1>+\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|2>+\<b-p\><rsub|3>|\|>=<around*|\||\<b-p\><rsub|3>+\<b-p\><rsub|4>|\|>=\<mu\>>=g<rsub|4>>
      ;

      <item>......if any.
    </enumerate>

    This renormalization scheme is called <em|SP (symmetric point)>. The key
    is that an ad hoc parameter is introduced in.
  </definition>

  The dependence of <math|\<Gamma\><rsup|<around*|(|N|)>>> is to be
  determined. This renormalization scheme must be regarded as formal
  relations, rather than numerical. Hence it becomes equations relating
  <math|g<rsub|B><around*|(|\<Lambda\>|)>>, <math|g>, <math|\<Lambda\>>, and
  <math|\<mu\>>, with <math|<around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>,\<mu\>|)>>
  on lhs, and <math|g> on rhs.<\footnote>
    Such as

    <\equation*>
      \<Gamma\><rsub|R><rsup|<around*|(|4|)>>\|<rsub|<around*|\||\<b-p\><rsub|1>+\<b-p\><rsub|2>|\|>=\<ldots\>=\<mu\>>=Z<rsup|n/2>
      <around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>
      \<Gamma\><rsup|<around*|(|N|)>><around*|(|\<b-p\><rsub|1>,\<ldots\>,\<b-p\><rsub|N>;g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>\|<rsub|<around*|\||\<b-p\><rsub|1>+\<b-p\><rsub|2>|\|>=\<ldots\>=\<mu\>>=g<rsub|4>
    </equation*>
  </footnote> Thus, <math|g<rsub|n>=g<rsub|n><around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>,\<mu\>|)>>.<\footnote>
    In addition, <math|g<rsub|B><around*|(|\<Lambda\>|)>> can also be
    represented by <math|g>, <math|\<Lambda\>>, and <math|\<mu\>>, such as
    <math|m<rsub|B><rsup|2>=\<mu\><rsup|2>+m<rsup|2>+\<Pi\><rsub|loop><around*|(|g,\<Lambda\>|)>>.
  </footnote> Or say, to give the same value of bared couplings
  <math|g<rsub|B>>s, which is independent of <math|\<mu\>>, different value
  of <math|\<mu\>> shall associate different value of <math|g>s. With this
  realization, the <math|\<Gamma\><rsup|<around*|(|N|)>>> depends on
  <math|g>s, <math|\<Lambda\>>, <math|\<mu\>>, and
  <math|<around*|{|\<b-p\><rsub|j>|}>>, abbreviate for
  <math|<around*|(|\<b-p\><rsub|1>,\<ldots\>,\<b-p\><rsub|N>|)>>.<\footnote>
    Suppose we know the renormalization factor of field, <math|Z<rsup|1/2>>,
    can be determined by <math|g>s.
  </footnote>

  <\remark>
    There are other renormalization schemes. Apart from SP, one of the most
    conventional is <em|MS (minimal subtraction)>.
  </remark>

  <subsubsection|Old Renormalization V.S. BPHZ>

  Old renonmalization procedure and BPHZ are quite different.

  <\itemize>
    <item>Old renonmalization starts at <math|S<around*|[|\<phi\>;\<Lambda\>|]>=-<big|int><frac|1|2><around*|(|\<partial\>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+g<rsub|B2><around*|(|\<Lambda\>|)>\<phi\><rsup|2><around*|(|x;\<Lambda\>|)>+\<cdots\>>,
    from which a <strong|direct> calculation gives
    <math|\<Gamma\><rsub|B><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>>.
    This bared vertex function, as <math|\<Lambda\>\<rightarrow\>+\<infty\>>,
    is divergent. Then, the renormalized <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g<around*|(|g<rsub|B>,\<Lambda\>|)>,\<Lambda\>|)>\<assign\>Z<rsup|-N/2>\<Gamma\><rsub|B><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>>,
    so that a given renormalization scheme (a set of renormalization
    conditions) is satisfied.

    <item>On the contrary, BPHZ starts at
    <math|S<around*|[|\<phi\>,\<Lambda\>|]>=-<big|int><frac|1|2><around*|(|\<partial\>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+g<rsub|2>\<phi\><rsup|2><around*|(|x;\<Lambda\>|)>+\<cdots\>+S<rsub|C.T.><around*|[|\<phi\>,g,\<Lambda\>|]>>,
    from which a direct calculation gives
    <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g,\<Lambda\>|)>>,
    without via <math|\<Gamma\><rsup|<around*|(|N|)>><rsub|B>> as well as
    <math|g<rsub|B>>, so that the same given renormalization is satisfied.
  </itemize>

  But,

  <\theorem>
    Let <math|\<delta\>g<around*|(|\<Lambda\>,\<mu\>|)>> the coupling of
    counter-term. Then, the <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g,\<Lambda\>|)>>
    by old renormalization procedure by <math|g<rsub|B><around*|(|\<Lambda\>|)>=g<around*|(|\<Lambda\>,\<mu\>|)>+g<around*|(|\<Lambda\>,\<mu\>|)>+\<delta\>g<around*|(|\<Lambda\>,\<mu\>|)>>
    and that by BPHZ are equal, so are their renormalized couplings
    <math|g>s.
  </theorem>

  <\proof>
    Indeed,

    <\eqnarray*>
      <tformat|<table|<row|<cell|>|<cell|>|<cell|-<big|int><frac|1|2><around*|(|\<partial\><sqrt|Z>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+g<rsub|B2><around*|(|\<Lambda\>|)><around*|(|<sqrt|Z>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+\<cdots\>>>|<row|<cell|>|<cell|=>|<cell|-<big|int><frac|1|2><around*|(|\<partial\>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+g<rsub|2>\<phi\><rsup|2><around*|(|x;\<Lambda\>|)>+\<cdots\>+S<rsub|C.T.><around*|[|\<phi\>,g,\<Lambda\>|]>;>>>>
    </eqnarray*>

    and remind that

    <\equation*>
      \<Gamma\><rsup|<around*|(|N|)>>\<assign\>Z<rsup|-N/2>
      <around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>
      \<Gamma\><rsub|B><rsup|<around*|(|N|)>><around*|(|\<b-p\><rsub|1>,\<ldots\>,\<b-p\><rsub|N>;g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>
      ,
    </equation*>

    asdf
  </proof>

  In this note, as well as in many textbooks, old renonmalization is
  employed. But parallel-ly thinking in BPHZ brings fruits.

  <subsubsection|Relations with S-matrix>

  <\remark>
    [Relation between <math|\<Gamma\><rsup|<around*|(|N|)>>> and S-matrix]

    Connected S-matrix, thus the complete S-matrix, can be calculated from
    <math|\<Gamma\><rsup|<around*|(|N|)>>> as follow. First draw all possible
    tree level diagrams, for any situation of scattering given. Associating
    each in-going extra-leg either by <math|u<rsub|l><rsup|\<ast\>><around*|(|\<b-p\>|)>>
    or by <math|u<rsub|l><rsup|\<ast\>><around*|(|\<b-p\>|)>
    <around*|[|lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>
    <around*|(|p<rsup|2>+m<rsup|2>|)> \<Gamma\><rsup|<around*|(|2|)>>-1|]>>
    (loops on extra-legs). And the same for out-going extra-legs. Associating
    each vertex by the corresponding vertex function
    <math|\<Gamma\><rsup|<around*|(|N|)>>>. Summing over all such tree level
    diagrams get the connected S-matrix. (C.f. the diagram of explaining
    <math|\<Gamma\><rsup|<around*|(|N|)>>>.)
  </remark>

  <\remark>
    <strong|For any value of <math|\<mu\>> given, the <math|\<mu\>> dependent
    renormalized couplings <math|g> can be fitted out from experimental
    results of particle scattering via this relation between
    <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,g,\<Lambda\>|)>>
    and S-matrix.>

    From this point, the renormalized couplings, <math|g>, shall contain
    <strong|no> physical meaning. This is a non-trivial realization on the
    renormalized couplings, or generally, couplings: <strong|they are purely
    mathematical, rather than physical.> asdf

    <strong|It seems that field-theory calculation is nothing but a
    parameterization of S-matrix, but always with finite effective parameters
    for each specie of particle, so that the fitting will never be
    redundant.>
  </remark>

  <\remark>
    [Relation between <math|S<around*|[|\<phi\>;\<Lambda\>|]>> with bared
    couplings and S-matrix]

    No direct relation. They are connected only implicitly via
    <math|\<Gamma\><rsup|<around*|(|N|)>>>. But since
    <math|\<Gamma\><rsup|<around*|(|N|)>>> depends on explicit
    renormalization scheme, <strong|the connection between
    <math|S<around*|[|\<phi\>;\<Lambda\>|]>> with bared couplings and
    S-matrix then depends on explicit renormalization scheme.>
  </remark>

  <subsection|Relations of Dependence>

  Denote

  <\equation*>
    <wide|g|~><rsub|n>\<assign\> \<mu\><rsup|n-4> g<rsub|n> ,
  </equation*>

  <math|<wide|g|~>>s are thus dimensionless. Consider a fixed renormalization
  scale <math|\<mu\><rsup|0>>, on which the dimensionless renormalized
  couplings are <math|<wide|g|~><rsup|0>>, which thus is also fixed. And,
  from <math|\<mu\><rsup|0>> and <math|<wide|g|~><rsup|0>>, we can recover
  the <math|g<rsub|B><around*|(|\<Lambda\>|)>> at any <math|\<Lambda\>>
  given, as discussed. Then consider another <math|\<mu\>>, on which are
  <math|<wide|g|~>>. Originally <math|<wide|g|~>> depends on
  <math|g<rsub|B><around*|(|\<Lambda\>|)>>, <math|\<Lambda\>> and
  <math|\<mu\>>, which, then, depends on <math|<wide|g|~><rsup|0>>,
  <math|\<mu\><rsup|0>>, <math|\<Lambda\>> and <math|\<mu\>>. Since
  <math|<wide|g|~>> is dimensionless, it must be of the form
  <math|<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>,\<mu\><rsup|0>/\<Lambda\>|)>>.

  The <math|Z>, originally depends on <math|g<rsub|B><around*|(|\<Lambda\>|)>>,
  <math|\<Lambda\>>, and <math|\<mu\>>, then depends on <math|<wide|g|~>>,
  <math|\<mu\>>, <math|\<Lambda\>>. Define

  <\equation*>
    \<zeta\>\<assign\><frac|Z\|<rsub|\<mu\>>|Z\|<rsub|\<mu\><rsup|0>>>
  </equation*>

  <math|\<zeta\>> depends on <math|<wide|g|~>>, <math|\<mu\>>,
  <math|<wide|g|~><rsup|0>>, <math|\<mu\><rsup|0>>, <math|\<Lambda\>>, which,
  because of dimensionless, must be of the form
  <math|\<zeta\><around*|(|<wide|g|~>,<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>,\<mu\><rsup|0>/\<Lambda\>|)>>.
  The motivation of this definition is to be clarified.

  As a summary,

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Gamma\><rsup|<around*|(|N|)>>>|<cell|=>|<cell|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>,\<Lambda\>|)>
    , or <wide|g|~><rsup|0> and \<mu\><rsup|0>
    ;>>|<row|<cell|\<zeta\>>|<cell|=>|<cell|\<zeta\><around*|(|<wide|g|~>,<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>,\<mu\><rsup|0>/\<Lambda\>|)>
    ,>>>>
  </eqnarray*>

  wherein

  <\equation*>
    <wide|g|~>=<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>,\<mu\><rsup|0>/\<Lambda\>|)>
    ;
  </equation*>

  Since the bared vertex function is independent of renormalization
  scale<\footnote>
    Indeed, remind that <math|\<Gamma\><rsup|<around*|(|N|)>><rsub|B>> is
    obtained by direct calculation from <math|S<around*|[|\<phi\>;\<Lambda\>|]>=-<big|int><frac|1|2><around*|(|\<partial\>\<phi\>|)><rsup|2><around*|(|x;\<Lambda\>|)>+g<rsub|B2><around*|(|\<Lambda\>|)>\<phi\><rsup|2><around*|(|x;\<Lambda\>|)>+\<cdots\>>
    wherein <math|\<mu\>> is formally absent.
  </footnote>, we get

  <\equation*>
    \<zeta\><rsup|-N/2> \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>,\<Lambda\>|)>=\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>,\<Lambda\>|)>
  </equation*>

  This relation holds for any <math|\<Lambda\>>. Thus, consider a series of
  Wilson effective actions, with <math|\<Lambda\>> increasing without an
  upper bound. As is known, when <math|\<Lambda\>\<rightarrow\>+\<infty\>>,
  <math|g<rsub|B><around*|(|\<Lambda\>|)>> and
  <math|Z<around*|(|g<rsub|B><around*|(|\<Lambda\>|)>,\<Lambda\>|)>> become
  divergent. But, <math|<wide|g|~>>, <math|<wide|g|~><rsup|0>>,
  <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>,\<Lambda\>|)>>,
  <math|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>,\<Lambda\>|)>>,
  thus <math|\<zeta\>> are all finite. Thus, by the limit
  <math|\<Lambda\>\<rightarrow\>+\<infty\>>, we get

  <\equation*>
    \<zeta\><rsup|-n/2> \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>|)>=\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>
    ;
  </equation*>

  and

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<Gamma\><rsup|<around*|(|N|)>>>|<cell|=>|<cell|\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>|)>
    , or <wide|g|~><rsup|0> and \<mu\><rsup|0>;>>|<row|<cell|\<zeta\>>|<cell|=>|<cell|\<zeta\><around*|(|<wide|g|~>,<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>
    ,>>>>
  </eqnarray*>

  wherein

  <\equation*>
    <wide|g|~>=<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>
    .
  </equation*>

  That is, the divergent <math|\<Lambda\>> diminishes in finite quantities.

  <subsection|Callan-S? Equation>

  Applying <math|\<mu\> \<partial\>/\<partial\>\<mu\>> on both sides of

  <\equation*>
    \<zeta\><rsup|-N/2><around*|(|<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>,<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>,\<mu\>|)>=\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>
  </equation*>

  and setting <math|\<mu\>=\<mu\><rsup|0>> gives<\footnote>
    <math|<around*|(|\<partial\>/\<partial\>\<mu\><rsup|0>|)>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>>
    represents <math|\<partial\><rsub|#3>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>>.
  </footnote>

  <\equation*>
    <around*|{|-<frac|N|2> \<gamma\>+\<beta\><rsub|n>
    <frac|\<partial\>|\<partial\><wide|g|~><rsup|0><rsub|n>>+\<mu\><rsup|0>
    <frac|\<partial\>|\<partial\>\<mu\><rsup|0>>|}>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>=0
    ,
  </equation*>

  where

  <\eqnarray*>
    <tformat|<table|<row|<cell|\<beta\><rsub|n>>|<cell|\<assign\>>|<cell|\<mu\>
    <frac|\<partial\>|\<partial\>\<mu\>><wide|g|~><rsub|n><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>\|<rsub|\<mu\>=\<mu\><rsup|0>>>>|<row|<cell|>|<cell|=>|<cell|\<partial\><rsub|#2><wide|g|~><rsub|n><around*|(|<wide|g|~><rsup|0>,1|)>
    ;>>|<row|<cell|\<gamma\>>|<cell|\<assign\>>|<cell|\<mu\>
    <frac|\<partial\>|\<partial\>\<mu\>>\<zeta\><around*|(|<wide|g|~><around*|(|<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>,<wide|g|~><rsup|0>,\<mu\>/\<mu\><rsup|0>|)>\|<rsub|\<mu\>=\<mu\><rsup|0>>
    >>|<row|<cell|>|<cell|=>|<cell|\<partial\><rsub|#1>\<zeta\><around*|(|<wide|g|~><rsup|0>,<wide|g|~><rsup|0>,1|)>
    \<partial\><rsub|#2><wide|g|~><around*|(|<wide|g|~><rsup|0>,1|)>+\<partial\><rsub|#3>\<zeta\><around*|(|<wide|g|~><rsup|0>,<wide|g|~><rsup|0>,1|)>
    .>>>>
  </eqnarray*>

  Since <math|\<mu\><rsup|0>> is arbitrary, then replace
  <math|\<mu\><rsup|0>\<rightarrow\>\<mu\>> at this end,

  <\equation*>
    <around*|{|-<frac|N|2> \<gamma\><around*|(|<wide|g|~>|)>+\<beta\><rsub|n><around*|(|<wide|g|~>|)>
    <frac|\<partial\>|\<partial\><wide|g|~><rsub|n>>+\<mu\>
    <frac|\<partial\>|\<partial\>\<mu\>>|}>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>;<wide|g|~>,\<mu\>|)>=0
    ,
  </equation*>

  where the dependence of <math|\<beta\>> and <math|\<gamma\>> have been
  explicitly displayed. It's manifest that <math|\<beta\><rsub|n>> and
  <math|\<gamma\>> depends only on <math|<wide|g|~>>. Thus, again, the phase
  vector field of <math|<wide|g|~>> is stable. This equation is called
  <em|Callan-S? equation>.

  <subsection|Solution>

  As a linear PDE, the solution is straight-forward.

  <\theorem>
    Callan-S? equation with initial condition
    <math|<wide|g|~><around*|(|\<mu\><rsup|0>|)>=<wide|g|~><rsup|0>> is

    <\equation*>
      \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><around*|(|\<mu\>/\<mu\><rsup|0>|)>,\<mu\>|)>=exp<around*|[|<frac|N|2>
      <big|int><rsub|1><rsup|\<mu\>/\<mu\><rsup|0>>\<gamma\><around*|(|<wide|g|~><around*|(|x|)>|)><frac|\<mathd\>x|x>|]>\<times\>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>
      ,
    </equation*>

    where the <math|<wide|g|~><around*|(|\<mu\>/\<mu\><rsup|0>|)>> is the
    solution of characteristic

    <\equation*>
      \<mu\><frac|\<partial\><wide|g|~><rsub|n>|\<partial\>\<mu\>>=\<beta\><rsub|n><around*|(|<wide|g|~>|)>
      .
    </equation*>
  </theorem>

  Conveniently, <math|\<rho\>\<assign\>\<mu\>/\<mu\><rsup|0>>. The solution
  can be re-arranged as

  <\equation*>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>=exp<around*|[|-<frac|N|2>
    <big|int><rsub|1><rsup|\<rho\>>\<gamma\><around*|(|<wide|g|~><around*|(|x|)>|)><frac|\<mathd\>x|x>|]>\<times\>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><around*|(|\<rho\>|)>,\<rho\>
    \<mu\><rsup|0>|)> .
  </equation*>

  Consider the rhs only. By dimension analysis, it is a homogeneous function,
  on <math|\<Gamma\><rsup|<around*|(|N|)>>>'s <math|#1> and <math|#3>. Thus,
  by Euler formula, it becomes

  <\equation*>
    \<rho\><rsup|N+d-N d/2>\<times\>exp<around*|[|-<frac|N|2>
    <big|int><rsub|1><rsup|\<rho\>>\<gamma\><around*|(|<wide|g|~><around*|(|x|)>|)><frac|\<mathd\>x|x>|]>\<times\>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>/\<rho\>|}>,<wide|g|~><around*|(|\<rho\>|)>,\<mu\><rsup|0>|)>
    ,
  </equation*>

  where the power factor <math|N+d-N d/2> comes from the dimension of
  <math|\<Gamma\><rsup|<around*|(|N|)>>>, with <math|d> denoting space-time
  dimension. Combining with the lhs, and replacing
  <math|\<b-p\><rsub|j>\<rightarrow\>\<rho\> \<b-p\><rsub|j>>, get

  <\equation*>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<rho\>
    \<b-p\><rsub|j>|}>,<wide|g|~><rsup|0>,\<mu\><rsup|0>|)>=\<rho\><rsup|N+d-N
    d/2>\<times\>exp<around*|[|-<frac|N|2>
    <big|int><rsub|1><rsup|\<rho\>>\<gamma\><around*|(|<wide|g|~><around*|(|x|)>|)><frac|\<mathd\>x|x>|]>\<times\>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><around*|(|\<rho\>|)>,
    \<mu\><rsup|0>|)> .
  </equation*>

  This form of the solution is the one usually employed.

  <subsection|Stable Point and Scaling Rule>

  Consider the root of <math|\<beta\><rsub|n><around*|(|<wide|g|~>|)>> of all
  <math|n> (<math|n> dofs v.s. <math|n> equations), flowing through which any
  phase flow must be fixed onto it forever. This is the stable point, denoted
  by <math|<wide|g|~><rsup|\<ast\>>>. And if initial condition
  <math|<wide|g|~><rsup|0>> is on this point, then
  <math|<wide|g|~><around*|(|\<rho\>|)>\<equiv\><wide|g|~><rsup|\<ast\>>>.
  The solution of Callan-S? equation thus becomes

  <\equation*>
    \<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<rho\>
    \<b-p\><rsub|j>|}>,<wide|g|~><rsup|\<ast\>>,\<mu\>|)>=\<rho\><rsup|N+d-N
    d/2-N \<gamma\><rsup|\<ast\>>/2>\<times\>\<Gamma\><rsup|<around*|(|N|)>><around*|(|<around*|{|\<b-p\><rsub|j>|}>,<wide|g|~><rsup|\<ast\>>,
    \<mu\>|)> .
  </equation*>

  This is the scaling rule of renormalized vertex function: one the lhs, the
  momenta of particles on legs, <math|\<b-p\><rsub|j>>, are uniformly
  enlarged by a factor <math|\<rho\>> comparing with that in the rhs, as a
  result of which, one the rhs, <math|\<Gamma\><rsup|<around*|(|N|)>>> is
  scaled by a factor <math|\<rho\><rsup|N+d-N d/2-N
  \<gamma\><rsup|\<ast\>>/2>>, leaving all others in-variant. The factor
  <math|\<gamma\><rsup|\<ast\>>> is called <em|anomaly dimension>, which is
  manifest in this equation.

  <subsection|Problems in Callan-Symanzik Approach>

  <\enumerate>
    <item>Modifying renormalization scheme away from mass-shell, as in
    <reference|section: Renormalized Vertex Function>, can make the
    expression of connected S-matrix ill-defined, as <reference|section:
    Loops on Extra-legs and Renormalization of Gamma-2> shows. So, if it is
    S-matrix that is the observable, as the case in particle physics, then
    this fact evokes a problem.

    <item>Physically, it is what we observed that is independent of the ad
    hoc renormalization scale, <math|\<mu\>>. So, the condition shall be
    <math|\<mu\> <around*|(|\<partial\>/\<partial\>\<mu\>|)> S<rsub|\<beta\>
    \<alpha\>>=0>, where <math|S<rsub|\<beta\> \<alpha\>>> the S-matrix from
    <math|\| \<alpha\>\<rangle\>> to <math|\| \<beta\>\<rangle\>>. Because of
    the direct relation between <math|S<rsub|\<beta\> \<alpha\>>> and
    <math|\<Gamma\><rsup|<around*|(|N|)>>=Z<rsup|-N/2>
    \<Gamma\><rsub|B><rsup|<around*|(|N|)>>>, it is hard to realized why
    <math|\<mu\> <around*|(|\<partial\>/\<partial\>\<mu\>|)> S<rsub|\<beta\>
    \<alpha\>>=0> and <math|\<mu\> <around*|(|\<partial\>/\<partial\>\<mu\>|)>
    \<Gamma\><rsup|<around*|(|N|)>><rsub|B>=0> are held simultaneously.
  </enumerate>

  <subsection|Summary>

  <\enumerate>
    <item>Callan-S?'s approach manipulates the renormalized quantities, for
    which renormalization is artificially defined. The RGE is derived by
    varying the ad hoc renormalization scale.

    Contrarily, Wilson's approach manipulates the bared quantities, for which
    the RGE is derived by varying the ultraviolet cut-off.

    <item>The bared theory of Callan-S? approach is the full theory.

    <item><strong|Renormalized couplings are purely mathematical, rather than
    physical. It seems that field-theory calculation is nothing but a
    parameterization of S-matrix.>asdf

    <item>Beta function is independent of ad hoc renormalization
    scale<\footnote>
      only when the ultraviolet cut-off tends to infinity.
    </footnote>.

    <item>The stable point in Callan-S?'s approach furnishes the re-scaling
    rule in a manifest way.

    <item>Modifying renormalization scheme away from mass-shell can make the
    expression of connected S-matrix ill-defined. It becomes a problem if the
    observable is S-matrix, as the case in particle physics.

    <item>Physically it shall be <math|\<mu\>
    <around*|(|\<partial\>/\<partial\>\<mu\>|)> S<rsub|\<beta\> \<alpha\>>=0>
    instead of <math|\<mu\> <around*|(|\<partial\>/\<partial\>\<mu\>|)>
    \<Gamma\><rsup|<around*|(|N|)>><rsub|B>=0>, unless they are equivalent.
    But whether they are equivalent or not is far from manifest.
  </enumerate>

  <section|RGE of Bared Theory>

  <section|Infrared/Ultraviolet Attractor and Asymptotic Freedom>

  In this section, we focus on the beta function only. Thus all discussions
  are applied to all RGE approaches.

  <subsection|One Parameter Model>

  For simplicity, consider the massless <math|\<lambda\> \<phi\><rsup|4>>
  model, which has a unique coupling. The dimensionless coupling is
  <math|<wide|\<lambda\>|~>>.

  <big-figure|<with|gr-mode|<tuple|group-edit|move>|gr-frame|<tuple|scale|1cm|<tuple|0.5gw|0.5gh>>|gr-geometry|<tuple|geometry|1par|0.6par>|<graphics||<line|<point|-6|4>|<point|-6.0|-3.0>>|<line|<point|-6.0|0.0>|<point|6.0|0.0>>|<text-at|<math|\<beta\><around*|(|<wide|\<lambda\>|~>|)>>|<point|-5.69709948405874|3.77784759888874>>|<text-at||<point|-6.0|-0.459370083595614>>|<text-at|<math|0>|<point|-6.39137|-0.218448>>|<text-at|<math|<wide|\<lambda\>|~>>|<point|6.30872|-0.235382>>|<spline|<point|-6.54377|-1.03125>|<point|-6.0|-0.0303546486376713>|<point|-3.24174824712264|-1.25138907262865>|<point|0.703747188781585|0.57742426246858>|<point|4.29364003175023|-1.04818759095118>|<point|5.74991731710544|2.28770339992062>>|<text-at|<math|\<lambda\><rsup|\<ast\>><rsub|1>>|<point|-5.78177|0.273614>>|<text-at|<math|\<lambda\><rsup|\<ast\>><rsub|2>>|<point|-1.39600671517397|0.22281350575473>>|<text-at|<math|\<lambda\><rsup|\<ast\>><rsub|3>>|<point|2.02455478502447|0.239746962958063>>|<text-at|<math|\<lambda\><rsup|\<ast\>><rsub|4>>|<point|4.53071200687921|0.13814735745469>>>>|A
  typical shape of beta function with asymptotic freedom.>

  <\problem>
    What's the relation between the \ Callan-S?'s ad hoc <math|\<mu\>> and
    the word \Pinfrared\Q or \Pultraviolet\Q?
  </problem>

  <subsection|Generalize to Multi-parameter Theory>

  How???

  <section|Residual Problems>

  <subsection|The <math|\<gamma\>> in Scaling Rule>

  Consider a ``standard'' path-integral (in
  <math|<around*|(|4-\<epsilon\>|)>>-dimension) with
  <math|<around*|{|\<b-p\><rsub|j>/\<mu\>|}>\<sim\>\<b-1\>> (for how-to, see
  note asdf):

  <\equation*>
    <big|int>D<around*|[|<wide|\<phi\>|~>|]> exp<around*|(|\<mathi\>
    <wide|S|~><around*|[|<sqrt|Z<around*|(|<wide|g|~><rsub|B><around*|(|\<mu\>|)>|)>>
    <wide|\<phi\>|~>;<wide|g|~><rsub|B><around*|(|\<mu\>|)>|]>asdf|)>
    <wide|\<phi\>|~><around*|(|<wide|\<b-p\>|~><rsub|1>\<sim\>\<b-1\>|)>
    \<cdots\> <wide|\<phi\>|~><around*|(|<wide|\<b-p\>|~><rsub|N>\<sim\>\<b-1\>|)>
    .
  </equation*>

  Action <math|<wide|S|~>> is explicity independent of <math|\<mu\>>. (What
  about the <math|<wide|S|~><rsub|CT>>???) Thus, with this ``standard'', the
  information of energy scale of <math|\<mu\>>
  (<math|\<sim\><around*|{|<wide|\<b-p\>|~><rsub|j>|}>>) (or say, of lab) is
  absent. This quantity is originally redundant. If we want to discuss
  <math|\<b-p\><rprime|'><rsub|j>=s \<b-p\><rsub|j>>, then by by tuning
  <math|\<mu\><rprime|'>=s \<mu\>>, the path-integral re-gains its
  ``standard'' form, only with coupling <math|<wide|g|~><around*|(|\<mu\>|)>>
  changes with <math|\<mu\>>. Verily, the variation of <math|<wide|g|~>> by
  variation of <math|\<mu\>>, the energy scale of lab, is plausible. Note
  that, this dependence may not only due to the relation between the
  ``standard'' <math|<wide|g|~>> and the realistic <math|g>, but also to the
  fact that <math|g> depends on energy scale of lab. For instance, at lower
  energy scale of lab, the contribution from W-boson is diminishing, reducing
  to a 4-fermion-vertex. Correspoingly, <math|<wide|g|~>> must
  change.<\footnote>
    Another instance is considering renormalization of
    <math|<wide|\<Gamma\>|~><rsup|<around*|(|4|)>>>, say, in <math|\<lambda\>
    \<phi\><rsup|4>>-model. The renormalization condition can be
    <math|<wide|\<Gamma\>|~><rsup|<around*|(|4|)>><around*|(|<around*|{|<wide|\<b-p\>|~><rsub|j>|}>=\<b-1\>|)>=<wide|g|~><rsub|4>>.
    In this way, <math|<wide|g|~><rsub|4>> depends on <math|\<mu\>>.
  </footnote>

  Vertex functin <math|<wide|\<Gamma\>|~><rsup|<around*|(|N|)>>> will be
  explicitly independent of <math|\<mu\>>. Indeed,
  <math|<wide|\<Gamma\><rsup|>|~><rsup|<around*|(|N|)>>> is calculated from
  the ``standard'' path-integral, which is explicitly independent of
  <math|\<mu\>>. Consequently, <math|<wide|\<Gamma\><rsup|>|~><rsup|<around*|(|N|)>>>
  depends only on <math|<wide|g|~><around*|(|\<mu\>|)>>, as the calculation
  of <math|\<Gamma\><rsup|\<mu\>><around*|(|q;e|)>> (vertex function is
  <math|e \<gamma\><rsup|\<mu\>>+\<Gamma\><rsup|\<mu\>><around*|(|q;e|)>> at
  one loop order) in QTF, section 11.3.

  Now declare the problem:

  <\question>
    Suppose we have calculated <math|<wide|\<Gamma\>|~><rsup|<around*|(|N|)>><around*|(|<around*|{|<wide|\<b-p\><rsub|j>|~>|}>\<sim\>\<b-1\>,<wide|g|~><around*|(|\<mu\>|)>|)>>.
    We wonder the <math|<wide|\<Gamma\>|~><rsup|<around*|(|N|)>><around*|(|<around*|{|<wide|\<b-p\><rsub|j>|~>|}>\<sim\>s
    \<b-1\>,<wide|g|~><around*|(|\<mu\>|)>|)>>. As discussed, it is
    equivalent to change <math|<wide|g|~><around*|(|\<mu\>|)>> to
    <math|<wide|g|~><around*|(|s \<mu\>|)>>. Thus,

    <\equation*>
      <wide|\<Gamma\>|~><rsup|<around*|(|N|)>><around*|(|<around*|{|<wide|\<b-p\><rsub|j>|~>|}>\<sim\>s
      \<b-1\>,<wide|g|~><around*|(|\<mu\>|)>|)>=<wide|\<Gamma\>|~><rsup|<around*|(|N|)>><around*|(|<around*|{|<wide|\<b-p\><rsub|j>|~>|}>\<sim\>
      \<b-1\>,<wide|g|~><around*|(|s \<mu\>|)>|)> .
    </equation*>

    So, where is the anomaly dimension? (The ``non-anomaly'' scaling is
    caused by the relation between the ``standard''
    <math|<wide|\<Gamma\>|~><rsup|<around*|(|N|)>>> and the realistic
    <math|\<Gamma\><rsup|<around*|(|N|)>>>.)
  </question>

  <\answer>
    \;

    <\enumerate>
      <item>First, we conclude that it is the bared coefficient
      <math|g<rsub|B>> that is <math|\<mu\>> independent. To see this,
      consider the renormalizaition of <math|g<rsub|4>> of <math|\<lambda\>
      \<phi\><rsup|4>> model. The renormalization condition can be the
      following. Set an energy scale of lab <math|\<mu\><rsub|0>>. Then let
      <math|g<rsub|4>> be the measured value of
      <math|\<Gamma\><rsup|<around*|(|4|)>>> when the prepared particles have
      momenta <math|\<mu\><rsub|0>> (in symmetric point). Suppose we want to
      predict the measured value of <math|\<Gamma\><rsup|<around*|(|4|)>>> at
      <math|<around*|{|\<b-p\><rsub|j>|}>\<sim\>s \<mu\><rsub|0>>. In the
      ``standard'' landscape, this is to calculate
      <math|<wide|g|~><rsub|4><around*|(|s \<mu\><rsub|0>|)>>. How to
      calculate <math|\<Gamma\><rsup|<around*|(|4|)>>>? At 1-loop level, we
      first formally give the <math|\<Gamma\><rsup|<around*|(|4|)>>>, as

      <\equation*>
        \<Gamma\><rsup|<around*|(|4|)>><around*|(|\<b-p\><rsub|1>,\<b-p\><rsub|2>,\<b-p\><rsub|3>|)>=g<rsub|4><around*|(|\<mu\><rsub|0>|)>-<frac|g<rsup|2><rsub|4><around*|(|\<mu\><rsub|0>|)>|2>
        <big|int>\<mathd\>k <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
        <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1>+\<delta\>g<rsub|4><around*|(|\<mu\><rsub|0>|)>
        ,
      </equation*>

      where, with the renormalization condition,
      <math|g<rsub|4><around*|(|\<mu\><rsub|0>|)>> shall be the measured
      value of <math|\<Gamma\><rsup|<around*|(|4|)>>> at
      <math|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=\<mu\><rsub|0>>.
      Thus, <math|\<delta\>g<rsub|4><around*|(|\<mu\><rsub|0>|)>=><math|<around*|(|g<rsup|2><rsub|4><around*|(|\<mu\><rsub|0>|)>/2|)>
      <big|int>\<mathd\>k <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
      <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1><mid|\|><rsub|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=\<mu\><rsub|0>>>.
      Now, at <math|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>s
      \<mu\><rsub|0>>, simply get the prediction value

      <\eqnarray*>
        <tformat|<table|<row|<cell|g<rsub|4><around*|(|s
        \<mu\><rsub|0>|)>>|<cell|=>|<cell|\<Gamma\><rsup|<around*|(|4|)>><around*|(|\<b-p\><rsub|1>,\<b-p\><rsub|2>,\<b-p\><rsub|3>|)>\<divides\><rsub|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=s
        \<mu\><rsub|0>>>>|<row|<cell|>|<cell|=>|<cell|g<rsub|4><around*|(|\<mu\><rsub|0>|)>-<frac|g<rsup|2><rsub|4><around*|(|\<mu\><rsub|0>|)>|2>
        <big|int>\<mathd\>k <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
        <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1>\<divides\><rsub|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=s
        \<mu\><rsub|0>>+\<delta\>g<rsub|4><around*|(|\<mu\><rsub|0>|)>>>|<row|<cell|>|<cell|=>|<cell|g<rsub|4><around*|(|\<mu\><rsub|0>|)>+<frac|g<rsup|2><rsub|4><around*|(|\<mu\><rsub|0>|)>|2>
        <big|int>\<mathd\>k <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
        <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1><mid|\|><rsup|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=\<mu\><rsub|0>><rsub|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=s
        \<mu\><rsub|0>> .>>>>
      </eqnarray*>

      Notce that, within the ``standard'' landscape, only <math|<wide|g|~>>
      is variable. Both different theories and the same theory at different
      energy scales of lab are differing only in values of <math|<wide|g|~>>.
      So, how to tell that <math|<wide|g|~><around*|(|\<mu\>|)>> and
      <math|<wide|g|~><around*|(|\<mu\><rprime|'>|)>> are the same theory at
      different energy scales of lab? In particular
      <math|<wide|g|~><rsub|4>>? The answer is manifest from the previous
      calculation, where it is

      <\equation*>
        g<rsub|4><around*|(|\<mu\>|)>+<frac|g<rsup|2><rsub|4><around*|(|\<mu\>|)>|2>
        <big|int>\<mathd\>k <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
        <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1><mid|\|><rsub|<around*|\||\<b-p\><rsub|1>|\|>=<around*|\||\<b-p\><rsub|2>|\|>=<around*|\||\<b-p\><rsub|3>|\|>=\<mu\>>
      </equation*>

      that is independent of <math|\<mu\>><\footnote>
        We used the fact that <math|g<rsub|4><rsup|2><around*|(|s
        \<mu\>|)>=g<rsub|4><rsup|2><around*|(|\<mu\>|)>+<with|math-font|cal|O><around*|(|g<rsub|4><rsup|3><around*|(|\<mu\>|)>|)>>,
        which is manifest by the expression of <math|g<rsub|4><around*|(|s
        \<mu\>|)>>.
      </footnote>. This quantity is just <math|g<rsub|4><around*|(|\<mu\>|)>+\<delta\>g<rsub|4><around*|(|\<mu\>|)>>,
      i.e. the <math|g<rsub|B 4>>, which is what we concluded.

      <\remark>
        <\with|color|dark blue>
          This conclusion is hidden in QTF because the coupling of
          interaction, i.e. charge <math|e>, needs no renormalization, unlike
          <math|g<rsub|4>>. This is why this problem borthers me so long!

          And what about the RGE, of this kind, of QED? We conclude that
          there's no such RGE. Verily, all couplings, <math|m> and <math|e>,
          are independent of <math|\<mu\>>. So, for QED, there's only RGE of
          <math|g<rsub|B><around*|(|\<mu\>|)>>, rather than of
          <math|g<around*|(|\<mu\>|)>>!
        </with>
      </remark>

      <item>Next, we wonder can this conclusion be generalized to all
      <math|g>? After all, renormalizations of <math|g<rsub|2>> and <math|Z>
      are not as arbitrary as <math|g<rsub|4>>. <math|g<rsub|2>> itself is a
      observable that is independent of <math|\<mu\>>, i.e. the mass pole.
      And without this renormalization, many serious problems arise. The
      answer thus is no. That is, at different energy scales of lab, the
      measured value of mass pole is unchanged, unlike the case of
      <math|g<rsub|4>>.

      Notice that it is the renormalized <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p;g<rsub|2>|)>>
      that is independent of <math|\<mu\>>! Indeed, it is always
      <math|p<rsup|2>+m<rsup|2>> with <math|m> (i.e. <math|<sqrt|g<rsub|2>>>)
      the value of observed mass pole.<\footnote>
        Remind that for <math|\<Gamma\><rsup|<around*|(|2|)>>>, unlike
        <math|\<Gamma\><rsup|N\<gtr\>2>>, the variable <math|p> can be
        off-shell. (Consider the effective tree diagram with a meson.)
      </footnote> What about <math|\<Gamma\><rsup|<around*|(|2|)>><rsub|B>>?
      By

      <\eqnarray*>
        <tformat|<table|<row|<cell|\<Gamma\><rsup|<around*|(|2|)>><rsub|B>>|<cell|=>|<cell|Z
        \<Gamma\><rsup|<around*|(|2|)>>>>|<row|<cell|>|<cell|=>|<cell|\<Gamma\><rsup|<around*|(|2|)>>+\<delta\>Z
        \<Gamma\><rsup|<around*|(|2|)>>>>>>
      </eqnarray*>

      which does depends on <math|\<mu\>>, via
      <math|\<delta\>Z=\<delta\>Z<around*|(|g<rsub|2>,g<rsub|4><around*|(|\<mu\>|)>|)>>.
      That is, for any <math|p> given, a change of <math|\<mu\>> will change
      the value of <math|\<Gamma\><rsup|<around*|(|2|)>><rsub|B>> (but not of
      <math|\<Gamma\><rsup|<around*|(|2|)>>>). This goes against to the
      proposal that <math|\<Gamma\><rsup|<around*|(|2|)>><rsub|B>> is
      independent of <math|\<mu\>>. This hints us that it maybe the
      <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>><rsub|B>> that is
      independent of <math|\<mu\>>.

      <item>So, focus on <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>><rsub|B>>
      only. At 1-loop level, <math|\<delta\>Z=0>. It has been shown that,

      <\eqnarray*>
        <tformat|<table|<row|<cell|\<Gamma\><rsup|<around*|(|4|)>><rsub|B><around*|(|\<b-p\><rsub|1>,\<b-p\><rsub|2>,\<b-p\><rsub|3>|)>>|<cell|=>|<cell|\<Gamma\><rsup|<around*|(|4|)>><around*|(|\<b-p\><rsub|1>,\<b-p\><rsub|2>,\<b-p\><rsub|3>|)>>>|<row|<cell|>|<cell|=>|<cell|g<rsub|B
        4>-<frac|g<rsup|2><rsub|4><around*|(|\<mu\>|)>|2> <big|int>\<mathd\>k
        <around*|[|k<rsup|2>+m<rsup|2>|]><rsup|-1>
        <around*|[|<around*|(|p<rsub|1>+p<rsub|2>-k|)><rsup|2>+m<rsup|2>|]><rsup|-1>
        .>>>>
      </eqnarray*>

      From this we find that the dependence on <math|\<mu\>>, appearing in
      <math|g<rsup|2><rsub|4><around*|(|\<mu\>|)>>, is at
      <math|g<rsub|4><rsup|3>> order, thus beyond 1-loop for
      <math|\<Gamma\><rsup|<around*|(|4|)>>><\footnote>
        Or say, beyond 2-vertex order.
      </footnote>. Thus, at 1-loop level,
      <math|\<Gamma\><rsup|<around*|(|4|)>><rsub|B><around*|(|\<b-p\><rsub|1>,\<b-p\><rsub|2>,\<b-p\><rsub|3>|)>>
      does be independent of <math|\<mu\>>, as proposed.

      <item>1-loop level, however, is not that exciting, since <math|Z=1>.
      What about 2-loop level?

      T.B.C.
    </enumerate>

    \;
  </answer>

  <appendix|Vertex Function: Physical Meaning>

  <\theorem>
    Vertex functions and connected Green functions:

    <\equation*>
      \<Gamma\><rsup|<around*|(|2|)>><around*|(|\<b-p\>|)>=1/G<rsup|<around*|(|2|)>><rsub|c><around*|(|\<b-p\>|)><rsup|>=p<rsup|2>+g<rsub|B2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>;
    </equation*>

    and

    <\equation*>
      G<rsub|c><rsup|<around*|(|N|)>><around*|(|x<rprime|'><rsub|1>,\<ldots\>,x<rprime|'><rsub|N>|)>=<big|int>\<mathd\>x<rprime|'><rsub|1>\<cdots\>\<mathd\>x<rprime|'><rsub|N>G<rsub|c><rsup|<around*|(|2|)>><around*|(|x<rsub|1>,x<rsub|1><rprime|'>|)>\<cdots\>G<rsub|c><rsup|<around*|(|2|)>><around*|(|x<rsub|N>,x<rsub|N><rprime|'>|)>\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>><around*|(|x<rprime|'><rsub|1>,\<ldots\>,x<rprime|'><rsub|N>|)>.
    </equation*>

    These relations hold for both renormalized and bared.
  </theorem>

  <\proof>
    C.f. asdf, section asdf.s
  </proof>

  <\corollary>
    The physical meaning of <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>>> is
    that asdf
  </corollary>

  <\remark>
    The physical meanings of <math|\<Gamma\><rsup|<around*|(|2|)>>> and
    <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>>> are different, even though
    they can be formally the same.
  </remark>

  <appendix|An Explicit Instance of Renormalization>

  <appendix|Further Notes: to be Re-edit>

  <subsection|Loops on Extra-legs and Renormalization of
  <math|\<Gamma\><rsup|<around*|(|2|)>>>><label|section: Loops on Extra-legs
  and Renormalization of Gamma-2>

  <\theorem>
    Renormalization conditions <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    and <math|<around*|(|\<mathd\>\<Gamma\><rsup|<around*|(|2|)>>/\<mathd\>p<rsup|2>|)><around*|(|p<rsup|2>=-m<rsup|2>|)>=1>
    is the sufficient and necessary condition of eliminating the loops on
    extra-legs. This also holds for massless theory, wherein simply set
    <math|m=0>.
  </theorem>

  <\problem>
    The fact that this theorem holds for <math|m=0> is indeed the case in
    QTF, section 11.2, wherein it is photon that is considered. But, it seems
    oppose to Peskin, wherein <math|m=0> rises additional divergent (c.f.
    P326~328 (P349~351)). What's the difference between the two?

    <\answer>
      The only thing to be considered is <math|\<Gamma\><rsup|<around*|(|2|)>>>.
      So let's focus on it, at 1-loop as instance. Thus

      <\equation*>
        \<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>|)>=p<rsup|2>+m<rsup|2>+\<lambda\>
        <big|int>\<mathd\><rsup|4>p <frac|1|p<rsup|2>+m<rsup|2>>+\<delta\>m<rsup|2>+p<rsup|2>
        \<delta\>Z .
      </equation*>

      As calculated by Peskin via dimensional regularization,
      <math|\<delta\>Z=0> and <math|\<delta\>m<rsup|2>=-\<lambda\>
      <big|int>\<mathd\><rsup|4>p <around*|[|p<rsup|2>+m<rsup|2>|]><rsup|-1>=-lim<rsub|d\<rightarrow\>4>\<lambda\>
      \<Gamma\><around*|(|1-d/2|)>/<around*|(|m<rsup|2>|)><rsup|1-d/2>>,
      which is meaningless as <math|m=0> before taking the limit. But, remind
      that, for <math|\<lambda\> \<phi\><rsup|4>> model, no gauge symmetry is
      to be held. So, if using hard cut-off regularization, then, when
      <math|m=0>, <math|\<delta\>m<rsup|2>=-lim<rsub|\<Lambda\>\<rightarrow\>+\<infty\>>\<lambda\>
      <big|int><rsup|\<Lambda\>>\<mathd\><rsup|4>p
      <around*|[|p<rsup|2>|]><rsup|-1>=-lim<rsub|\<Lambda\>\<rightarrow\>+\<infty\>>
      \<lambda\> \<Lambda\><rsup|2>>, which is well defined before taking the
      limit.

      This is the difference between the two. The regularization borthers. In
      the place where gauge symmetry must be held, i.e. QED, dimensional
      regularization employed at <math|m=0> encounters no difficulty. And in
      the place where gauge symmetry is absent, i.e. <math|\<lambda\>
      \<phi\><rsup|4>> model, hard cut-off regularization employed at
      <math|m=0> is well defined.

      The renormalization of <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>>>
      must be on-shell too, if S-matrix is expressed as the effective tree
      diagram with all vertex coefficients replaced by the corresponding
      vertex functions. So, to comparing with experiment, such expression of
      S-matrix must be well-defined. This becomes wicked in Peskin's.
    </answer>

    <\remark>
      By the way, remind that there's infrared divergence in the calculation
      of vertex function of QED, in QTF section 11.3. It is explained under
      eq. (11.3.23) of QTF (P491). To deal with this infrared divergence, a
      virtual photon mass <math|\<mu\>> is introduced into the calculation,
      with a list of reasons why this is valid explained in the following
      paragraph.
    </remark>
  </problem>

  <\proof>
    <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    and \ and <math|<around*|(|\<mathd\>\<Gamma\><rsup|<around*|(|2|)>>/\<mathd\>p<rsup|2>|)><around*|(|p<rsup|2>=-m<rsup|2>|)>=1>
    are equivalent to <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    and <math|<around*|(|\<mathd\>\<Pi\><rsup|\<ast\>>/\<mathd\>p<rsup|2>|)><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>,
    where <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>> is the 1PI.
    Remind that 1PI is the sum of all 1-particle irreducible diagrams. Then,
    consider the diagram with one side of which a leg representing in- (out-)
    going particle, and with the other side of which a propagator connected
    to a vertex function. Such a diagram is one component of the sum

    <\equation*>
      I<around*|(|\<b-p\>|)>\<assign\>lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>u<rsup|\<ast\>><rsub|l><around*|(|\<b-p\>|)>
      <around*|{|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      <frac|1|p<rsup|2>+m<rsup|2>> \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>+\<cdots\>|}>
      <frac|1|p<rsup|2>+m<rsup|2>> ;
    </equation*>

    and every component in <math|I<around*|(|\<b-p\>|)>> uniquely represents
    such a diagram. <math|I<around*|(|\<b-p\>|)>> thus represents the sum of
    all such diagrams. Then, what we want is <math|I<around*|(|\<b-p\>|)>=0>.
    The sufficient and necessary condition is
    <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0> and
    <math|<around*|(|\<mathd\>\<Pi\><rsup|\<ast\>>/\<mathd\>p<rsup|2>|)><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>.
    Indeed, Taylor expanding \ <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>>
    at <math|p<rsup|2>=-m<rsup|2>> gives

    <\equation*>
      <tabular|<tformat|<table|<row|<cell|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>>>>>>=\<Pi\><rsup|\<ast\>><around*|(|-m<rsup|2>|)>+<frac|\<mathd\>\<Pi\><rsup|\<ast\>>|\<mathd\>p<rsup|2>><around*|(|-m<rsup|2>|)>
      <around*|(|p<rsup|2>+m<rsup|2>|)>+<with|math-font|cal|O<around*|(|<around*|(|p<rsup|2>+m<rsup|2>|)><rsup|2>|)>>
      .
    </equation*>

    Thus, for instance

    <\eqnarray*>
      <tformat|<table|<row|<cell|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      <frac|1|p<rsup|2>+m<rsup|2>>>|<cell|=>|<cell|\<Pi\><rsup|\<ast\>><around*|(|-m<rsup|2>|)>+<frac|\<mathd\>\<Pi\><rsup|\<ast\>>|\<mathd\>p<rsup|2>><around*|(|-m<rsup|2>|)>
      <around*|(|p<rsup|2>+m<rsup|2>|)>+<with|math-font|cal|O<around*|(|<around*|(|p<rsup|2>+m<rsup|2>|)><rsup|2>|)>>
      <frac|1|p<rsup|2>+m<rsup|2>>>>|<row|<cell|>|<cell|=>|<cell|<frac|\<Pi\><rsup|\<ast\>><around*|(|-m<rsup|2>|)>|p<rsup|2>+m<rsup|2>>+<frac|\<mathd\>\<Pi\><rsup|\<ast\>>|\<mathd\>p<rsup|2>><around*|(|-m<rsup|2>|)>+O<around*|(|p<rsup|2>+m<rsup|2>|)>
      .>>>>
    </eqnarray*>

    Thus, demanding <math|lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
    <around*|(|p<rsup|2>+m<rsup|2>|)><rsup|-1>> is equivalent to demand
    <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0> and
    <math|<around*|(|\<mathd\>\<Pi\><rsup|\<ast\>>/\<mathd\>p<rsup|2>|)><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>.
    This can be easily generalized to the whole
    <math|I<around*|(|\<b-p\>|)>>. Thus, proof ends.
  </proof>

  <\remark>
    This is the reason of employing the renormalization conditions of
    <math|\<Gamma\><rsup|<around*|(|2|)>>> in QTF's renormalization
    scheme<\footnote>
      C.f. section 10.3 of QTF. I suddenly find that this has been a theorem
      in QTF. Ah, I made the wheel again!
    </footnote>, instead of any other: it brings (great) convenience. Indeed,
    with these two conditions employed, you can safely regardless of all the
    diagrams with loops on extra-legs. It seems, however essential too.
    Verily, without these two specific renormalization conditions, when
    calculating S-matrix, every extra-leg must not be simply
    <math|u<rsup|\<ast\>><rsub|l><around*|(|\<b-p\>|)>> for in-going particle
    or <math|u<rsup|\<ast\>><rsub|l><around*|(|\<b-p\>|)>> for out-going, but
    the <math|I<around*|(|\<b-p\>|)>>. Unfortunately, this
    <math|I<around*|(|\<b-p\>|)>>, when using other renormalization
    conditions on <math|\<Gamma\><rsup|<around*|(|2|)>>> instead of those in
    QTF, will be divergent because of <math|lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>><around*|(|p<rsup|2>+m<rsup|2>|)><rsup|-1>>.
    Thus, the form of S-matrix will be ill-defined. So, apart from those for
    <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>>>, these two specific
    renormalization conditions for <math|\<Gamma\><rsup|<around*|(|2|)>>>
    must be employed for giving a well-defined form of S-matrix (for fitting
    data).
  </remark>

  <\with|color|black>
    <subsection|Renormalization of <math|\<Gamma\><rsup|<around*|(|2|)>>> and
    Single Particle Scattering Problem>
  </with>

  Remind that <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>|)>\<assign\>p<rsup|2>+m<rsup|2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>>,
  where <math|\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>> is the 1PI.

  <\definition>
    <label|definition: (Modified) S-matrix>(Modified) S-matrix is defined by

    <\equation*>
      S<rsub|\<beta\> \<alpha\>>=<frac|<around*|(|\<Phi\><rsub|\<beta\>>,S
      \<Phi\><rsub|\<alpha\>>|)>|<around*|(|\<Phi\><rsub|0>,S
      \<Phi\><rsub|0>|)>> ,
    </equation*>

    where <math|S> is defined as in QTF, as <math|exp<around*|(|-\<mathi\>
    H<rsub|int><around*|(|t|)>|)>> in interactive picture, or as the Dyson
    series <math|S=1+<big|sum><rsub|n=1><rsup|+\<infty\>><frac|i<rsup|n>|n!>
    <big|int>\<mathd\>x<rsub|1> \<cdots\> \<mathd\>x<rsub|n>
    T<around*|{|<with|math-font|cal|H><around*|(|x<rsub|1>|)> \<cdots\>
    <with|math-font|cal|H><around*|(|x<rsub|n>|)><rsub|>|}>>, where
    <math|<with|math-font|cal|H>> the denstity of
    <math|H<rsub|int><around*|(|t|)>>.
  </definition>

  <\definition>
    <label|definition: vacuum diagram>Vacuum diagram is defined as the
    Feynman diagram wherein there exists at least one sub-diagram that has no
    extra-leg.
  </definition>

  <\lemma>
    <label|lemma: elimination of vacuum diagram>The Dyson series
    expansion<\footnote>
      i.e. eq. (4.5.13) in QTF. Explicitly,
      <math|S=1+<big|sum><rsub|n=1><rsup|+\<infty\>><frac|i<rsup|n>|n!>
      <big|int>\<mathd\>x<rsub|1> \<cdots\> \<mathd\>x<rsub|n>
      T<around*|{|<with|math-font|cal|H><around*|(|x<rsub|1>|)> \<cdots\>
      <with|math-font|cal|H><around*|(|x<rsub|n>|)><rsub|>|}>>, where
      <math|<with|math-font|cal|H>> the denstity of
      <math|H<rsub|int><around*|(|t|)>>.
    </footnote> of this modified S-matrix contians no vacuum diagram. That
    is, all vacuum diagrams in <math|<around*|(|\<Phi\><rsub|\<beta\>>,S
    \<Phi\><rsub|\<alpha\>>|)>> will be eliminated by contributions from
    <math|<around*|(|\<Phi\><rsub|0>,S \<Phi\><rsub|0>|)>>.
  </lemma>

  <\proof>
    How to proof? In <reference|bib: RG book>, section 4-2, an explicit
    instance is displayed; and in section 4-3, a proof is given, but hard to
    be understood.<\footnote>
      Hint: it's convenient to think with the bared action, instead of the
      renormalized (BPHZ).
    </footnote> Giving a clear proof is a left problem.

    Remark that, even though the explicit instance is for Green functions,
    instead of S-matrix. But, the structure of diagram are the same. And, in
    the instance, it is the cancellation of diagrams that proves the
    elimination of vacuum diagrams.
  </proof>

  <\theorem>
    <label|theorem: on connected diagram with two extra-legs>The sufficient
    and necessary condition of letting <math|S<rsub|\<b-p\><rprime|'>,\<sigma\><rprime|'>,n<rprime|'>,\<b-p\>,\<sigma\>,n>=\<delta\><rsup|3><around*|(|\<b-p\>-\<b-p\><rprime|'>|)>
    \<delta\><rsub|\<sigma\><rprime|'>,\<sigma\>>
    \<delta\><rsub|n<rprime|'>,n>> is <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    with <math|<around*|(|\<mathd\>\<Gamma\><rsup|<around*|(|2|)>>/\<mathd\>p<rsup|2>|)><around*|(|-m<rsup|2>|)>>
    finite, where <math|S<rsub|\<b-p\><rprime|'>,\<sigma\><rprime|'>,n<rprime|'>;\<b-p\>,\<sigma\>,n>=<around*|(|\<Phi\><rsub|\<b-p\><rprime|'>,\<sigma\><rprime|'>,n<rprime|'>>,S
    \<Phi\><rsub|\<b-p\>,\<sigma\>,n>|)>/<around*|(|\<Phi\><rsub|0>,S
    \<Phi\><rsub|0>|)>> is the modified S-matrix.
  </theorem>

  <\proof>
    For simplicity, label the quantum number only by <math|\<b-p\>>. The
    Dyson series expansion <math|S<rsub|\<b-p\><rprime|'>;\<b-p\>>=<around*|(|\<Phi\><rsub|\<b-p\><rprime|'>>,S
    \<Phi\><rsub|\<b-p\>>|)>/<around*|(|\<Phi\><rsub|0>,S \<Phi\><rsub|0>|)>>
    contains only three possibilities: vacuum diagrams, tadpoles, and
    connected diagrams. This is because there are only one in-going leg and
    one out-going leg. Indeed, because of this, there are two ways of pairing
    by Wick theorem:

    <\enumerate>
      <item>pairing <math|a<around*|(|\<b-p\><rprime|'>|)>> with
      <math|a<rsup|\<dagger\>><around*|(|\<b-p\>|)>>, and then pairing left
      fields in <math|<with|math-font|cal|H><around*|(|x<rsub|i>|)>>s;

      <item>pairing <math|a<around*|(|\<b-p\><rprime|'>|)>> with a field in
      one of the <math|<with|math-font|cal|H><around*|(|x<rsub|i>|)>>s, and
      pairing <math|a<rsup|\<dagger\>><around*|(|\<b-p\>|)>> with another
      field in one of the <math|<with|math-font|cal|H><around*|(|x<rsub|i>|)>>s,
      and then paring the left fields;
    </enumerate>

    The first class contains only vacuum diagrams. The second class can be
    divided into connected and disconnected. All disconnected diagrams are
    vacuum diagrams.<\footnote>
      This is specific result, holding only for single particle scattering.
      For instance, for two particle scattering,
      <math|\<delta\><rsup|3><around*|(|\<b-p\><rsub|1><rprime|'>-\<b-p\><rsub|1>|)>\<times\>\<delta\><rsup|3><around*|(|\<b-p\><rsub|2><rprime|'>-\<b-p\><rsub|2>|)>>
      are disconnected diagram, but without vacuum sub-diagram.
    </footnote> By lemma <reference|lemma: elimination of vacuum diagram>,
    all vacuum diagrams are eliminated. This leaves only the connected
    diagrams in the second class. That is, only the connected diagrams
    survive in the Dyson series expansion
    <math|S<rsub|\<b-p\><rprime|'>;\<b-p\>>=<around*|(|\<Phi\><rsub|\<b-p\><rprime|'>>,S
    \<Phi\><rsub|\<b-p\>>|)>/<around*|(|\<Phi\><rsub|0>,S
    \<Phi\><rsub|0>|)>>.

    Hence, only consider connected diagrams. Direct calculation by Feynman
    rules gives

    <\eqnarray*>
      <tformat|<table|<row|<cell|S<rsub|\<b-p\><rprime|'>,\<b-p\>>>|<cell|=>|<cell|\<delta\><rsup|3><around*|(|\<b-p\>-\<b-p\><rprime|'>|)>+>>|<row|<cell|>|<cell|+>|<cell|u<rsup|\<ast\>><around*|(|\<b-p\>|)>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      u<around*|(|\<b-p\>|)>+u<rsup|\<ast\>><around*|(|\<b-p\>|)>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      <frac|1|p<rsup|2>+m<rsup|2>> \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      u<around*|(|\<b-p\>|)>+>>|<row|<cell|>|<cell|+>|<cell|u<rsup|\<ast\>><around*|(|\<b-p\>|)>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      <frac|1|p<rsup|2>+m<rsup|2>> \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      <frac|1|p<rsup|2>+m<rsup|2>> \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>
      u<around*|(|\<b-p\>|)>+\<cdots\>>>|<row|<cell|>|<cell|=>|<cell|\<delta\><rsup|3><around*|(|\<b-p\>-\<b-p\><rprime|'>|)>+>>|<row|<cell|>|<cell|+>|<cell|lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>u<rsup|\<ast\>><around*|(|\<b-p\>|)>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)> u<around*|(|\<b-p\>|)>
      <around*|(|1+<frac|1|p<rsup|2>+m<rsup|2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>|)> ,>>>>
    </eqnarray*>

    So, the sufficient and necessary condition of
    <math|S<rsub|\<b-p\><rprime|'>,\<b-p\>>=\<delta\><rsup|3><around*|(|\<b-p\>-\<b-p\><rprime|'>|)>>
    is

    <\equation*>
      lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>u<rsup|\<ast\>><around*|(|\<b-p\>|)>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)> u<around*|(|\<b-p\>|)>
      <around*|(|1+<frac|1|p<rsup|2>+m<rsup|2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>>
      \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>|)>=0 .
    </equation*>

    Thus, <math|lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>>\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>=0>
    or <math| lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>><around*|(|1+<around*|[|p<rsup|2>+m<rsup|2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>|]><rsup|-1>
    \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>|)>=0>. Both lead to
    <math|\<Pi\><rsup|\<ast\>><around*|(|-m<rsup|2>|)>=0>. This condition
    also makes <math| lim<rsub|p<rsup|2>\<rightarrow\>-m<rsup|2>><around*|[|p<rsup|2>+m<rsup|2>+\<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>|]><rsup|-1>
    \<Pi\><rsup|\<ast\>><around*|(|p<rsup|2>|)>> well-defined as long as
    <math|<around*|(|\<mathd\>\<Pi\><rsup|\<ast\>>/\<mathd\>p<rsup|2>|)><around*|(|-m<rsup|2>|)>>
    is finite, provided by l'hospital rule. Remind that the conditions
    <math|\<Pi\><rsup|\<ast\>><around*|(|-m<rsup|2>|)>=0> and
    <math|<around*|(|\<mathd\>\<Pi\><rsup|\<ast\>>/\<mathd\>p<rsup|2>|)><around*|(|-m<rsup|2>|)>>
    is finite correspond to <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    and <math|<around*|(|\<mathd\>\<Gamma\><rsup|<around*|(|2|)>>/\<mathd\>p<rsup|2>|)><around*|(|-m<rsup|2>|)>>
    is finite, respectively. Thus proof ends.
  </proof>

  <\corollary>
    The single particle scattering problem is whether the (modified) S-matrix
    of one in-going particle and one out-going particle in an interactive
    theory is the same as that in a free theory, or not. This problem can be
    completely solved and giving a positive answer if and only if
    renormalization condition <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    (with <math|<around*|(|\<mathd\>\<Gamma\><rsup|<around*|(|2|)>>/\<mathd\>p<rsup|2>|)><around*|(|-m<rsup|2>|)>>
    finite) is held.
  </corollary>

  <\remark>
    To make the (modified) S-matrix self-consistent <strong|in its form>,
    renormalization condition <math|\<Gamma\><rsup|<around*|(|2|)>><around*|(|p<rsup|2>=-m<rsup|2>|)>=0>
    must be held. And we surprisingly find that, <strong|with a proper
    re-definition of S-matrix and a proper renormalization, the field theory
    can be self-consistent, i.e. no single particle scattering problem
    appears.>
  </remark>

  <subsection|What is the <math|\<mu\>>-independent?>

  <\theorem>
    The renormalized vertex functions <math|\<Gamma\><rsup|<around*|(|N|)>>>,
    for both <math|N=2> and <math|N\<gtr\>2>, are observables.
  </theorem>

  <\proof>
    This can be directly deduced from the physical meaning of
    <math|\<Gamma\><rsup|<around*|(|N\<gtr\>2|)>>>, i.e. the relation between
    renormalized connected Green function and renormalized vertex function.
    Moreover, because of LSZ formula, this relation is nothing but that
    between connected S-matrix and renormalized vertex function. So, the
    connection between connected S-matrix, the observable, and renormalized
    vertex function is straight-forward. For <math|N=2>,
    <math|\<Gamma\><rsup|<around*|(|2|)>>> has only one parameter, which is
    the observed mass pole. Thus, it is also an observable. Proof ends.
  </proof>

  Now, we clarify where the RGE comes from. That is, what quantity is the
  <math|\<mu\>>-independent.

  For simplicity, consider <math|\<Gamma\><rsup|<around*|(|4|)>>> only. Let
  some <math|\<mu\><rsub|0>> fixed and consider another variable
  <math|\<mu\>>. The renormalization scheme at <math|\<mu\>> gives
  <math|\<Gamma\><rsup|<around*|(|4|)>><mid|\|><rsub|SP=\<mu\>>=g<rsub|4><around*|(|\<mu\>|)>>.
  Then, with <math|g<rsub|4><around*|(|\<mu\>|)>>, we can calculate out a
  full functional expression of <math|\<Gamma\><rsup|<around*|(|4|)>><around*|(|<around*|{|\<b-k\><rsub|j>|}>;\<mu\>,g<rsub|4><around*|(|\<mu\>|)>|)>>.
  Set the value of <math|<around*|{|\<b-k\><rsub|j>|}>> at
  <math|SP=\<mu\><rsub|0>>. The functinal form of
  <math|\<Gamma\><rsup|<around*|(|4|)>>> at <math|SP=\<mu\><rsub|0>> is
  denoted by <math|f>. Apparently, <math|f> depends on <math|\<mu\>> and
  <math|g<rsub|4><around*|(|\<mu\>|)>>. That is,
  <math|f<around*|(|\<mu\>,g<rsub|4><around*|(|\<mu\>|)>|)>=\<Gamma\><rsup|<around*|(|4|)>><mid|\|><rsub|SP=\<mu\><rsub|0>>>
  However, since <math|\<Gamma\><rsup|<around*|(|4|)>>> is observable, the
  value of <math|\<Gamma\><rsup|<around*|(|4|)>><mid|\|><rsub|SP=\<mu\><rsub|0>>>
  shall be independent of renormalization scheme <math|\<mu\>>. \ Thus, we
  get

  <\equation*>
    \<mu\> <frac|\<mathd\>f|\<mathd\>\<mu\>><around*|(|\<mu\>,g<rsub|4><around*|(|\<mu\>|)>|)>=0
    .
  </equation*>

  It is this that furnishes the RGE!

  \;
</body>

<\initial>
  <\collection>
    <associate|sfactor|4>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|(Modified) S-matrix|<tuple|22|?>>
    <associate|auto-1|<tuple|1|1>>
    <associate|auto-10|<tuple|2.8|5>>
    <associate|auto-11|<tuple|2.9|6>>
    <associate|auto-12|<tuple|2.10|7>>
    <associate|auto-13|<tuple|3|7>>
    <associate|auto-14|<tuple|3.1|?>>
    <associate|auto-15|<tuple|3.1.1|?>>
    <associate|auto-16|<tuple|3.1.2|?>>
    <associate|auto-17|<tuple|3.1.3|?>>
    <associate|auto-18|<tuple|3.2|?>>
    <associate|auto-19|<tuple|3.3|?>>
    <associate|auto-2|<tuple|2|1>>
    <associate|auto-20|<tuple|3.4|?>>
    <associate|auto-21|<tuple|3.5|?>>
    <associate|auto-22|<tuple|3.6|?>>
    <associate|auto-23|<tuple|3.7|?>>
    <associate|auto-24|<tuple|4|?>>
    <associate|auto-25|<tuple|5|?>>
    <associate|auto-26|<tuple|5.1|?>>
    <associate|auto-27|<tuple|1|?>>
    <associate|auto-28|<tuple|5.2|?>>
    <associate|auto-29|<tuple|6|?>>
    <associate|auto-3|<tuple|2.1|1>>
    <associate|auto-30|<tuple|6.1|?>>
    <associate|auto-31|<tuple|A|?>>
    <associate|auto-32|<tuple|B|?>>
    <associate|auto-33|<tuple|C|?>>
    <associate|auto-34|<tuple|C.1|?>>
    <associate|auto-35|<tuple|C.2|?>>
    <associate|auto-36|<tuple|C.3|?>>
    <associate|auto-37|<tuple|C.2|?>>
    <associate|auto-38|<tuple|D.1|?>>
    <associate|auto-4|<tuple|2.2|2>>
    <associate|auto-5|<tuple|2.3|3>>
    <associate|auto-6|<tuple|2.4|3>>
    <associate|auto-7|<tuple|2.5|4>>
    <associate|auto-8|<tuple|2.6|4>>
    <associate|auto-9|<tuple|2.7|5>>
    <associate|definition: (Modified) S-matrix|<tuple|21|?>>
    <associate|definition: vacuum diagram|<tuple|22|?>>
    <associate|example: lambda phi 4 model|<tuple|3|2>>
    <associate|footnote-1|<tuple|1|2>>
    <associate|footnote-10|<tuple|10|?>>
    <associate|footnote-11|<tuple|11|?>>
    <associate|footnote-12|<tuple|12|?>>
    <associate|footnote-13|<tuple|13|?>>
    <associate|footnote-14|<tuple|14|?>>
    <associate|footnote-15|<tuple|15|?>>
    <associate|footnote-2|<tuple|2|4>>
    <associate|footnote-3|<tuple|3|4>>
    <associate|footnote-4|<tuple|4|4>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnote-6|<tuple|6|?>>
    <associate|footnote-7|<tuple|7|?>>
    <associate|footnote-8|<tuple|8|?>>
    <associate|footnote-9|<tuple|9|?>>
    <associate|footnr-1|<tuple|1|2>>
    <associate|footnr-10|<tuple|10|?>>
    <associate|footnr-11|<tuple|11|?>>
    <associate|footnr-12|<tuple|12|?>>
    <associate|footnr-13|<tuple|13|?>>
    <associate|footnr-14|<tuple|14|?>>
    <associate|footnr-15|<tuple|15|?>>
    <associate|footnr-2|<tuple|2|4>>
    <associate|footnr-3|<tuple|3|4>>
    <associate|footnr-4|<tuple|4|4>>
    <associate|footnr-5|<tuple|5|?>>
    <associate|footnr-6|<tuple|6|?>>
    <associate|footnr-7|<tuple|7|?>>
    <associate|footnr-8|<tuple|8|?>>
    <associate|footnr-9|<tuple|9|?>>
    <associate|lemma: elimination of tadpole|<tuple|25|?>>
    <associate|lemma: elimination of vacuum diagram|<tuple|23|?>>
    <associate|section: Loops on Extra-legs and Renormalization of
    Gamma-2|<tuple|C.1|?>>
    <associate|section: Renormalized Vertex Function|<tuple|3.1.1|?>>
    <associate|theorem: on connected diagram with two
    extra-legs|<tuple|24|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|figure>
      <tuple|normal|A typical shape of beta function with asymptotic
      freedom.|<pageref|auto-27>>
    </associate>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Introduction>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|RGE
      in Wilson's Approach> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>

      <with|par-left|<quote|1.5fn>|Integrate out Momentum Slice
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-3>>

      <with|par-left|<quote|1.5fn>|A Toy Instance
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-4>>

      <with|par-left|<quote|1.5fn>|Matching Vertices Method
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-5>>

      <with|par-left|<quote|1.5fn>|General Case
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-6>>

      <with|par-left|<quote|1.5fn>|Beta Function
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-7>>

      <with|par-left|<quote|1.5fn>|Stable Point
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-8>>

      <with|par-left|<quote|1.5fn>|Digression: Effective Potential and
      Symmetry Breaking <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-9>>

      <with|par-left|<quote|1.5fn>|Discussion: What's Renormalization for
      Wilson Effective Action? <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-10>>

      <with|par-left|<quote|1.5fn>|Problems with Wilson's Approach
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-11>>

      <with|par-left|<quote|1.5fn>|Summary
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-12>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|RGE
      in Callan-S? Approach> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-13><vspace|0.5fn>

      <with|par-left|<quote|1.5fn>|Renormalization Scheme
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-14>>

      <with|par-left|<quote|3fn>|Renormalized Vertex Function
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-15>>

      <with|par-left|<quote|3fn>|Old Renormalization V.S. BPHZ
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-16>>

      <with|par-left|<quote|3fn>|Relations with S-matrix
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-17>>

      <with|par-left|<quote|1.5fn>|Relations of Dependence
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-18>>

      <with|par-left|<quote|1.5fn>|Callan-S? Equation
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-19>>

      <with|par-left|<quote|1.5fn>|Solution
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-20>>

      <with|par-left|<quote|1.5fn>|Stable Point and Scaling Rule
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-21>>

      <with|par-left|<quote|1.5fn>|Problems in Callan-Symanzik Approach
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-22>>

      <with|par-left|<quote|1.5fn>|Summary
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-23>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|RGE
      of Bared Theory> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-24><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Infrared/Ultraviolet
      Attractor and Asymptotic Freedom> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-25><vspace|0.5fn>

      <with|par-left|<quote|1.5fn>|One Parameter Model
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-26>>

      <with|par-left|<quote|1.5fn>|Generalize to Multi-parameter Theory
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-28>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Residual
      Problems> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-29><vspace|0.5fn>

      <with|par-left|<quote|1.5fn>|The <with|mode|<quote|math>|\<gamma\>> in
      Scaling Rule <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-30>>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix<space|2spc>Vertex
      Function: Physical Meaning> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-31><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix<space|2spc>An
      Explicit Instance of Renormalization>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-32><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Appendix<space|2spc>Further
      Notes: to be Re-edit> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-33><vspace|0.5fn>

      <with|par-left|<quote|1.5fn>|Loops on Extra-legs and Renormalization of
      <with|mode|<quote|math>|\<Gamma\><rsup|<around*|(|2|)>>>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-34>>

      <with|par-left|<quote|1.5fn>|Renormalization of
      <with|mode|<quote|math>|\<Gamma\><rsup|<around*|(|2|)>>> and Single
      Particle Scattering Problem <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-35>>
    </associate>
  </collection>
</auxiliary>