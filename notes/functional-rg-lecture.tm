<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <\note>
    [P5, pg4]

    <\indent>
      Eq. (8) is gained as follow. Comparing eq. (7) with eq. (A6-1) in
      [<reference|bib: RG book>], recognize that <math|<wide|\<phi\>|~>>
      herein is <math|\<phi\>> therein, that <math|\<phi\>> herein is a
      parameter therein, and that <math|S<rsub|k-\<Delta\>k>=-F>. Thus, at
      1-loop level, since (c.f. eq. A6-16)

      <\equation*>
        F<rsub|1>=-<frac|1|2> Tr ln <frac|\<delta\><rsup|2>S|\<delta\>\<phi\>
        \<delta\>\<phi\>><around*|[|<wide|\<phi\>|~>|]> ,
      </equation*>

      we get

      <\equation*>
        \<Delta\>S<rsub|k-\<Delta\>k>=<frac|1|2> Tr ln
        <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~>
        \<delta\><wide|\<phi\>|~>><around*|[|\<phi\>+<wide|\<phi\>|~><rsub|cl>|]>
        ,
      </equation*>

      which is eq. (8).

      <\question>
        Is eq. (8) the Wegner-Honghton equation, if
        <math|<wide|\<phi\>|~>=0>? Because they look alike.
      </question>

      <\answer>
        Indeed so! See the following (eq. (10)).
      </answer>
    </indent>
  </note>

  <\note>
    [P6, pg3]

    <\indent>
      How is eq. (10) obtained???
    </indent>
  </note>

  <\note>
    [P7, pg1]

    <\indent>
      <\question>
        Why it is <math|<with|math-font|cal|O><around*|(|<around*|(|\<hbar\>
        \<Delta\>k|)><rsup|2>|)>> in eq. (13)? It vanishes as
        <math|\<Delta\>k\<rightarrow\>0> while <math|\<hbar\>> is fixed.
        Notice that it is different from eq. (10) where only both
        <math|\<Delta\>k\<rightarrow\>0> and <math|\<hbar\>\<rightarrow\>0>
        make <math|<with|math-font|cal|O><around*|(|#|)>> terms vanish.
      </question>
    </indent>
  </note>

  <\note>
    <label|note: assumption of loop expansion>[P7, pg2]

    <\quotation>
      Notice that the convergence of the loop [i.e. <math|\<hbar\>>-]
      expansion was assumed in the argument above.
    </quotation>
  </note>

  <\note>
    [P7, pg1]

    <\indent>
      <\theorem>
        If let <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>, then the
        <math|<around*|(|\<delta\>S/\<delta\><wide|\<phi\>|~>|)>\<cdot\><around*|(|\<delta\><rsup|2>S/\<delta\><wide|\<phi\>|~>
        \<delta\><wide|\<phi\>|~>|)><rsup|-1>\<cdot\><around*|(|\<delta\>S/\<delta\><wide|\<phi\>|~>|)>>
        term in Wegner-Honghton equation is absent, as eq. (13) displays.
      </theorem>

      And if <math|\<phi\><around*|(|x|)>\<neq\>\<Phi\>>, then this
      expression will be too complicated.

      <\proof>
        First,

        <\equation*>
          <frac|\<delta\>S|\<delta\><wide|\<phi\>|~><around*|(|q|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=<around*|(|q<rsup|2>+m<rsup|2>|)>+<frac|\<lambda\>|3!>
          <big|int>\<mathd\>p<rsub|1> \<mathd\>p<rsub|2> \<mathd\>p<rsub|3>
          \<delta\><around*|(|p<rsub|1>+p<rsub|2>+p<rsub|3>+q|)>
          \<phi\><around*|(|p<rsub|1>|)> \<phi\><around*|(|p<rsub|2>|)>
          \<phi\><around*|(|p<rsub|3>|)> .
        </equation*>

        And if <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>, then

        <\equation*>
          <frac|\<delta\>S|\<delta\><wide|\<phi\>|~><around*|(|q|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=<around*|(|q<rsup|2>+m<rsup|2>|)>+0
          .
        </equation*>

        Thus, if <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>, then the
        other term in Wegner-Honghton equation becomes

        <\eqnarray*>
          <tformat|<table|<row|<cell|>|<cell|>|<cell|<big|int>\<mathd\>q<rsub|1>
          \<mathd\>q<rsub|2> <frac|\<delta\>S|\<delta\><wide|\<phi\>|~><around*|(|q<rsub|1>|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>
          <around*|(|<frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>><around*|(|q<rsub|1>|)><rsup|>
          \<delta\><wide|\<phi\>|~><around*|(|q<rsub|2>|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>|)><rsup|-1>
          <frac|\<delta\>S|\<delta\><wide|\<phi\>|~><around*|(|q<rsub|2>|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>>>|<row|<cell|>|<cell|=>|<cell|<big|int>\<mathd\>q<rsub|1>
          \<mathd\>q<rsub|2> <frac|<around*|(|q<rsub|1><rsup|2>+m<rsup|2>|)>
          <around*|(|q<rsub|2><rsup|2>+m<rsup|2>|)>|\<delta\><around*|(|0|)>
          <around*|[|<around*|(|q<rsup|2>+m<rsup|2>|)>+<around*|(|\<lambda\>/2|)>
          \<Phi\><rsup|2>|]>> .>>>>
        </eqnarray*>

        Apparently, this term vanishes because of the
        <math|\<delta\><rsup|-1><around*|(|0|)>> factor. This concides with
        eq. (13) where this term is absent. Proof ends.
      </proof>
    </indent>
  </note>

  <\note>
    [P8, pg3]

    <\quotation>
      ......the sum of the one-loop graphs whose external legs have vanishing
      momentum and the internal lines carry momentum
      <math|<around*|\||p|\|>=k>, cf Fig. 2......
    </quotation>

    <\indent>
      <strong|Recognize that this is the usual diagramic deriving (rather
      than explanation) of Wegner-Honghton equation!> And herein, this
      diagramic becomes, instead, the explanation of Wegner-Hongton equation,
      which is derived by many approximations (e.g.
      <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>, a constant) and
      assumptions (e.g. <reference|note: assumption of loop expansion>),
      instead of being ``exact''.

      [Condition <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>> is Essential:
      the Appearence of <math|\<delta\><rsup|-1><around*|(|0|)>> Factor]

      In Fourier-space, denote <math|<around*|\||p|\|>\<in\><around*|[|0,k-\<Delta\>k|]>>
      and <math|<around*|\||q|\|>\<in\><around*|(|k-\<Delta\>k,k|]>>, that
      is, <math|p> is ``off-shell'' and <math|q> ``on-shell''. Then,

      <\equation*>
        <frac|1|2> Tr ln <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>>
        \<delta\><wide|\<phi\>|~>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=<frac|1|2>
        <big|int>\<mathd\>q ln <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><around*|(|-q|)>
        \<delta\><wide|\<phi\>|~><around*|(|q|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>
        .
      </equation*>

      LPA makes <math|S=<big|int>\<mathd\>p <around*|(|p<rsup|2>+m<rsup|2>|)>
      \<phi\><around*|(|p|)> \<phi\><around*|(|-p|)>+<around*|(|\<lambda\>/4!|)>
      <big|int>\<mathd\>p<rsub|1>\<cdots\>\<mathd\>p<rsub|4>
      \<delta\><around*|(|p<rsub|1>+\<cdots\>+p<rsub|4>|)>
      \<phi\><around*|(|p<rsub|1>|)>\<cdots\>\<phi\><around*|(|p<rsub|4>|)>>,
      thus

      <\equation*>
        <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>><around*|(|q|)>
        \<delta\><wide|\<phi\>|~><around*|(|-q|)>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=\<delta\><around*|(|0|)>
        <around*|(|q<rsup|2>+m<rsup|2>|)>+<frac|\<lambda\>|2>
        <big|int>\<mathd\>p \<phi\><around*|(|p|)> \<phi\><around*|(|-p|)> .
      </equation*>

      Then

      <\equation*>
        <frac|1|2> Tr ln <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>>
        \<delta\><wide|\<phi\>|~>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=<frac|1|2>
        <big|int>\<mathd\>q ln<around*|(|\<delta\><around*|(|0|)>
        <around*|(|q<rsup|2>+m<rsup|2>|)>+<frac|\<lambda\>|2>
        <big|int>\<mathd\>p \<phi\><around*|(|p|)> \<phi\><around*|(|-p|)>|)>
      </equation*>

      Direct Taylor expansion makes

      <\eqnarray*>
        <tformat|<table|<row|<cell|<frac|1|2> Tr ln
        <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>>
        \<delta\><wide|\<phi\>|~>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>>|<cell|=>|<cell|<frac|1|2>
        <big|int>\<mathd\>q ln<around*|(|\<delta\><around*|(|0|)>
        <around*|(|q<rsup|2>+m<rsup|2>|)>|)>+>>|<row|<cell|>|<cell|+>|<cell|<frac|1|2>
        <big|int>\<mathd\>q ln<around*|(|1+<frac|\<lambda\>|2>
        \<delta\><rsup|-1><around*|(|0|)>
        <around*|[|q<rsup|2>+m<rsup|2>|]><rsup|-1> <big|int>\<mathd\>p
        \<phi\><around*|(|p|)> \<phi\><around*|(|-p|)>|)>>>|<row|<cell|>|<cell|=>|<cell|Const->>|<row|<cell|>|<cell|->|<cell|<frac|1|2>
        <big|int>\<mathd\>q <big|sum><rsub|n=0><rsup|+\<infty\>>
        <frac|<around*|(|-1|)><rsup|n>|n!> <around*|(|<frac|\<lambda\>|2
        \<delta\><around*|(|0|)>> <frac|<big|int>\<mathd\>p
        \<phi\><around*|(|p|)> \<phi\><around*|(|-p|)>|q<rsup|2>+m<rsup|2>>|)><rsup|n>
        .>>>>
      </eqnarray*>

      <strong|Notice the appearence of <math|\<delta\><rsup|-1><around*|(|0|)>>
      factor!> For any configuation in <math|L<rsup|2><around*|(|\<bbb-R\><rsup|4>|)>>,
      the final line vanishes, then becomes trivial. This triviality is
      bothering, unless restricting to the case
      <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>, a constant, wherein
      <math|<big|int>\<mathd\>p \<phi\><around*|(|p|)>
      \<phi\><around*|(|-p|)>=<big|int>\<mathd\>p
      \<delta\><rsup|2><around*|(|p|)> \<Phi\><rsup|2>=\<delta\><around*|(|0|)>
      \<Phi\><rsup|2>>, making

      <\equation*>
        <frac|1|2> Tr ln <frac|\<delta\><rsup|2>S|\<delta\><wide|\<phi\>|~><rsup|\<ast\>>
        \<delta\><wide|\<phi\>|~>><around*|[|\<phi\>+<wide|\<phi\>|~>|]><mid|\|><rsub|<wide|\<phi\>|~>=0>=<frac|1|2>
        <around*|(|<big|int>\<mathd\>q|)>
        <big|sum><rsub|n=0><rsup|+\<infty\>>
        <frac|<around*|(|-1|)><rsup|n>|n!> <around*|(|<frac|\<lambda\>|2>
        <frac|\<Phi\><rsup|2>|k<rsup|2>+m<rsup|2>>|)><rsup|n>+Const .
      </equation*>

      <strong|So, only in \ the case <math|\<phi\><around*|(|x|)>\<equiv\>\<Phi\>>
      can the diagramic explanation, i.e. figure 2, be gained!>

      <\remark>
        <with|color|blue|The crucial <math|\<delta\><around*|(|0|)>> factor
        appears herein! (C.f. the discussion in
        <samp|rg-seminar/issues-on-wh-eq>, where it is
        <math|\<delta\><rsub|p<rsub|1>+p<rsub|2>>> rather than
        <math|\<delta\><around*|(|p<rsub|1>+p<rsub|2>|)>>.)>
      </remark>
    </indent>
  </note>

  <\note>
    [P14, pg4]

    <\quotation>
      First, the irrelevance and unimportance of a coupling constant are
      quite different concepts. An irrelevant coupling constant approaches
      its I.R. fixed point value as we move in the I.R. direction. But its
      value at the fixed point may be strong, indicating that the coupling
      constant in question is important. It is the scale dependence only what
      becomes unimportant for an irrelevant coupling constant and not its
      presence. The higher order vertices induced at low energies in a
      strongly coupled self interacting scalar field theory in
      <math|d\<less\>4> may serve as an example of this difference.
    </quotation>

    <\indent>
      Isn't the <math|g<rsub|2>> always divergent as
      <math|\<mu\>\<rightarrow\>0<rsup|+>> since it's relevant, and
      relevant-ness is defined so? Then, doesn't the suppression (by
      <math|1/<around*|(|1+g<rsub|2>|)>>) of corse-graining part of
      <math|\<beta\>>-functions always happen, so that only classical scaling
      part of <math|\<beta\>>-functions is left? And then, isn't the
      irrelevant coupling vanishing as <math|\<mu\>\<rightarrow\>0<rsup|+>>
      since the classical scaling makes it so?
    </indent>
  </note>

  <with|color|red|Give up reading. It has become terrible on friendliness.>

  \;

  <\bibliography|bib|tm-plain|>
    <\enumerate>
      <item><label|bib: RG book><em|Field Theory, Renormalization Group, and
      Critical Phenomena>, D. J. Amit, 2nd Edition.
    </enumerate>
  </bibliography>
</body>

<\references>
  <\collection>
    <associate|auto-1|<tuple|11|?>>
    <associate|bib: RG book|<tuple|1|?>>
    <associate|note: assumption of loop expansion|<tuple|6|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Bibliography>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>