<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <section|The Issues on Trans-Planckian and Inflation>

  <\question>
    <\with|color|red>
      [Key Question]

      How to describe the state of modes whose momentum was originally
      greater than the UV cut-off but now stretched by scale factor into the
      region of momentum under consideration.
    </with>
  </question>

  <\remark>
    There are two approaches to this problem. One is study the full theory,
    like applying string theory to inflation. The other is keeping effective,
    studying the critical property of the general field theory satisfying the
    demanded symmetries.
  </remark>

  <\question>
    <\with|color|red>
      [de-Sitter QFT?]

      Remind that in QTF, the co-variances of vacuum and of single particle
      states, thus of fields, are defined by Wigner little group of Poincare
      group. This is for space-time with Poincare symmetry. But, for the
      space-time with de-Sitter symmetry, what's the corresponding Wigner
      little group, and then what're the co-variances of vacuum and of single
      particles states, thus of fields, defined?
    </with>
  </question>

  <\note>
    I searched the internet, whereas found no related paper.
  </note>
</body>

<\initial>
  <\collection>
    <associate|sfactor|4>
  </collection>
</initial>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|The
      Questions> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>