<TeXmacs|1.0.7.11>

<style|generic>

<\body>
  <section|Remark on Wilson's Trick>

  Wilson's trick cannot hold at the general sense of Fourier-space.
  Precisely, if set <math|<big|int>D<around*|[|\<phi\>|]>=<big|prod><rsub|\<b-k\>>
  <big|int>\<mathd\><around*|(|\<phi\><around*|(|\<b-k\>|)>|)>> with
  <math|\<b-k\>> ``uniformly distrubuted''<\footnote>
    Aparently, <math|k> must be discreted, or <math|<big|prod><rsub|k>
    <big|int>\<mathd\><around*|(|\<phi\><around*|(|k|)>|)>> is non-sense. But
    how is it discreted? We say ``uniformly''. This word must be declared.
    For instance, let one-dimensinoal <math|k\<in\><around*|[|-M,M|]>>. Then
    ``uniformly distributed'' means a set
    <math|D<rsub|k>\<assign\><around*|{|k<rsub|j>|}>>, where
    <math|k<rsub|1>=-M>, <math|k<rsub|j+1>=k<rsub|j>+\<Delta\>k>, and
    <math|\<Delta\>k=2M/N> (so <math|k<rsub|N>=M>). So,

    <\equation*>
      <big|prod><rsub|k> <big|int>\<mathd\><around*|(|\<phi\><around*|(|k|)>|)>\<rightarrow\><big|prod><rsub|k<rsub|j>\<in\>D<rsub|k>>
      <big|int>\<mathd\><around*|(|\<phi\><around*|(|k<rsub|j>|)>|)> .
    </equation*>
  </footnote> on Fourier-space, then Wilson's trick cannot hold. Indeed, if
  <math|k> uniformly distrubuted on Fourier-space, then only <math|10%> of
  <math|k>-modes populate within <math|<around*|(|0,\<Lambda\>/10|)>>, But
  remind that path-integral furnishes every <math|k>-mode the same weight!
  Hence, <math|<wide|\<phi\>|\<bar\>><around*|(|x|)>> <\footnote>
    The definitions are <math|<wide|\<phi\>|\<bar\>><around*|(|x|)>\<assign\><big|int><rsup|\<Lambda\>><rsub|\<Lambda\><rsub|IR>>\<mathd\><rsup|3>k
    \<phi\><around*|(|\<b-k\>|)> \<mathe\><rsup|\<mathi\>
    \<b-k\>\<cdot\>\<b-x\>>> and <math|\<psi\><around*|(|x|)>\<assign\><big|int><rsup|\<Lambda\>+\<delta\>\<Lambda\>><rsub|\<Lambda\>>\<mathd\><rsup|3>k
    \<phi\><around*|(|\<b-k\>|)> \<mathe\><rsup|\<mathi\>
    \<b-k\>\<cdot\>\<b-x\>>>.
  </footnote>cannot be slowly varying in views of
  <math|\<psi\><around*|(|x|)>>. Wilson's trick breaks down.

  To solve this problem, we must look back to the original image Wilson
  constructed in his early treatises.<\footnote>
    Wilson (1970), Wilson (1979), and Peskin's review on Wilson's papers.
  </footnote> Originally, Wilson's image is like wavelet. That is, momentum
  slice is set on log-Fourier-space, i.e. ``uniformly distributed'' along
  <math|<around*|[|ln<around*|(|\<Lambda\><rsub|IR>/\<Lambda\><rsub|IR>|)>,ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>|]>>.
  Precisely, let <math|<around*|\||\<b-k\>|\|>\<in\><around*|[|\<Lambda\><rsub|IR>,\<Lambda\>|]>>
  and <math|N\<gg\>1>; Then <math|D<rsub|\<kappa\>>\<assign\><around*|{|\<kappa\><rsub|j>|}>>
  with <math|\<kappa\><rsub|1>=ln<around*|(|\<Lambda\><rsub|IR>/\<Lambda\><rsub|IR>|)>=0>,
  <math|\<kappa\><rsub|j+1>=\<kappa\><rsub|j>
  \<mathe\><rsup|\<Delta\>\<kappa\>>>, and
  <math|\<Delta\>\<kappa\>=ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>/N>
  (so <math|\<kappa\><rsub|N>=\<Lambda\>>);
  <math|D<rsub|\<b-k\>>\<assign\><around*|{|<around*|(|\<kappa\><rsub|j>,\<theta\>,\<varphi\>|)>:\<kappa\><rsub|j>\<in\>D<rsub|\<kappa\>>|}>>
  in polar-coordiantes. And then,

  <\equation*>
    <big|prod><rsub|\<b-k\>> <big|int>\<mathd\><around*|(|\<phi\><around*|(|\<b-k\>|)>|)>\<rightarrow\><big|prod><rsub|\<b-k\><rsub|j>\<in\>D<rsub|\<b-k\>>>
    <big|int>\<mathd\><around*|(|\<phi\><around*|(|\<b-k\><rsub|j>|)>|)> .
  </equation*>

  <\framed>
    As a consequence, the Wilson effective action
    <math|S<rsub|\<Lambda\>><around*|[|#|]>>, within path-integral (thus
    <math|\<b-k\>> is discreted), must be realized as a function on
    <math|<around*|{|\<phi\><around*|(|\<b-k\>|)>:\<b-k\>\<in\>D<rsub|\<b-k\>>|}>>.
  </framed>

  Indeed, if not so, then the path-integral is non-sense, since integration
  is only taken on the <math|k>-mode with
  <math|\<b-k\>\<in\>D<rsub|\<b-k\>>>. With this realization, Wilson's trick
  holds. Verily, for instance let <math|\<Lambda\>/\<Lambda\><rsub|IR>=\<mathe\><rsup|10>\<sim\>10<rsup|5>>,
  then about <math|80%> of <math|k>-modes populate within
  <math|<around*|(|0,\<Lambda\>/100|)>>.<\footnote>
    Concretely, denote <math|\<alpha\>\<assign\>ln<around*|(|\<mu\>/\<Lambda\><rsub|IR>|)>>,
    then <math|\<mu\>=\<Lambda\><rsub|IR> \<mathe\><rsup|\<alpha\>>>. Let
    <math|\<mu\>=\<Lambda\>>, then <math|\<alpha\>=ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>>.
    And let <math|\<mu\>=\<Lambda\>/10>, then
    <math|\<alpha\>=ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>-2.3>.
    Hence, if <math|\<Lambda\>/\<Lambda\><rsub|IR>=\<mathe\><rsup|10>>, then
    the ratio is <math|<around*|[|ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>-2.3|]>/<around*|[|ln<around*|(|\<Lambda\>/\<Lambda\><rsub|IR>|)>|]>\<approx\>80%>.
    So, about <math|80%> of <math|k>-modes populate within
    <math|<around*|(|0,\<Lambda\>/10|)>> (used
    <math|\<Lambda\><rsub|IR>\<approx\>0>).
  </footnote><\footnote>
    And if <math|\<Lambda\>/\<Lambda\><rsub|IR>=\<mathe\><rsup|50>\<sim\>10<rsup|21>>,
    then it is about <math|95%> for <math|<around*|(|0,\<Lambda\>/10|)>>, and
    about <math|90%> for <math|<around*|(|0,\<Lambda\>/100|)>>.
  </footnote> And remind that path-integral furnishes every <math|k>-mode the
  same weight. Then, <math|<wide|\<phi\>|\<bar\>><around*|(|x|)>> does be
  slowly varying in views of <math|\<psi\><around*|(|x|)>>.

  In addition, <math|N> can be large enough, so that the distribution of
  <math|<around*|\||\<b-k\>|\|>> along <math|<around*|[|\<Lambda\><rsub|IR>,\<Lambda\>|]>>
  is ``dense'' enough. Only <math|<big|int>D<around*|[|\<phi\>|]>> is changed
  by this realization. So, we shall not worry about manipulating Fourier
  transform. And the only thing to be kept in mind, when manipulating in
  Fourier space, is that the lower <math|k>-modes has more weight in
  path-integral.

  <section|Application>

  If <math|S<around*|[|\<phi\>|]>> is local on <math|\<phi\><around*|(|x|)>>,
  i.e. <math|S<around*|[|\<phi\>|]>=<big|int>\<mathd\>x
  <with|math-font|cal|L><around*|(|\<partial\><rsup|n>\<phi\><around*|(|x|)>|)>>
  with <math|n=0,1,\<ldots\>> then <math|\<delta\><rsup|n>S/\<delta\>\<phi\><rsup|n>>
  is local. Hence, if expand <math|S<around*|[|\<phi\>|]>> by <math|\<psi\>>
  where <math|\<phi\><around*|(|x|)>=<wide|\<phi\>|\<bar\>><around*|(|x|)>+\<psi\><around*|(|x|)>>
  with <math|\<psi\><around*|(|x|)>=<big|int><rsub|\<Lambda\>><rsup|\<Lambda\>+\<delta\>\<Lambda\>>\<mathd\>k
  \<mathe\><rsup|\<mathi\> k x> \<phi\><around*|(|k|)>>, then we have

  <\equation*>
    S<around*|[|\<phi\>|]>=S<around*|[|<wide|\<phi\>|\<bar\>>|]>+<big|int>\<mathd\>x
    <frac|\<delta\>S|\<delta\>\<phi\><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><around*|(|x|)>+<big|int>\<mathd\>x
    <frac|\<delta\><rsup|2>S|\<delta\>\<phi\><rsup|2><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><rsup|2><around*|(|x|)>+<with|math-font|cal|O><around*|(|\<delta\>\<Lambda\><rsup|2>|)>
    .
  </equation*>

  Indeed, notice that <math|\<delta\><rsup|n>S/\<delta\>\<phi\><rsup|n><around*|(|x|)>>
  depends only on <math|<wide|\<phi\>|\<bar\>><around*|(|x|)>>. Then,

  <\equation*>
    <big|int>\<mathd\>x <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><rsup|n><around*|(|x|)>=<big|sum><rsub|j><around*|(|<frac|100|\<Lambda\>>|)>
    <big|int><rsub|D<rsub|j>>\<mathd\>x <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><rsup|n><around*|(|x|)> ,
  </equation*>

  where <math|D<rsub|j>> are blocks with width <math|100/\<Lambda\>>. By
  Wilson's trick, within these blocks, <math|<wide|\<phi\>|\<bar\>><around*|(|x|)>>
  is slowly varying for <math|\<psi\><around*|(|x|)>>, while
  <math|\<psi\><around*|(|x|)>> varies as usual (since
  <math|100/\<Lambda\>\<gg\>1/\<Lambda\>>, the Fourier period of
  <math|\<psi\><around*|(|x|)>>). Thus, approximately

  <\equation*>
    <big|sum><rsub|j><around*|(|<frac|100|\<Lambda\>>|)>
    <big|int><rsub|D<rsub|j>>\<mathd\>x <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><rsup|n><around*|(|x|)><above|\<approx\>|W.T.><big|sum><rsub|j><around*|(|<frac|100|\<Lambda\>>|)>
    <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x<rsub|j>|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    <big|int><rsub|D<rsub|j>>\<mathd\>x \ \<psi\><rsup|n><around*|(|x|)> .
  </equation*>

  Thus, for <math|n\<gtr\>1>,

  <\eqnarray*>
    <tformat|<table|<row|<cell|<big|int>\<mathd\>x
    <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    \<psi\><rsup|n><around*|(|x|)>>|<cell|<above|\<approx\>|W.T.>>|<cell|<big|sum><rsub|j><around*|(|<frac|100|\<Lambda\>>|)>
    <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x<rsub|j>|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    <big|int><rsub|D<rsub|j>>\<mathd\>x \ \<psi\><rsup|n><around*|(|x|)>>>|<row|<cell|>|<cell|=>|<cell|<big|sum><rsub|j><around*|(|<frac|100|\<Lambda\>>|)>
    <frac|\<delta\><rsup|n>S|\<delta\>\<phi\><rsup|n><around*|(|x<rsub|j>|)>><around*|[|<wide|\<phi\>|\<bar\>>|]>
    <big|int><rsub|\<Lambda\>><rsup|\<Lambda\>+\<delta\>\<Lambda\>>\<mathd\>p<rsub|1>\<cdots\><big|int><rsub|\<Lambda\>><rsup|\<Lambda\>+\<delta\>\<Lambda\>>
    \<mathd\>p<rsub|n> \<delta\><around*|(|p<rsub|1>+\<cdots\>+p<rsub|n>|)>
    \<phi\><around*|(|p<rsub|1>|)> \<cdots\>
    \<phi\><around*|(|p<rsub|n>|)>>>|<row|<cell|>|<cell|=>|<cell|<with|math-font|cal|<with|math-font|cal|O><around*|(|\<delta\>\<Lambda\><rsup|n-1>|)>>
    .>>>>
  </eqnarray*>

  And for <math|n=1>, it is <math|<with|math-font|cal|O><around*|(|\<delta\>\<Lambda\>|)>>.
  This is why we can drop the <math|<big|int>\<delta\><rsup|n>S/\<delta\>\<phi\><rsup|n>
  \<psi\><rsup|n>> terms for <math|n\<geqslant\>3>, as long as
  <math|S<around*|[|\<phi\>|]>> is local on <math|\<phi\><around*|(|x|)>>,
  when all shall be kept linear to <math|\<delta\>\<Lambda\>>. If it is
  non-local, that is the kernel is not a peak-like as
  <math|\<delta\>>-function, but a smooth one, then it becomes
  <math|<with|math-font|cal|O><around*|(|\<delta\>\<Lambda\><rsup|n>|)>>.
</body>

<\references>
  <\collection>
    <associate|auto-1|<tuple|1|?>>
    <associate|auto-2|<tuple|2|?>>
    <associate|footnote-1|<tuple|1|?>>
    <associate|footnote-2|<tuple|2|?>>
    <associate|footnote-3|<tuple|3|?>>
    <associate|footnote-4|<tuple|4|?>>
    <associate|footnote-5|<tuple|5|?>>
    <associate|footnr-1|<tuple|1|?>>
    <associate|footnr-2|<tuple|2|?>>
    <associate|footnr-3|<tuple|3|?>>
    <associate|footnr-4|<tuple|4|?>>
    <associate|footnr-5|<tuple|5|?>>
  </collection>
</references>

<\auxiliary>
  <\collection>
    <\associate|toc>
      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Remark
      on Wilson's Trick> <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-1><vspace|0.5fn>

      <vspace*|1fn><with|font-series|<quote|bold>|math-font-series|<quote|bold>|Application>
      <datoms|<macro|x|<repeat|<arg|x>|<with|font-series|medium|<with|font-size|1|<space|0.2fn>.<space|0.2fn>>>>>|<htab|5mm>>
      <no-break><pageref|auto-2><vspace|0.5fn>
    </associate>
  </collection>
</auxiliary>