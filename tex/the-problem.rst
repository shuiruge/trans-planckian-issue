The Problem
===========

How to describe the state of modes whose momentum was originally greater
than the UV cut-off but now stretched by scale factor into the region of
momentum under consideration.

There are two approaches to this problem. One is study the full theory,
like applying string theory to inflation. The other is keeping
effective, studying the critical property of the general field theory
satisfying the demanded symmetries.
