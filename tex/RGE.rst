.. role:: math(raw)
   :format: html latex
..

Introduction
============

RGE is called for when we focus only on large spatial scale, while
omitting any phenomena in smaller spatial scale. This is intuitive
valid, since we all know we can study the friction without knowing the
fluctuation of atoms. But, if coupling between different momentum-modes
exists, we must keep in mind that the actions of large spatial scale,
and that containing also smaller spatial scale modes, are different
(L.Landau’s mistake). Indeed, the law of friction is different from that
of QED. The RGE is, then, employed for giving a quantitative description
of this modification on action.

It was born for studying the Ising model, but inspired from the fixed
source theory (solvable renormalizable model). Verily, because of the
generality of this modification on action, RGE is useful in many areas.
For instance, in cosmology, c.f. `this
paper <http://arxiv.org/abs/hep-ph/0410095v3>`__.

RGE, however, has developed many versions. In this note, we first give
the original version that Wilson employed. Then, show up the second
version by Callan-S?, which is based on Wilson’s.

Throughout this note, all bared quantities are labeled by a :math:`B`
index, while all renormalizaed are by none.

RGE in Wilson’s Approach
========================

Consider a truncated theory, with a ultraviolet cut-off :math:`\Lambda`.
Thus

.. math::

   \phi \left( x ; \Lambda \right) = \int_{\left| p \right| \in \left[ 0,
      \Lambda \right]} {\mathrm{d}}p {\mathrm{e}}^{{\mathrm{i}}p \cdot x} \phi \left( p \right) .

 Its Euclidean [1]_ un-normalized partition functional is

.. math::

   \tilde{Z} \left[ J ; \Lambda \right] = \int D \left[ \phi \right]_{\Lambda}
      \exp \left( - S \left[ \phi \left( x ; s \Lambda \right) ; \Lambda \right]
      - \int^{\Lambda} {\mathrm{d}}p \phi \left( p \right) J \left( - p \right)
      \right),

 where denoted
:math:`\int D \left[ \phi \right]_{\Lambda} {:=}\prod_{p \in
\left[ 0, \Lambda \right]} \int {\mathrm{d}}\left( \phi \left( p \right) \right)`,
and where the parameter :math:`\Lambda` in
:math:`S \left[ \#1 ; \Lambda \right]` is represented by the dependence
of :math:`\Lambda` in the couplings in :math:`S`, as to be revealed in
the following. This kind of truncated action, whose couplings depend on
the truncated scale :math:`\Lambda`, is called *Wilson (Wilsonian)
effective action*.

In any case, this action can always be gained by reducing a full theory,
for instance, by matching results of S-matrices between that calculated
by the full theory and that by the Wilson effective action. It must be
stressed that all couplings in Wilson effective action
:math:`S \left[ \phi \left( x ; \Lambda
\right) ; \Lambda \right]` are bared, including the field
:math:`\phi \left( x ;
\Lambda \right)`. The bared coefficients, even though all finite, can be
much great, so that renormalization is inevitable.

Normalized partition function is
:math:`Z \left[ J ; \Lambda \right] = \tilde{Z}
\left[ J ; \Lambda \right] / \tilde{Z} \left[ 0 ; \Lambda \right]`. But,
by virtue of this simple relation, we focus on :math:`\tilde{Z}` only.

Integrate out Momentum Slice
----------------------------

Conveniently, denote
:math:`\psi \left( x \right) {:=}\int_{\left| p \right| \in
\left( s \Lambda, \Lambda \right]} {\mathrm{d}}p {\mathrm{e}}^{{\mathrm{i}}p \cdot x} \phi
\left( p \right)`; thus
:math:`\phi \left( x ; \Lambda \right) = \phi \left( x ; s
\Lambda \right) + \psi \left( x \right)`.

For :math:`s \Lambda` with any :math:`0 < s < 1`,
:math:`\exists S \left[ \phi \left( x ; s
  \Lambda \right) ; s \Lambda \right]`, s.t.

.. math::

   \tilde{Z} \left[ J ; \Lambda \right] = \int D \left[ \phi \right]_{s
        \Lambda} \exp \left( - S \left[ \phi \left( x ; s \Lambda \right) ; s
        \Lambda \right] - \int^{s \Lambda} {\mathrm{d}}p \phi \left( p \right) J
        \left( - p \right) \right) .

For any configuration :math:`\phi \left( x ; s \Lambda \right)` given,
denote

.. math::

   \exp \left( - \delta S \left[ \phi \left( x ; s \Lambda \right) \right]
        \right) {:=}\int D \left[ \psi \right] \exp \left( - S \left[ \phi
        \left( x ; s \Lambda \right) + \psi \left( x \right) ; \Lambda \right] +
        S \left[ \phi \left( x ; s \Lambda \right) ; \Lambda \right] \right) .

 Then,
:math:`S \left[ \phi \left( x ; s \Lambda \right) ; s \Lambda \right] = S
  \left[ \phi \left( x ; s \Lambda \right) ; \Lambda \right] + \delta S \left[
  \phi \left( x ; s \Lambda \right) \right]`. Indeed, let
:math:`J \left( p \right)
  = 0` for any :math:`p \in \left( s \Lambda, \Lambda \right]` then the
source term
:math:`\int^{s \Lambda} {\mathrm{d}}p \phi \left( p \right) J \left( - p \right)`
is directly gained. This is because :math:`J \left( p \right)` with
:math:`p \in \left( s
  \Lambda, \Lambda \right]` is useless when
:math:`\phi \left( p \right)` with :math:`p
  \in \left( s \Lambda, \Lambda \right]` has been integrated out. Then,
by omitting this source term,

.. math::

   \begin{aligned}
       \tilde{Z} \left[ J ; \Lambda \right] & = & \int D \left[ \phi
       \right]_{\Lambda} \exp \left( - S \left[ \phi \left( x ; \Lambda \right) ;
       \Lambda \right] \right)\\
       & = & \int D \left[ \phi \right]_{s \Lambda}  \int D \left[ \psi \right]
       \exp \left( - S \left[ \phi \left( x ; s \Lambda \right) + \psi \left( x
       \right) ; \Lambda \right] + S \left[ \phi \left( x ; s \Lambda \right) ;
       \Lambda \right] - S \left[ \phi \left( x ; s \Lambda \right) ; \Lambda
       \right] \right)\\
       & = & \int D \left[ \phi \right]_{s \Lambda} \exp \left( - S \left[ \phi
       \left( x ; s \Lambda \right) ; \Lambda \right] \right)  \int D \left[ \psi
       \right] \exp \left( - S \left[ \phi \left( x ; s \Lambda \right) + \psi
       \left( x \right) ; \Lambda \right] + S \left[ \phi \left( x ; s \Lambda
       \right) ; \Lambda \right] \right)\\
       & = & \int D \left[ \phi \right]_{s \Lambda} \exp \left( - S \left[ \phi
       \left( x ; s \Lambda \right) ; \Lambda \right] - \delta S \left[ \phi
       \left( x ; s \Lambda \right) \right] \right),
     \end{aligned}

 which is what we want.

A Toy Instance
--------------

With this proof, we find the key is to calculate
:math:`\exp \left( - \delta S
\left[ \phi \left( x ; s \Lambda \right) \right] \right)`. For clarity
the key ideas while avoiding complicated calculation, consider the toy
mode with the bared couplings :math:`\lambda_B` small enough so that the
perturbative calculation is straight-forward without any
renormalization. In addition, let :math:`\left| 1 -
s \right| \ll 1`.

[example: lambda phi 4 model]Consider a truncated Euclidean
:math:`\lambda
  \phi^4` model with mass :math:`m \ll \Lambda` and with
:math:`\lambda \ll 1`, whose action is

.. math::

   S \left[ \phi ; \Lambda \right] = - \int \frac{1}{2}  \left( \partial^2 +
        m_B^2 \right) \phi^2 \left( x ; \Lambda \right) + \frac{\lambda_B}{4!}
        \phi^4 \left( x ; \Lambda \right) {\mathrm{d}}x .

To calculate :math:`\delta S`, we first notice

.. math::

   \begin{aligned}
     &  & S \left[ \phi \left( x ; s \Lambda \right) + \psi \left( x \right) ;
     \Lambda \right] - S \left[ \phi \left( x ; s \Lambda \right) ; \Lambda
     \right]\\
     & = & - \int \frac{1}{2}  \left( \partial^2 + m_B^2 \right) \psi^2 \left( x
     \right) +\\
     & + & \lambda_B  \left( \frac{1}{3!} \phi \left( x ; s \Lambda \right)
     \psi^3 \left( x \right) + \frac{1}{4} \phi^2 \left( x ; s \Lambda \right)
     \psi^2 \left( x \right) + \frac{1}{3!} \phi^3 \left( x ; s \Lambda \right)
     \psi \left( x \right) + \frac{1}{4!} \psi^4 \left( x ; \Lambda \right)
     \right) {\mathrm{d}}x,\end{aligned}

 where used
:math:`\int {\mathrm{d}}x \left( \partial^2 + m^2 \right) \phi \left( x ; s
\Lambda \right) \psi \left( x \right) = 0` since Fourier modes are
orthogonal to each other. Next is to perturbation to :math:`\lambda` in
path-integral of :math:`\psi`. Keep in mind that
:math:`\left\langle \phi^{{\ensuremath{\operatorname{any}}}} \psi^{{\ensuremath{\operatorname{odd}}}}
\right\rangle_{\psi} = 0`. At 1st order of expansion, up to un-important
constant, only
:math:`\phi^2 \left( x ; s \Lambda \right) \psi^2 \left( x \right)` term
contributes, which is

.. math::

   \begin{aligned}
     &  & \frac{\lambda_B}{4}  \int D \left[ \psi \right] \exp \left( -
     \frac{1}{2} \int_{\left| p \right| \in \left( s \Lambda, \Lambda \right)}
     {\mathrm{d}}p \psi \left( p \right)  \left( p^2 + m_B^2 \right) \psi \left( - p
     \right) \right) \times\\
     & \times & \int_{\left| p \right| \in \left[ 0, s \Lambda \right]} {\mathrm{d}}p_1 {\mathrm{d}}p_2 \int_{\left| p \right| \in \left[ s \Lambda, \Lambda \right]}
     {\mathrm{d}}p_3 {\mathrm{d}}p_4 \delta^3 \left( p_1 + \cdots + p_4 \right) \phi \left(
     p_1 \right) \phi \left( p_2 \right) \phi \left( p_3 \right) \phi \left( p_4
     \right)\\
     & = & \frac{\lambda_B}{4}  \int_{\left| p \right| \in \left[ 0, s \Lambda
     \right]} {\mathrm{d}}p_1 {\mathrm{d}}p_2 \int_{\left| p \right| \in \left[ s \Lambda,
     \Lambda \right]} {\mathrm{d}}p_3 {\mathrm{d}}p_4 \delta^3 \left( p_1 + \cdots + p_4
     \right) \phi \left( p_1 \right) \phi \left( p_2 \right)  \frac{\delta \left(
     p_3 + p_4 \right)}{p_3^2 + m_B^2}\\
     & = & \frac{\lambda_B}{4}  \int {\mathrm{d}}x \phi^2 \left( x ; s \Lambda
     \right)  \int_{\left| p \right| \in \left[ s \Lambda, \Lambda \right]}
     {\mathrm{d}}p \frac{1}{p^2 + m_B^2}  \left( \approx \frac{1}{p^2} \right)\\
     & = & \frac{\lambda_B \Lambda^2}{4}  \left( 1 - s^2 \right)  \int {\mathrm{d}}x
     \phi^2 \left( x ; s \Lambda \right)\end{aligned}

 where in the last step used :math:`m \ll \Lambda`, and dropped some
cookies. Thus

.. math::

   \delta S \left[ \phi \left( x ; s \Lambda \right) \right] = -
      \frac{\lambda_B \Lambda^2}{4}  \left( 1 - s^2 \right)  \int {\mathrm{d}}x \phi^2
      \left( x ; s \Lambda \right) + \mathcal{O} \left( \lambda^2 \right)

 Recognizing that this contributes a
:math:`\delta m_B^2 \sim \lambda_B \Lambda^2 
\left( 1 - s^2 \right)`.

In the 2nd order of expansion, we can recognize that, for instance,
:math:`\left(
\lambda_B \phi^2 \left( x ; s \Lambda \right) \psi^2 \left( x \right)
\right)^2` contributes to :math:`\delta \lambda`. Interestingly, there’s
contribution to :math:`\phi^6 \left( x ; s \Lambda \right)`!

Matching Vertices Method
------------------------

Still consider the previous example [example: lambda phi 4 model], but
in vertex function approach. This is what Polchinski (1984) did. At
one-loop level, it is manifest that, when momenta of in-going particles,
:math:`q_j`\ s, are much smaller than :math:`\Lambda`,

.. math::

   \begin{aligned}
     \delta m_B^2 & = & \lambda_B  \int_{\left| p \right| \in \left( s \Lambda,
     \Lambda \right]} {\mathrm{d}}p \frac{1}{p^2 + m_B^2} \sim \lambda_B \Lambda^2 
     \left( 1 - s^2 \right)\\
     \delta \lambda_B & = & \lambda_B^2  \int_{\left| p \right| \in \left( s
     \Lambda, \Lambda \right]} {\mathrm{d}}p \frac{1}{p^2  \left( q_1 + q_2 - p
     \right)^2} \sim \lambda_B^2  \left( 1 - \ln \left( s \right) \right) ;\\
     \delta g_{B 6} & = & 0 ;\\
     \delta g_{B 8} & = & \lambda_B^4  \int_{\left| p \right| \in \left( s
     \Lambda, \Lambda \right]} {\mathrm{d}}p \left( \frac{1}{p^2} \right)^4 \sim
     \lambda_B^4 \Lambda^{- 4}  \left( 1 - s^{- 4} \right) ;\\
     & \ldots & ,\end{aligned}

 where :math:`g_{B n}` denotes the bared coefficients of
:math:`\phi^n \left( x \right)` term in Lagrangian density. (Sorry that
I cannot draw the associated Feynman diagrams.) Notice that this
approach gains the same as Wilson’s integrating out process at
:math:`\lambda_B^1`-order. But, it’s much easier and manifest than the
previous method.

Manifestly, after integrating out a momentum slice, many couplings,
including infinitely many irrelevant ones, emerge. This hints that, when
writing down an effective action, it would be essential to including all
terms of interaction that are valid by additional restrictions such as
symmetries.

General Case
------------

Generally, :math:`\Lambda` is large. This makes divergence” such that
contribution from each diagram cannot be small (even though still
finite), so that renormalization is essential. Thus, bared couplings
cannot be small enough, while the observed (renormalized) couplings,
however, can. Explicitly, we perform the general renormalization
procedure with renomalized couplings, say :math:`g`, fixed (remind that
renormalized mass is the pole that is observed), but with ultraviolet
cut-off. This calculate can be taken by setting ultraviolet cut-off at
:math:`\Lambda` and at :math:`s \Lambda`. Each of them can furnish bared
couplings,
:math:`g_B \left( \Lambda \right) = g + \delta g \left( \Lambda \right)`
or
:math:`g_B \left( s \Lambda \right) = g + \delta g \left( s \Lambda \right)`,
which are finite. In Wilson’s approach, the beta function, then, is the
phase vector field of :math:`\Lambda \partial g_B / \partial \Lambda`,
with additional re-scaling, as we will see.

Beta Function
-------------

The (bared) coefficients in Wilson effective action
:math:`S \left[ \phi \left( x ;
s \Lambda \right) ; s \Lambda \right]` depends on :math:`s \Lambda`,
i.e. :math:`m_B = m_B
\left( s \Lambda \right)` and
:math:`g_{B n} = g_{B n} \left( s \Lambda \right)`. In previous
instance, :math:`g_{B j > 4} \left( \Lambda \right) = 0`, but not keep
vanishing when :math:`s \neq 1`. By deriving to :math:`s`, we can get
differential equations for these coefficients, like
:math:`{\mathrm{d}}g_{B n} / {\mathrm{d}}s = \beta_n
\left( g_B ; s \Lambda \right)`, for some :math:`\beta` to be
determined. But, it benefits a lot if :math:`\beta` is independent of
:math:`s \Lambda`, so that the phase vector field is stable. This can be
fulfilled by a re-scaling. In :math:`S \left[
\phi \left( x ; \mu \right) ; \mu \right]` for a general :math:`\mu`, we
re-scale

.. math::

   \begin{aligned}
     p & \rightarrow & \tilde{p} \mu ;\\
     x & \rightarrow &  \tilde{x} \mu^{- 1}\\
     \phi \left( p \right) & \rightarrow & \tilde{\phi} \left( \tilde{p} \right)
     \mu^{- 3} ;\\
     m_B^2 \left( \mu \right) & \rightarrow & \tilde{m}_B^2 \left( \mu \right)
     \mu^2 ;\\
     g_{B n} \left( \mu \right) & \rightarrow & \tilde{g}_{B n} \left( \mu
     \right) \mu^{4 - n},\end{aligned}

 so that :math:`\tilde{p} \in \left[ 0, 1 \right]`, and that
:math:`S \left[ \phi \left( x
; \mu \right) ; \mu \right] = S \left[ \tilde{\phi} \left( \tilde{x} ; 1
\right) ; \mu \right]` (Reminding that :math:`\mu` in :math:`S`
represents the :math:`\mu` in :math:`m_B \left( \mu \right)` and
:math:`g_B \left( \mu \right)`), within which the integral always ranges
over :math:`\left[ 0, 1 \right]`.

Back to previous instance (therein the index :math:`B` is dropped).
Before integrating out,
:math:`\tilde{m}^2 \left( \Lambda \right) = m_B^2 \left( \Lambda
\right) \Lambda^{- 2}`. After integrating out,

.. math::

   \begin{aligned}
     \tilde{m}_B^2 \left( s \Lambda \right) & = & m_B^2 \left( s \Lambda \right) 
     \left( s \Lambda \right)^{- 2}\\
     & = & \left( \tilde{m}_B^2 \left( \Lambda \right) \Lambda^2 -
     \tilde{\lambda}_B \left( \Lambda \right) \Lambda^2  \left( 1 - s^2 \right)
     \right)  \left( s \Lambda \right)^2\\
     & = & \left( \tilde{m}_B^2 \left( \Lambda \right) - \tilde{\lambda}_B
     \left( \Lambda \right)  \left( 1 - s^2 \right) \right) s^2 .\end{aligned}

 Deriving to :math:`s` on both sides, and then letting :math:`s = 1`,
get

.. math::

   \Lambda \frac{{\mathrm{d}}\tilde{m}_B^2 \left( \Lambda \right)}{{\mathrm{d}}\Lambda}
      = 2 \tilde{m}_B^2 \left( \Lambda \right) - 2 \tilde{\lambda}_B \left(
      \Lambda \right) = : \beta_2 \left( \tilde{m}_B^2, \tilde{\lambda}_B
      \right),

 where
:math:`{\mathrm{d}}\tilde{m}^2_B \left( \Lambda \right) / {\mathrm{d}}\Lambda`
just represents the first order derivative of function
:math:`\tilde{m}^2_B \left( \#
\right)`. In this differential equation, the :math:`\Lambda` is
implicitly contained. The component of phase vector field,
:math:`\beta_2 \left( \tilde{m}_B^2,
\tilde{\lambda}_B \right) = 2 \tilde{m}_B^2 - 2 \tilde{\lambda}_B` is
stable forsooth. Contrarily, if not perform the re-scaling, then
:math:`m_B \left( s
\Lambda \right) = m_B \left( \Lambda \right) - \lambda_B \left( \Lambda
\right) \Lambda^2  \left( 1 - s^2 \right)`, which does make
:math:`\beta` depend on :math:`\Lambda`, for which the phase vector is
unstable.

This is the RGE version 1. For further qualitative study, especially on
stable point of RGE, c.f. QTF, section 12.4.

Stable Point
------------

Consider the root of :math:`\beta_n \left( \tilde{g}_B \right)` of all
:math:`n` (:math:`n` dofs v.s. :math:`n` equations), flowing through
which any phase flow must be fixed onto it forever. This is the stable
point, denoted by :math:`\tilde{g}_B^{\ast}`. Consider the flow with
:math:`\Lambda` decreasing, :math:`\tilde{g}_B^{\ast}` is an attractor
on :math:`n`-axis of phase space as long as
:math:`\partial \beta_n / \partial
\tilde{g}_{B n} \left( g^{\ast}_B \right) > 0`; and it repulses phase
flow as long as
:math:`\partial \beta_n / \partial \tilde{g}_{B n} \left( g^{\ast}_B \right)
< 0`. So, for the axis of phase on which it’s an attractor, the
information of the full theory is smeared out! Realizing this was
Wilson’s breakthrough (1970), as Wilson himself said so.

Digression: Effective Potential and Symmetry Breaking
-----------------------------------------------------

Effective potential :math:`V_{{\ensuremath{\operatorname{eff}}}}` is
defined as

.. math::

   V_{{\ensuremath{\operatorname{eff}}}} \left[ \Phi \left( t \right) \right] {:=}\lim_{\Lambda
        \rightarrow 0^+} V \left[ \phi \left( x ; \Lambda \right), g_B \left(
        \Lambda \right) \right],

 where
:math:`\Phi \left( t \right) {:=}\phi \left( {\ensuremath{\boldsymbol{p}}}= 0, t \right)`
and
:math:`V \left[ \phi \left( x ; \Lambda \right), g_B \left( \Lambda \right)
  \right]` the potential in
:math:`S \left[ \phi \left( x ; \Lambda \right), g_B
  \left( \Lambda \right) \right]`.

By
:math:`\lim_{\Lambda \rightarrow 0^+} S \left[ \phi \left( x ; \Lambda \right),
g_B \left( \Lambda \right) \right]`, a system with only momentum mode is
obtained. Such a system is quantum mechanical rather than of QFT!

The couplings
:math:`\lim_{\Lambda \rightarrow 0^+} g_B \left( \Lambda \right)`
determine the shape of the effective potential. If the effective
potential has the form like :math:`\left( \Phi^2 - a^2 \right)^2`, then
it has three stable points: :math:`\Phi = 0, \pm a`. Instead of being
:math:`\Phi = 0` as usual, the minimal points, however, are
:math:`\Phi = \pm a`, around which there are two potential wells. The
ground state of this quantum mechanical system is in one of the two
potential wells, which are in symmetric positions.

Discussion: What’s Renormalization for Wilson Effective Action?
---------------------------------------------------------------

For a Wilson effective action
:math:`S \left[ \phi \left( x ; \Lambda \right) ;
\Lambda \right]`, the S-matrix is directly calculated from the bared
field instead of the renormalized, between which there’s a :math:`Z`
factor. So, why is Wilson’s approach, with bared field, to S-matrix
valid? What’s the role of renormalization in Wilson’s approach?

The motivation of renormalization of field is making all things that
construct S-matrix, the observable, finite and well-defined. But, as a
truncated action, everything in and given by Wilson effective action is
finite and well-defined, even without renormalization. And, starting at
the Wilson effective action with either a renormalized field (with the
renormalization scheme independent of ad hoc renomralization scale, such
as that in QTF) or a bared field will return an expression of S-matrix,
with parameters the couplings of the Wilson effective action. Then, for
both cases, the explicit values of these couplings can be fitted out by
comparing with experiment, even though the fitted values in different
cases are different. And then, for both cases, the prediction of further
experiment follows. So we find that, **for Wilson effective action,
renormalization is nothing but a re-paramenterization, thus far from
essential.** This is unlike the case of non-truncated action, where,
without renormalization, everything is infinite and ill-defined, so that
renormalization plays an essential role.

 But, this prescription involves obstacle: from it S-matrix cannot be
calculated out, since the bared couplings are so large that perturbation
breaks down.

 The fact is that the coupling observed at infrared limit is tiny. That
is, the renormalized coupling with renormalization scale at
:math:`{\ensuremath{\boldsymbol{p}}} \approx {\ensuremath{\boldsymbol{0}}}`
is tiny. And it is on this that perturbation calculation of S-matrix is
taken!

Problems with Wilson’s Approach
-------------------------------

Perturbation calculation can only be carried out when
:math:`\lambda_B \left(
\Lambda \right)` is small. In general case, however,
:math:`\lambda_B \left( \Lambda
\right)`, the bared, is extremely large. This breaks perturbation down;
but it is the only way we know to perform some calculation.

Summary
-------

#. If coupling between different momentum-modes exists, we must keep in
   mind that the actions of large spatial scale, and that containing
   also smaller spatial scale modes, are different (L.Landau’s mistake).

#. Wilson’s approach is manipulating the bared couplings.

#. Beta function is independent of ultraviolet cut-off.

#. The existence of stable point on phase space smears out the
   information of the full theory on those bared couplings for which the
   stable point is an attractor.

#. Large bared couplings break perturbation down. But, practically,
   bared couplings are usually extremely large.

RGE in Callan-S? Approach
=========================

This section bases on the book asdf. But the deriving is different; and
many crucial conceptions are clarified. Don’t be nerving, nowhere will
we do renormalization explicitly.

Keep in mind that, in this section, only one full theory is considered.
This is crucial when analyze the relation of dependence.

Denote the couplings in Wilson effective actions
:math:`S \left[ \phi_{} \left( x
  ; \Lambda \right) ; \Lambda_{} \right]` by
:math:`\left\{ g_{B 1} \left( \Lambda
  \right), g_{B 2} \left( \Lambda \right), \ldots \right\}` including
the bared mass square.

Renormalization Scheme
----------------------

Renormalized Vertex Function
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Renormalized vertex function is DEFINED by

.. math::

   \Gamma^{\left( N \right)} {:=}Z^{- N / 2}  \left( g_B \left( \Lambda
        \right), \Lambda \right) \Gamma_B^{\left( N \right)} \left(
        {\ensuremath{\boldsymbol{p}}}_1, \ldots, {\ensuremath{\boldsymbol{p}}}_N ; g_B \left( \Lambda \right),
        \Lambda \right),

 such that, for some :math:`\mu`, called renormalization scale,

#. :math:`\Gamma^{\left( 2 \right)} |_{\left| {\ensuremath{\boldsymbol{p}}} \right| = \mu} =
       0`;

#. :math:`\frac{\partial \Gamma^{\left( 2 \right)}}{\partial \left( p^2
       \right)} |_{\left| {\ensuremath{\boldsymbol{p}}} \right| = \mu} = 1`
   ;

#. :math:`\Gamma^{\left( 4 \right)} |_{\left| {\ensuremath{\boldsymbol{p}}}_1 +{\ensuremath{\boldsymbol{p}}}_2
       \right| = \left| {\ensuremath{\boldsymbol{p}}}_2 +{\ensuremath{\boldsymbol{p}}}_3 \right| = \left|
       {\ensuremath{\boldsymbol{p}}}_3 +{\ensuremath{\boldsymbol{p}}}_4 \right| = \mu} = g_4`
   ;

#. ......if any.

This renormalization scheme is called *SP (symmetric point)*. The key is
that an ad hoc parameter is introduced in.

The dependence of :math:`\Gamma^{\left( N \right)}` is to be determined.
This renormalization scheme must be regarded as formal relations, rather
than numerical. Hence it becomes equations relating
:math:`g_B \left( \Lambda \right)`, :math:`g`, :math:`\Lambda`, and
:math:`\mu`, with :math:`\left( g_B \left( \Lambda \right), \Lambda,
\mu \right)` on lhs, and :math:`g` on rhs. [2]_ Thus,
:math:`g_n = g_n \left( g_B \left( \Lambda \right), \Lambda, \mu
\right)`. [3]_ Or say, to give the same value of bared couplings
:math:`g_B`\ s, which is independent of :math:`\mu`, different value of
:math:`\mu` shall associate different value of :math:`g`\ s. With this
realization, the :math:`\Gamma^{\left( N \right)}` depends on
:math:`g`\ s, :math:`\Lambda`, :math:`\mu`, and :math:`\left\{
{\ensuremath{\boldsymbol{p}}}_j \right\}`, abbreviate for
:math:`\left( {\ensuremath{\boldsymbol{p}}}_1, \ldots,
{\ensuremath{\boldsymbol{p}}}_N \right)`. [4]_

There are other renormalization schemes. Apart from SP, one of the most
conventional is *MS (minimal subtraction)*.

Old Renormalization V.S. BPHZ
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Old renonmalization procedure and BPHZ are quite different.

-  Old renonmalization starts at
   :math:`S \left[ \phi ; \Lambda \right] = -
     \int \frac{1}{2} \left( \partial \phi \right)^2 \left( x ; \Lambda \right) +
     g_{B 2} \left( \Lambda \right) \phi^2 \left( x ; \Lambda \right) + \cdots`,
   from which a **direct** calculation gives :math:`\Gamma_B^{\left( N
     \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, g_B \left( \Lambda \right),
     \Lambda \right)`. This bared vertex function, as
   :math:`\Lambda \rightarrow +
     \infty`, is divergent. Then, the renormalized
   :math:`\Gamma^{\left( N \right)}
     \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, g \left( g_B, \Lambda \right),
     \Lambda \right) {:=}Z^{- N / 2} \Gamma_B^{\left( N \right)} \left(
     \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, g_B \left( \Lambda \right), \Lambda
     \right)`, so that a given renormalization scheme (a set of
   renormalization conditions) is satisfied.

-  On the contrary, BPHZ starts at
   :math:`S \left[ \phi, \Lambda \right] = -
     \int \frac{1}{2} \left( \partial \phi \right)^2 \left( x ; \Lambda \right) +
     g_2 \phi^2 \left( x ; \Lambda \right) + \cdots + S_{C.T.} \left[ \phi, g,
     \Lambda \right]`, from which a direct calculation gives
   :math:`\Gamma^{\left( N
     \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, g, \Lambda \right)`,
   without via :math:`\Gamma^{\left( N \right)}_B` as well as
   :math:`g_B`, so that the same given renormalization is satisfied.

But,

Let :math:`\delta g \left( \Lambda, \mu \right)` the coupling of
counter-term. Then, the
:math:`\Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\},
  g, \Lambda \right)` by old renormalization procedure by
:math:`g_B \left( \Lambda
  \right) = g \left( \Lambda, \mu \right) + g \left( \Lambda, \mu \right) +
  \delta g \left( \Lambda, \mu \right)` and that by BPHZ are equal, so
are their renormalized couplings :math:`g`\ s.

Indeed,

.. math::

   \begin{aligned}
       &  & - \int \frac{1}{2} \left( \partial \sqrt{Z} \phi \right)^2 \left( x
       ; \Lambda \right) + g_{B 2} \left( \Lambda \right) \left( \sqrt{Z} \phi
       \right)^2 \left( x ; \Lambda \right) + \cdots\\
       & = & - \int \frac{1}{2} \left( \partial \phi \right)^2 \left( x ;
       \Lambda \right) + g_2 \phi^2 \left( x ; \Lambda \right) + \cdots +
       S_{C.T.} \left[ \phi, g, \Lambda \right] ;
     \end{aligned}

 and remind that

.. math::

   \Gamma^{\left( N \right)} {:=}Z^{- N / 2}  \left( g_B \left( \Lambda
        \right), \Lambda \right) \Gamma_B^{\left( N \right)} \left(
        {\ensuremath{\boldsymbol{p}}}_1, \ldots, {\ensuremath{\boldsymbol{p}}}_N ; g_B \left( \Lambda \right),
        \Lambda \right),

 asdf

In this note, as well as in many textbooks, old renonmalization is
employed. But parallel-ly thinking in BPHZ brings fruits.

Relations with S-matrix
~~~~~~~~~~~~~~~~~~~~~~~

[Relation between :math:`\Gamma^{\left( N \right)}` and S-matrix]

Connected S-matrix, thus the complete S-matrix, can be calculated from
:math:`\Gamma^{\left( N \right)}` as follow. First draw all possible
tree level diagrams, for any situation of scattering given. Associating
each in-going extra-leg either by
:math:`u_l^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right)` or by
:math:`u_l^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right)  \left[ \lim_{p^2 \rightarrow - m^2}
  \left( p^2 + m^2 \right) \Gamma^{\left( 2 \right)} - 1 \right]` (loops
on extra-legs). And the same for out-going extra-legs. Associating each
vertex by the corresponding vertex function
:math:`\Gamma^{\left( N \right)}`. Summing over all such tree level
diagrams get the connected S-matrix. (C.f. the diagram of explaining
:math:`\Gamma^{\left( N \right)}`.)

**For any value of :math:`\mu` given, the :math:`\mu` dependent
renormalized couplings :math:`g` can be fitted out from experimental
results of particle scattering via this relation between
:math:`\Gamma^{\left( N \right)} \left(
  \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, g, \Lambda \right)`
and S-matrix.**

From this point, the renormalized couplings, :math:`g`, shall contain
**no** physical meaning. This is a non-trivial realization on the
renormalized couplings, or generally, couplings: **they are purely
mathematical, rather than physical.** asdf

**It seems that field-theory calculation is nothing but a
parameterization of S-matrix, but always with finite effective
parameters for each specie of particle, so that the fitting will never
be redundant.**

[Relation between :math:`S \left[ \phi ; \Lambda \right]` with bared
couplings and S-matrix]

No direct relation. They are connected only implicitly via
:math:`\Gamma^{\left( N
  \right)}`. But since :math:`\Gamma^{\left( N \right)}` depends on
explicit renormalization scheme, **the connection between
:math:`S \left[ \phi ;
  \Lambda \right]` with bared couplings and S-matrix then depends on
explicit renormalization scheme.**

Relations of Dependence
-----------------------

Denote

.. math:: \tilde{g}_n {:=}\mu^{n - 4} g_n,

 :math:`\tilde{g}`\ s are thus dimensionless. Consider a fixed
renormalization scale :math:`\mu^0`, on which the dimensionless
renormalized couplings are :math:`\tilde{g}^0`, which thus is also
fixed. And, from :math:`\mu^0` and :math:`\tilde{g}^0`, we can recover
the :math:`g_B \left( \Lambda \right)` at any :math:`\Lambda` given, as
discussed. Then consider another :math:`\mu`, on which are
:math:`\tilde{g}`. Originally :math:`\tilde{g}` depends on
:math:`g_B \left( \Lambda \right)`, :math:`\Lambda` and :math:`\mu`,
which, then, depends on :math:`\tilde{g}^0`, :math:`\mu^0`,
:math:`\Lambda` and :math:`\mu`. Since :math:`\tilde{g}` is
dimensionless, it must be of the form
:math:`\tilde{g} \left( \tilde{g}^0, \mu /
\mu^0, \mu^0 / \Lambda \right)`.

The :math:`Z`, originally depends on :math:`g_B \left( \Lambda \right)`,
:math:`\Lambda`, and :math:`\mu`, then depends on :math:`\tilde{g}`,
:math:`\mu`, :math:`\Lambda`. Define

.. math:: \zeta {:=}\frac{Z|_{\mu}}{Z|_{\mu^0}}

 :math:`\zeta` depends on :math:`\tilde{g}`, :math:`\mu`,
:math:`\tilde{g}^0`, :math:`\mu^0`, :math:`\Lambda`, which, because of
dimensionless, must be of the form :math:`\zeta \left( \tilde{g},
\tilde{g}^0, \mu / \mu^0, \mu^0 / \Lambda \right)`. The motivation of
this definition is to be clarified.

As a summary,

.. math::

   \begin{aligned}
     \Gamma^{\left( N \right)} & = & \Gamma^{\left( N \right)} \left( \left\{
     {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}, \mu, \Lambda \right), {\ensuremath{\operatorname{or}}}
     \tilde{g}^0 {\ensuremath{\operatorname{and}}} \mu^0 ;\\
     \zeta & = & \zeta \left( \tilde{g}, \tilde{g}^0, \mu / \mu^0, \mu^0 /
     \Lambda \right),\end{aligned}

 wherein

.. math::

   \tilde{g} = \tilde{g} \left( \tilde{g}^0, \mu / \mu^0, \mu^0 / \Lambda
      \right) ;

 Since the bared vertex function is independent of renormalization
scale [5]_, we get

.. math::

   \zeta^{- N / 2} \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j
      \right\} ; \tilde{g}, \mu, \Lambda \right) = \Gamma^{\left( N \right)}
      \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}^0, \mu^0, \Lambda
      \right)

 This relation holds for any :math:`\Lambda`. Thus, consider a series of
Wilson effective actions, with :math:`\Lambda` increasing without an
upper bound. As is known, when :math:`\Lambda \rightarrow + \infty`,
:math:`g_B \left( \Lambda \right)` and
:math:`Z \left( g_B \left( \Lambda \right), \Lambda \right)` become
divergent. But, :math:`\tilde{g}`, :math:`\tilde{g}^0`,
:math:`\Gamma^{\left( N \right)} \left( \left\{
{\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}, \mu, \Lambda \right)`,
:math:`\Gamma^{\left( N
\right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}^0, \mu^0, \Lambda
\right)`, thus :math:`\zeta` are all finite. Thus, by the limit
:math:`\Lambda \rightarrow
+ \infty`, we get

.. math::

   \zeta^{- n / 2} \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j
      \right\} ; \tilde{g}, \mu \right) = \Gamma^{\left( N \right)} \left(
      \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}^0, \mu^0 \right) ;

 and

.. math::

   \begin{aligned}
     \Gamma^{\left( N \right)} & = & \Gamma^{\left( N \right)} \left( \left\{
     {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}, \mu \right), {\ensuremath{\operatorname{or}}} \tilde{g}^0
     {\ensuremath{\operatorname{and}}} \mu^0 ;\\
     \zeta & = & \zeta \left( \tilde{g}, \tilde{g}^0, \mu / \mu^0 \right),\end{aligned}

 wherein

.. math:: \tilde{g} = \tilde{g} \left( \tilde{g}^0, \mu / \mu^0 \right) .

 That is, the divergent :math:`\Lambda` diminishes in finite quantities.

Callan-S? Equation
------------------

Applying :math:`\mu \partial / \partial \mu` on both sides of

.. math::

   \zeta^{- N / 2} \left( \tilde{g} \left( \tilde{g}^0, \mu / \mu^0 \right),
      \tilde{g}^0, \mu / \mu^0 \right) \Gamma^{\left( N \right)} \left( \left\{
      {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g} \left( \tilde{g}^0, \mu / \mu^0
      \right), \mu \right) = \Gamma^{\left( N \right)} \left( \left\{
      {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}^0, \mu^0 \right)

 and setting :math:`\mu = \mu^0` gives [6]_

.. math::

   \left\{ - \frac{N}{2} \gamma + \beta_n  \frac{\partial}{\partial
      \tilde{g}^0_n} + \mu^0  \frac{\partial}{\partial \mu^0} \right\}
      \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ;
      \tilde{g}^0, \mu^0 \right) = 0,

 where

.. math::

   \begin{aligned}
     \beta_n & {:=}& \mu \frac{\partial}{\partial \mu} \tilde{g}_n \left(
     \tilde{g}^0, \mu / \mu^0 \right) |_{\mu = \mu^0}\\
     & = & \partial_{\#2} \tilde{g}_n \left( \tilde{g}^0, 1 \right) ;\\
     \gamma & {:=}& \mu \frac{\partial}{\partial \mu} \zeta \left( \tilde{g}
     \left( \tilde{g}^0, \mu / \mu^0 \right), \tilde{g}^0, \mu / \mu^0 \right)
     |_{\mu = \mu^0} \\
     & = & \partial_{\#1} \zeta \left( \tilde{g}^0, \tilde{g}^0, 1 \right)
     \partial_{\#2} \tilde{g} \left( \tilde{g}^0, 1 \right) + \partial_{\#3}
     \zeta \left( \tilde{g}^0, \tilde{g}^0, 1 \right) .\end{aligned}

 Since :math:`\mu^0` is arbitrary, then replace
:math:`\mu^0 \rightarrow \mu` at this end,

.. math::

   \left\{ - \frac{N}{2} \gamma \left( \tilde{g} \right) + \beta_n \left(
      \tilde{g} \right)  \frac{\partial}{\partial \tilde{g}_n} + \mu
      \frac{\partial}{\partial \mu} \right\} \Gamma^{\left( N \right)} \left(
      \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}, \mu \right) = 0,

 where the dependence of :math:`\beta` and :math:`\gamma` have been
explicitly displayed. It’s manifest that :math:`\beta_n` and
:math:`\gamma` depends only on :math:`\tilde{g}`. Thus, again, the phase
vector field of :math:`\tilde{g}` is stable. This equation is called
*Callan-S? equation*.

Solution
--------

As a linear PDE, the solution is straight-forward.

Callan-S? equation with initial condition
:math:`\tilde{g} \left( \mu^0 \right) =
  \tilde{g}^0` is

.. math::

   \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\},
        \tilde{g} \left( \mu / \mu^0 \right), \mu \right) = \exp \left[
        \frac{N}{2}  \int_1^{\mu / \mu^0} \gamma \left( \tilde{g} \left( x
        \right) \right) \frac{{\mathrm{d}}x}{x} \right] \times \Gamma^{\left( N
        \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\}, \tilde{g}^0, \mu^0
        \right),

 where the :math:`\tilde{g} \left( \mu / \mu^0 \right)` is the solution
of characteristic

.. math::

   \mu \frac{\partial \tilde{g}_n}{\partial \mu} = \beta_n \left( \tilde{g}
        \right) .

Conveniently, :math:`\rho {:=}\mu / \mu^0`. The solution can be
re-arranged as

.. math::

   \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\},
      \tilde{g}^0, \mu^0 \right) = \exp \left[ - \frac{N}{2}  \int_1^{\rho}
      \gamma \left( \tilde{g} \left( x \right) \right) \frac{{\mathrm{d}}x}{x} \right]
      \times \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\},
      \tilde{g} \left( \rho \right), \rho \mu^0 \right) .

 Consider the rhs only. By dimension analysis, it is a homogeneous
function, on :math:`\Gamma^{\left( N \right)}`\ ’s :math:`\#1` and
:math:`\#3`. Thus, by Euler formula, it becomes

.. math::

   \rho^{N + d - N d / 2} \times \exp \left[ - \frac{N}{2}  \int_1^{\rho}
      \gamma \left( \tilde{g} \left( x \right) \right) \frac{{\mathrm{d}}x}{x} \right]
      \times \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j / \rho
      \right\}, \tilde{g} \left( \rho \right), \mu^0 \right),

 where the power factor :math:`N + d - N d / 2` comes from the dimension
of :math:`\Gamma^{\left( N \right)}`, with :math:`d` denoting space-time
dimension. Combining with the lhs, and replacing
:math:`{\ensuremath{\boldsymbol{p}}}_j \rightarrow \rho {\ensuremath{\boldsymbol{p}}}_j`,
get

.. math::

   \Gamma^{\left( N \right)} \left( \left\{ \rho {\ensuremath{\boldsymbol{p}}}_j \right\},
      \tilde{g}^0, \mu^0 \right) = \rho^{N + d - N d / 2} \times \exp \left[ -
      \frac{N}{2}  \int_1^{\rho} \gamma \left( \tilde{g} \left( x \right) \right)
      \frac{{\mathrm{d}}x}{x} \right] \times \Gamma^{\left( N \right)} \left( \left\{
      {\ensuremath{\boldsymbol{p}}}_j \right\}, \tilde{g} \left( \rho \right), \mu^0 \right) .

 This form of the solution is the one usually employed.

Stable Point and Scaling Rule
-----------------------------

Consider the root of :math:`\beta_n \left( \tilde{g} \right)` of all
:math:`n` (:math:`n` dofs v.s. :math:`n` equations), flowing through
which any phase flow must be fixed onto it forever. This is the stable
point, denoted by :math:`\tilde{g}^{\ast}`. And if initial condition
:math:`\tilde{g}^0` is on this point, then :math:`\tilde{g} \left( \rho
\right) \equiv \tilde{g}^{\ast}`. The solution of Callan-S? equation
thus becomes

.. math::

   \Gamma^{\left( N \right)} \left( \left\{ \rho {\ensuremath{\boldsymbol{p}}}_j \right\},
      \tilde{g}^{\ast}, \mu \right) = \rho^{N + d - N d / 2 - N \gamma^{\ast} /
      2} \times \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\},
      \tilde{g}^{\ast}, \mu \right) .

 This is the scaling rule of renormalized vertex function: one the lhs,
the momenta of particles on legs,
:math:`{\ensuremath{\boldsymbol{p}}}_j`, are uniformly enlarged by a
factor :math:`\rho` comparing with that in the rhs, as a result of
which, one the rhs, :math:`\Gamma^{\left( N \right)}` is scaled by a
factor :math:`\rho^{N + d - N d / 2
- N \gamma^{\ast} / 2}`, leaving all others in-variant. The factor
:math:`\gamma^{\ast}` is called *anomaly dimension*, which is manifest
in this equation.

Problems in Callan-Symanzik Approach
------------------------------------

#. Modifying renormalization scheme away from mass-shell, as in
   [section: Renormalized Vertex Function], can make the expression of
   connected S-matrix ill-defined, as [section: Loops on Extra-legs and
   Renormalization of Gamma-2] shows. So, if it is S-matrix that is the
   observable, as the case in particle physics, then this fact evokes a
   problem.

#. Physically, it is what we observed that is independent of the ad hoc
   renormalization scale, :math:`\mu`. So, the condition shall be
   :math:`\mu \left(
     \partial / \partial \mu \right) S_{\beta \alpha} = 0`, where
   :math:`S_{\beta
     \alpha}` the S-matrix from :math:`| \alpha \rangle` to
   :math:`| \beta \rangle`. Because of the direct relation between
   :math:`S_{\beta \alpha}` and :math:`\Gamma^{\left( N
     \right)} = Z^{- N / 2} \Gamma_B^{\left( N \right)}`, it is hard to
   realized why
   :math:`\mu \left( \partial / \partial \mu \right) S_{\beta \alpha} = 0`
   and
   :math:`\mu \left( \partial / \partial \mu \right) \Gamma^{\left( N \right)}_B = 0`
   are held simultaneously.

Summary
-------

#. Callan-S?’s approach manipulates the renormalized quantities, for
   which renormalization is artificially defined. The RGE is derived by
   varying the ad hoc renormalization scale.

   Contrarily, Wilson’s approach manipulates the bared quantities, for
   which the RGE is derived by varying the ultraviolet cut-off.

#. The bared theory of Callan-S? approach is the full theory.

#. **Renormalized couplings are purely mathematical, rather than
   physical. It seems that field-theory calculation is nothing but a
   parameterization of S-matrix.**\ asdf

#. Beta function is independent of ad hoc renormalization scale [7]_.

#. The stable point in Callan-S?’s approach furnishes the re-scaling
   rule in a manifest way.

#. Modifying renormalization scheme away from mass-shell can make the
   expression of connected S-matrix ill-defined. It becomes a problem if
   the observable is S-matrix, as the case in particle physics.

#. Physically it shall be
   :math:`\mu \left( \partial / \partial \mu \right)
     S_{\beta \alpha} = 0` instead of
   :math:`\mu \left( \partial / \partial \mu \right)
     \Gamma^{\left( N \right)}_B = 0`, unless they are equivalent. But
   whether they are equivalent or not is far from manifest.

RGE of Bared Theory
===================

Infrared/Ultraviolet Attractor and Asymptotic Freedom
=====================================================

In this section, we focus on the beta function only. Thus all
discussions are applied to all RGE approaches.

One Parameter Model
-------------------

For simplicity, consider the massless :math:`\lambda \phi^4` model,
which has a unique coupling. The dimensionless coupling is
:math:`\tilde{\lambda}`.

.. figure:: RGE-1.eps
   :alt: A typical shape of beta function with asymptotic freedom.

   A typical shape of beta function with asymptotic freedom.

What’s the relation between the  Callan-S?’s ad hoc :math:`\mu` and the
word infrared” or ultraviolet”?

Generalize to Multi-parameter Theory
------------------------------------

How???

Residual Problems
=================

Vertex Function and Effective Action
====================================

Definition of Vertex Function: Legendre Transform
-------------------------------------------------

Define

.. math::

   \exp \left( - F \left[ J ; \Lambda \right] \right) {:=}Z \left[ J ;
      \Lambda \right] .

 Consider the Taylor expansion of :math:`F \left[ J ; \Lambda \right]`
by :math:`J \left( x
; \Lambda \right) {:=}\int^{\Lambda} {\mathrm{d}}^3 {\ensuremath{\operatorname{pJ}}} \left( p \right)`

.. math::

   F \left[ J ; \Lambda \right] = \sum_n \frac{1}{n!} \int {\mathrm{d}}^? x
      \frac{\delta^n}{\delta J \left( x_1 \right) \cdots \delta J \left( x_n
      \right)} {\ensuremath{\operatorname{asdf}}}

The nth coefficient of Taylor expansion of
:math:`F \left[ J ; \Lambda \right]` by
:math:`J \left( x ; \Lambda \right)` is the n-point connected Green
function, i.e.

.. math:: {\ensuremath{\operatorname{asdf}}}

Effective action is defined by Legrende transform

.. math:: {\ensuremath{\operatorname{asdf}}}

The n-point vertex function
:math:`\Gamma^{\left( N \right)} \left( {\ensuremath{\operatorname{asdf}}}
  \right)` is defined as the nth coefficient of Taylor expansion of
effective action by :math:`\bar{\phi} \left( x \right)`, i.e.

.. math:: \Gamma \left[ \bar{\phi} \right] = \sum_n {\ensuremath{\operatorname{asdf}}}

Physical Meaning: Relation with Connected Green Function
--------------------------------------------------------

Vertex functions and connected Green functions:

.. math::

   \Gamma^{\left( 2 \right)} \left( {\ensuremath{\boldsymbol{p}}} \right) = 1 / G^{\left( 2
        \right)}_c \left( {\ensuremath{\boldsymbol{p}}} \right)^{} = p^2 + g_{B 2} + \Pi^{\ast}
        \left( p^2 \right) ;

 and

.. math::

   G_c^{\left( N \right)} \left( x'_1, \ldots, x'_N \right) = \int {\mathrm{d}}x'_1 \cdots {\mathrm{d}}x'_N G_c^{\left( 2 \right)} \left( x_1, x_1' \right)
        \cdots G_c^{\left( 2 \right)} \left( x_N, x_N' \right) \Gamma^{\left( N >
        2 \right)} \left( x'_1, \ldots, x'_N \right) .

 These relations hold for both renormalized and bared.

C.f. asdf, section asdf.s

The physical meaning of :math:`\Gamma^{\left( N > 2 \right)}` is that
asdf

The physical meanings of :math:`\Gamma^{\left( 2 \right)}` and
:math:`\Gamma^{\left( N >
  2 \right)}` are different, even though they can be formally the same.

An Explicit Instance of Renormalization
=======================================

Further Notes: to be Re-edit
============================

Loops on Extra-legs and Renormalization of :math:`\Gamma^{\left( 2
\right)}`
------------------------------------------------------------------

Renormalization conditions
:math:`\Gamma^{\left( 2 \right)} \left( p^2 = - m^2
  \right) = 0` and
:math:`\left( {\mathrm{d}}\Gamma^{\left( 2 \right)} / {\mathrm{d}}p^2
  \right) \left( p^2 = - m^2 \right) = 1` is the sufficient and
necessary condition of eliminating the loops on extra-legs. This also
holds for massless theory, wherein simply set :math:`m = 0`.

The fact that this theorem holds for :math:`m = 0` is indeed the case in
QTF, section 11.2, wherein it is photon that is considered. But, it
seems oppose to Peskin, wherein :math:`m = 0` rises additional divergent
(c.f. P326 328 (P349 351)). What’s the difference between the two?

**Answer. **\ The only thing to be considered is
:math:`\Gamma^{\left( 2 \right)}`. So let’s focus on it, at 1-loop as
instance. Thus

.. math::

   \Gamma^{\left( 2 \right)} \left( p^2 \right) = p^2 + m^2 + \lambda \int
        {\mathrm{d}}^4 p \frac{1}{p^2 + m^2} + \delta m^2 + p^2 \delta Z .

 As calculated by Peskin via dimensional regularization,
:math:`\delta Z = 0` and
:math:`\delta m^2 = - \lambda \int {\mathrm{d}}^4 p \left[ p^2 + m^2 \right]^{- 1} = -
  \lim_{d \rightarrow 4} \lambda \Gamma \left( 1 - d / 2 \right) / \left( m^2
  \right)^{1 - d / 2}`, which is meaningless as :math:`m = 0` before
taking the limit. But, remind that, for :math:`\lambda \phi^4` model, no
gauge symmetry is to be held. So, if using hard cut-off regularization,
then, when :math:`m = 0`,
:math:`\delta m^2 = - \lim_{\Lambda \rightarrow + \infty} \lambda \int^{\Lambda}
  {\mathrm{d}}^4 p \left[ p^2 \right]^{- 1} = - \lim_{\Lambda \rightarrow + \infty}
  \lambda \Lambda^2`, which is well defined before taking the limit.

This is the difference between the two. The regularization borthers. In
the place where gauge symmetry must be held, i.e. QED, dimensional
regularization employed at :math:`m = 0` encounters no difficulty. And
in the place where gauge symmetry is absent, i.e. :math:`\lambda \phi^4`
model, hard cut-off regularization employed at :math:`m = 0` is well
defined.

The renormalization of :math:`\Gamma^{\left( N > 2 \right)}` must be
on-shell too, if S-matrix is expressed as the effective tree diagram
with all vertex coefficients replaced by the corresponding vertex
functions. So, to comparing with experiment, such expression of S-matrix
must be well-defined. This becomes wicked in Peskin’s.

By the way, remind that there’s infrared divergence in the calculation
of vertex function of QED, in QTF section 11.3. It is explained under
eq. (11.3.23) of QTF (P491). To deal with this infrared divergence, a
virtual photon mass :math:`\mu` is introduced into the calculation, with
a list of reasons why this is valid explained in the following
paragraph.

:math:`\Gamma^{\left( 2 \right)} \left( p^2 = - m^2 \right) = 0` and
 and :math:`\left(
  {\mathrm{d}}\Gamma^{\left( 2 \right)} / {\mathrm{d}}p^2 \right) \left( p^2 = - m^2
  \right) = 1` are equivalent to
:math:`\Pi^{\ast} \left( p^2 = - m^2 \right) = 0` and
:math:`\left( {\mathrm{d}}\Pi^{\ast} / {\mathrm{d}}p^2 \right) \left( p^2 = - m^2
  \right) = 0`, where :math:`\Pi^{\ast} \left( p^2 \right)` is the 1PI.
Remind that 1PI is the sum of all 1-particle irreducible diagrams. Then,
consider the diagram with one side of which a leg representing in-
(out-) going particle, and with the other side of which a propagator
connected to a vertex function. Such a diagram is one component of the
sum

.. math::

   I \left( {\ensuremath{\boldsymbol{p}}} \right) {:=}\lim_{p^2 \rightarrow - m^2}
        u^{\ast}_l \left( {\ensuremath{\boldsymbol{p}}} \right)  \left\{ \Pi^{\ast} \left( p^2
        \right) + \Pi^{\ast} \left( p^2 \right)  \frac{1}{p^2 + m^2} \Pi^{\ast}
        \left( p^2 \right) + \cdots \right\}  \frac{1}{p^2 + m^2} ;

 and every component in
:math:`I \left( {\ensuremath{\boldsymbol{p}}} \right)` uniquely
represents such a diagram.
:math:`I \left( {\ensuremath{\boldsymbol{p}}} \right)` thus represents
the sum of all such diagrams. Then, what we want is
:math:`I \left( {\ensuremath{\boldsymbol{p}}} \right) =
  0`. The sufficient and necessary condition is
:math:`\Pi^{\ast} \left( p^2 = - m^2
  \right) = 0` and
:math:`\left( {\mathrm{d}}\Pi^{\ast} / {\mathrm{d}}p^2 \right) \left( p^2 =
  - m^2 \right) = 0`. Indeed, Taylor expanding
 :math:`\Pi^{\ast} \left( p^2
  \right)` at :math:`p^2 = - m^2` gives

.. math::

   \begin{array}{l}
          \Pi^{\ast} \left( p^2 \right)
        \end{array} = \Pi^{\ast} \left( - m^2 \right) + \frac{{\mathrm{d}}\Pi^{\ast}}{{\mathrm{d}}p^2} \left( - m^2 \right)  \left( p^2 + m^2 \right) +
        \mathcal{O \left( \left( p^2 + m^2 \right)^2 \right)} .

 Thus, for instance

.. math::

   \begin{aligned}
       \Pi^{\ast} \left( p^2 \right)  \frac{1}{p^2 + m^2} & = & \Pi^{\ast} \left(
       - m^2 \right) + \frac{{\mathrm{d}}\Pi^{\ast}}{{\mathrm{d}}p^2} \left( - m^2 \right) 
       \left( p^2 + m^2 \right) + \mathcal{O \left( \left( p^2 + m^2 \right)^2
       \right)}  \frac{1}{p^2 + m^2}\\
       & = & \frac{\Pi^{\ast} \left( - m^2 \right)}{p^2 + m^2} + \frac{{\mathrm{d}}\Pi^{\ast}}{{\mathrm{d}}p^2} \left( - m^2 \right) + O \left( p^2 + m^2 \right)
       .
     \end{aligned}

 Thus, demanding
:math:`\lim_{p^2 \rightarrow - m^2} \Pi^{\ast} \left( p^2 \right) 
  \left( p^2 + m^2 \right)^{- 1}` is equivalent to demand
:math:`\Pi^{\ast} \left(
  p^2 = - m^2 \right) = 0` and
:math:`\left( {\mathrm{d}}\Pi^{\ast} / {\mathrm{d}}p^2 \right)
  \left( p^2 = - m^2 \right) = 0`. This can be easily generalized to the
whole :math:`I \left( {\ensuremath{\boldsymbol{p}}} \right)`. Thus,
proof ends.

This is the reason of employing the renormalization conditions of
:math:`\Gamma^{\left( 2 \right)}` in QTF’s renormalization scheme [8]_,
instead of any other: it brings (great) convenience. Indeed, with these
two conditions employed, you can safely regardless of all the diagrams
with loops on extra-legs. It seems, however essential too. Verily,
without these two specific renormalization conditions, when calculating
S-matrix, every extra-leg must not be simply
:math:`u^{\ast}_l \left( {\ensuremath{\boldsymbol{p}}} \right)` for
in-going particle or
:math:`u^{\ast}_l \left( {\ensuremath{\boldsymbol{p}}} \right)` for
out-going, but the :math:`I \left(
  {\ensuremath{\boldsymbol{p}}} \right)`. Unfortunately, this
:math:`I \left( {\ensuremath{\boldsymbol{p}}} \right)`, when using other
renormalization conditions on :math:`\Gamma^{\left( 2 \right)}` instead
of those in QTF, will be divergent because of
:math:`\lim_{p^2 \rightarrow
  - m^2} \left( p^2 + m^2 \right)^{- 1}`. Thus, the form of S-matrix
will be ill-defined. So, apart from those for
:math:`\Gamma^{\left( N > 2 \right)}`, these two specific
renormalization conditions for :math:`\Gamma^{\left( 2 \right)}` must be
employed for giving a well-defined form of S-matrix (for fitting data).

Renormalization of :math:`\Gamma^{\left( 2 \right)}` and Single Particle Scattering Problem
-------------------------------------------------------------------------------------------

Remind that
:math:`\Gamma^{\left( 2 \right)} \left( p^2 \right) {:=}p^2 + m^2 +
\Pi^{\ast} \left( p^2 \right)`, where
:math:`\Pi^{\ast} \left( p^2 \right)` is the 1PI.

[definition: (Modified) S-matrix](Modified) S-matrix is defined by

.. math::

   S_{\beta \alpha} = \frac{\left( \Phi_{\beta}, S \Phi_{\alpha}
        \right)}{\left( \Phi_0, S \Phi_0 \right)},

 where :math:`S` is defined as in QTF, as
:math:`\exp \left( - {\mathrm{i}}H_{{\ensuremath{\operatorname{int}}}}
  \left( t \right) \right)` in interactive picture, or as the Dyson
series :math:`S
  = 1 + \sum_{n = 1}^{+ \infty} \frac{i^n}{n!}  \int {\mathrm{d}}x_1 \cdots {\mathrm{d}}x_n T \left\{ \mathcal{H} \left( x_1 \right) \cdots \mathcal{H} \left( x_n
  \right)_{} \right\}`, where :math:`\mathcal{H}` the denstity of
:math:`H_{{\ensuremath{\operatorname{int}}}}
  \left( t \right)`.

[definition: vacuum diagram]Vacuum diagram is defined as the Feynman
diagram wherein there exists at least one sub-diagram that has no
extra-leg.

[lemma: elimination of vacuum diagram]The Dyson series expansion [9]_ of
this modified S-matrix contians no vacuum diagram. That is, all vacuum
diagrams in :math:`\left( \Phi_{\beta}, S \Phi_{\alpha} \right)` will be
eliminated by contributions from
:math:`\left( \Phi_0, S \Phi_0 \right)`.

How to proof? In [bib: RG book], section 4-2, an expliciti instance is
displayed; and in section 4-3, a proof is given, but hard to be
understood. [10]_ Giving a clear proof is a left problem.

[theorem: on connected diagram with two extra-legs]The sufficient and
necessary condition of letting
:math:`S_{{\ensuremath{\boldsymbol{p}}}', \sigma', n', {\ensuremath{\boldsymbol{p}}},
  \sigma, n} = \delta^3 \left( {\ensuremath{\boldsymbol{p}}}-{\ensuremath{\boldsymbol{p}}}' \right)
  \delta_{\sigma', \sigma} \delta_{n', n}` is
:math:`\Gamma^{\left( 2 \right)}
  \left( p^2 = - m^2 \right) = 0` with
:math:`\left( {\mathrm{d}}\Gamma^{\left( 2
  \right)} / {\mathrm{d}}p^2 \right) \left( - m^2 \right)` finite, where
:math:`S_{{\ensuremath{\boldsymbol{p}}}', \sigma', n' ; {\ensuremath{\boldsymbol{p}}}, \sigma, n} = \left(
  \Phi_{{\ensuremath{\boldsymbol{p}}}', \sigma', n'}, S \Phi_{{\ensuremath{\boldsymbol{p}}}, \sigma, n} \right)
  / \left( \Phi_0, S \Phi_0 \right)` is the modified S-matrix.

For simplicity, label the quantum number only by
:math:`{\ensuremath{\boldsymbol{p}}}`. The Dyson series expansion
:math:`S_{{\ensuremath{\boldsymbol{p}}}' ; {\ensuremath{\boldsymbol{p}}}} = \left(
  \Phi_{{\ensuremath{\boldsymbol{p}}}'}, S \Phi_{{\ensuremath{\boldsymbol{p}}}} \right) / \left( \Phi_0, S
  \Phi_0 \right)` contains only three possibilities: vacuum diagrams,
tadpoles, and connected diagrams. This is because there are only one
in-going leg and one out-going leg. Indeed, because of this, there are
two ways of pairing by Wick theorem:

#. pairing :math:`a \left( {\ensuremath{\boldsymbol{p}}}' \right)` with
   :math:`a^{\dagger} \left(
       {\ensuremath{\boldsymbol{p}}} \right)`, and then pairing left
   fields in :math:`\mathcal{H} \left(
       x_i \right)`\ s;

#. pairing :math:`a \left( {\ensuremath{\boldsymbol{p}}}' \right)` with
   a field in one of the :math:`\mathcal{H} \left( x_i \right)`\ s, and
   pairing :math:`a^{\dagger} \left(
       {\ensuremath{\boldsymbol{p}}} \right)` with another field in one
   of the :math:`\mathcal{H} \left(
       x_i \right)`\ s, and then paring the left fields;

The first class contains only vacuum diagrams. The second class can be
divided into connected and disconnected. All disconnected diagrams are
vacuum diagrams. [11]_ By lemma [lemma: elimination of vacuum diagram],
all vacuum diagrams are eliminated. This leaves only the connected
diagrams in the second class. That is, only the connected diagrams
survive in the Dyson series expansion
:math:`S_{{\ensuremath{\boldsymbol{p}}}' ; {\ensuremath{\boldsymbol{p}}}} = \left(
  \Phi_{{\ensuremath{\boldsymbol{p}}}'}, S \Phi_{{\ensuremath{\boldsymbol{p}}}} \right) / \left( \Phi_0, S
  \Phi_0 \right)`.

Hence, only consider connected diagrams. Direct calculation by Feynman
rules gives

.. math::

   \begin{aligned}
       S_{{\ensuremath{\boldsymbol{p}}}', {\ensuremath{\boldsymbol{p}}}} & = & \delta^3 \left(
       {\ensuremath{\boldsymbol{p}}}-{\ensuremath{\boldsymbol{p}}}' \right) +\\
       & + & u^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right) \Pi^{\ast} \left( p^2 \right)
       u \left( {\ensuremath{\boldsymbol{p}}} \right) + u^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right)
       \Pi^{\ast} \left( p^2 \right)  \frac{1}{p^2 + m^2} \Pi^{\ast} \left( p^2
       \right) u \left( {\ensuremath{\boldsymbol{p}}} \right) +\\
       & + & u^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right) \Pi^{\ast} \left( p^2 \right) 
       \frac{1}{p^2 + m^2} \Pi^{\ast} \left( p^2 \right)  \frac{1}{p^2 + m^2}
       \Pi^{\ast} \left( p^2 \right) u \left( {\ensuremath{\boldsymbol{p}}} \right) + \cdots\\
       & = & \delta^3 \left( {\ensuremath{\boldsymbol{p}}}-{\ensuremath{\boldsymbol{p}}}' \right) +\\
       & + & \lim_{p^2 \rightarrow - m^2} u^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right)
       \Pi^{\ast} \left( p^2 \right) u \left( {\ensuremath{\boldsymbol{p}}} \right)  \left( 1 +
       \frac{1}{p^2 + m^2 + \Pi^{\ast} \left( p^2 \right)} \Pi^{\ast} \left( p^2
       \right) \right),
     \end{aligned}

 So, the sufficient and necessary condition of
:math:`S_{{\ensuremath{\boldsymbol{p}}}',
  {\ensuremath{\boldsymbol{p}}}} = \delta^3 \left( {\ensuremath{\boldsymbol{p}}}-{\ensuremath{\boldsymbol{p}}}' \right)`
is

.. math::

   \lim_{p^2 \rightarrow - m^2} u^{\ast} \left( {\ensuremath{\boldsymbol{p}}} \right)
        \Pi^{\ast} \left( p^2 \right) u \left( {\ensuremath{\boldsymbol{p}}} \right)  \left( 1 +
        \frac{1}{p^2 + m^2 + \Pi^{\ast} \left( p^2 \right)} \Pi^{\ast} \left( p^2
        \right) \right) = 0 .

 Thus,
:math:`\lim_{p^2 \rightarrow - m^2} \Pi^{\ast} \left( p^2 \right) = 0`
or
:math:`\lim_{p^2 \rightarrow - m^2} \left( 1 + \left[ p^2 + m^2 + \Pi^{\ast}
  \left( p^2 \right) \right]^{- 1} \Pi^{\ast} \left( p^2 \right) \right) = 0`.
Both lead to :math:`\Pi^{\ast} \left( - m^2 \right) = 0`. This condition
also makes
:math:`\lim_{p^2 \rightarrow - m^2} \left[ p^2 + m^2 + \Pi^{\ast} \left( p^2
  \right) \right]^{- 1} \Pi^{\ast} \left( p^2 \right)` well-defined as
long as
:math:`\left( {\mathrm{d}}\Pi^{\ast} / {\mathrm{d}}p^2 \right) \left( - m^2 \right)`
is finite, provided by l’hospital rule. Remind that the conditions
:math:`\Pi^{\ast}
  \left( - m^2 \right) = 0` and
:math:`\left( {\mathrm{d}}\Pi^{\ast} / {\mathrm{d}}p^2 \right)
  \left( - m^2 \right)` is finite correspond to
:math:`\Gamma^{\left( 2 \right)}
  \left( p^2 = - m^2 \right) = 0` and
:math:`\left( {\mathrm{d}}\Gamma^{\left( 2 \right)}
  / {\mathrm{d}}p^2 \right) \left( - m^2 \right)` is finite,
respectively. Thus proof ends.

The single particle scattering problem is whether the (modified)
S-matrix of one in-going particle and one out-going particle in an
interactive theory is the same as that in a free theory, or not. This
problem can be completely solved and giving a positive answer if and
only if renormalization condition
:math:`\Gamma^{\left( 2 \right)} \left( p^2 = - m^2 \right) = 0` (with
:math:`\left(
  {\mathrm{d}}\Gamma^{\left( 2 \right)} / {\mathrm{d}}p^2 \right) \left( - m^2 \right)`
finite) is held.

To make the (modified) S-matrix self-consistent **in its form**,
renormalization condition
:math:`\Gamma^{\left( 2 \right)} \left( p^2 = - m^2
  \right) = 0` must be held. And we surprisingly find that, **with a
proper re-definition of S-matrix and a proper renormalization, the field
theory can be self-consistent, i.e. no single particle scattering
problem appears.**

.. [1]
   For the equivalence and conversion between Minkovskian and Euclidean
   theory, c.f. the note asdf.

.. [2]
   Such as

   .. math::

      \Gamma_R^{\left( 4 \right)} |_{\left| {\ensuremath{\boldsymbol{p}}}_1 +{\ensuremath{\boldsymbol{p}}}_2
         \right| = \ldots = \mu} = Z^{n / 2}  \left( g_B \left( \Lambda \right),
         \Lambda \right) \Gamma^{\left( N \right)} \left( {\ensuremath{\boldsymbol{p}}}_1, \ldots,
         {\ensuremath{\boldsymbol{p}}}_N ; g_B \left( \Lambda \right), \Lambda \right) |_{\left|
         {\ensuremath{\boldsymbol{p}}}_1 +{\ensuremath{\boldsymbol{p}}}_2 \right| = \ldots = \mu} = g_4

.. [3]
   In addition, :math:`g_B \left( \Lambda \right)` can also be
   represented by :math:`g`, :math:`\Lambda`, and :math:`\mu`, such as
   :math:`m_B^2 = \mu^2 + m^2 +
   \Pi_{{\ensuremath{\operatorname{loop}}}} \left( g, \Lambda \right)`.

.. [4]
   Suppose we know the renormalization factor of field,
   :math:`Z^{1 / 2}`, can be determined by :math:`g`\ s.

.. [5]
   Indeed, remind that :math:`\Gamma^{\left( N \right)}_B` is obtained
   by direct calculation from
   :math:`S \left[ \phi ; \Lambda \right] = - \int
   \frac{1}{2} \left( \partial \phi \right)^2 \left( x ; \Lambda \right) + g_{B
   2} \left( \Lambda \right) \phi^2 \left( x ; \Lambda \right) + \cdots`
   wherein :math:`\mu` is formally absent.

.. [6]
   :math:`\left( \partial / \partial \mu^0
   \right) \Gamma^{\left( N \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ;
   \tilde{g}^0, \mu^0 \right)` represents
   :math:`\partial_{\#3} \Gamma^{\left( N
   \right)} \left( \left\{ {\ensuremath{\boldsymbol{p}}}_j \right\} ; \tilde{g}^0, \mu^0
   \right)`.

.. [7]
   only when the ultraviolet cut-off tends to infinity.

.. [8]
   C.f. section 10.3 of QTF. I suddenly find that this has been a
   theorem in QTF. Ah, I made the wheel again!

.. [9]
   i.e. eq. (4.5.13) in QTF. Explicitly, :math:`S = 1 + \sum_{n =
     1}^{+ \infty} \frac{i^n}{n!}  \int {\mathrm{d}}x_1 \cdots {\mathrm{d}}x_n T \left\{
     \mathcal{H} \left( x_1 \right) \cdots \mathcal{H} \left( x_n \right)_{}
     \right\}`, where :math:`\mathcal{H}` the denstity of
   :math:`H_{{\ensuremath{\operatorname{int}}}} \left( t
     \right)`.

.. [10]
   Hint: it’s convenient to think with the bared action, instead of the
   renormalized (BPHZ).

.. [11]
   This is specific result, holding only for single particle scattering.
   For instance, for two particle scattering, :math:`\delta^3
     \left( {\ensuremath{\boldsymbol{p}}}_1' -{\ensuremath{\boldsymbol{p}}}_1 \right) \times \delta^3 \left(
     {\ensuremath{\boldsymbol{p}}}_2' -{\ensuremath{\boldsymbol{p}}}_2 \right)`
   are disconnected diagram, but without vacuum sub-diagram.
