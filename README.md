Descirption
---------

This is a directory involving anything on the trans-Planckian issue of inflation, as a topic of cooperation with CL.

* Welcome CL!

Convention
---------

* All notes are taken by TeXmacs or by Markdown.


Tips
---

* Convert from TeX (exported from TeXmacs) to anything (`md`, `rst`, and so on) by `pandoc`:
		
		pandoc -s input.tex -o output.anything
		
