
Declaration
=========
This directory involves papers purely on _exact renormalization group (ERG)_.

Notes on these papers
=========

1. _An introduction to the nonperturbative renormalization group_
   * The appendix contains proofs of several version of ERGE. 2008.

1. _A Note On Relation Between Holographic RG Equation and Polchinski RG Equation_
   * A strange paper, appearing when searched "Polchinski equation". 2005.

1. _Exact renormalization group equations- an introductory review_
   * Hard to understand, since it exhibits almost no process of derivation, but results only. It's **not** an introduction at all! _Physical Reports_, 2001

1. _Fundamentals of the exact renormalization group_
   * The newest **highly** cited _physical Reports_ paper, 2012.
   
1. _Introduction to Non-perturbative Renormalization Group and Its Recent Applications_
	* **Extremely friendly introductory paper.** 2000.

1. _Lectures on the functional renormalization group method_
   * Terrible! 2002.
   
1. _Non-perturbative renormalization group in quantum field theory and statistical physics_
   * Lecture by Wetterich, et. al, 2002.

1. Polchinski (1984).
   * Polchinski equation and proof of renormalizability of mode of single real scalar field.
   * Discussed.

To be Read
=========

1. _Fundamentals of the exact renormalization group_
   * The newest **highly** cited _physical Reports_ paper, 2012.
  
1. _An introduction to the nonperturbative renormalization group_
   * The appendix contains proofs of several version of ERGE.
