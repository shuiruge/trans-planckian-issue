
Declaration
=========
This directory involves papers relating with trans-Planckian issue of inflation.

Papers purely on non-perturbative renormalization group are in `non-perturbative-rg/`.

Notes on these papers
=========


To be read
=========

1. <del>_Cosmological ultraviolet/infrared divergences and de Sitter spacetime_.</del>
	- Discussed.

1. _Lecture -- de-Sitter QTF_
	- This relates to the quantization **after** the background-field-expansion (-perturbation) of inflaton. Then, the aim is to see **what if** using de-Sitter QTF, rather than Poincare QTF, to treat this quantization.
	- It's strange that no one apply this to inflation, as I find after taking a survey.
	
1. _Inflation as a probe of trans-Planckian physics -- a brief review and progress report_
	- Related paper: _Boundary RG-Flow Affects Initial Conditions for Inflation_.
	- Both are written by G. Shiu.

1. _Renormalization of Initial Conditions and the Trans-Planckian Problem of Inflation_.
	-This paper has no cross citing with G. Shiu's, so also needs careful treatment.

1. _Boundary Effective Field Theory and Trans-Planckian Perturbations -- Astrophysical Implications_
   - Cite _Boundary RG-Flow Affects Initial Conditions for Inflation_ by G. Shiu.
